<?php

namespace App\Reports;

use Illuminate\Support\Facades\DB;
use \koolreport\processes\NumberBucket;

use \DateTime;
use Illuminate\Support\Facades\Redis;

class TrendsReport extends \koolreport\KoolReport
{
    use \koolreport\laravel\Friendship;

    function setup()
    {

        $periodToSelect = $this->params["filterPeriod"];

        if ($periodToSelect == "1d") {

            $periodToSelect = "HOUR(time_of_trade)";
            $intervalPeriod = " INTERVAL 1 DAY ";
        } else if ($periodToSelect == "1w") {

            $periodToSelect = 'DATE_FORMAT(DATE(time_of_trade), "%b %D")';
            $intervalPeriod = " INTERVAL 1 WEEK ";
        } else if ($periodToSelect == "1m") {

            $periodToSelect = 'DATE_FORMAT(DATE(time_of_trade), "%b %e, %y")';
            $intervalPeriod = " INTERVAL 1 MONTH ";
        } else if ($periodToSelect == "6m") {

            $periodToSelect = 'DATE_FORMAT(DATE(time_of_trade), "%b %e, %y")';
            $intervalPeriod = " INTERVAL 6 MONTH ";
        } else if ($periodToSelect == "ytd") {

            $periodToSelect = 'DATE_FORMAT(DATE(time_of_trade), "%b %e, %y")';
        } else if ($periodToSelect == "1y") {

            $periodToSelect = 'DATE_FORMAT(DATE(time_of_trade), "%b %e, %y")';
            $intervalPeriod = " INTERVAL 1 YEAR ";
        } else if ($periodToSelect == "5y") {

            $periodToSelect = 'DATE_FORMAT(DATE(time_of_trade), "%b %e, %y")';
            $intervalPeriod = " INTERVAL 5 YEAR ";
        } else if ($periodToSelect == "ally") {

            $periodToSelect = 'DATE_FORMAT(DATE(time_of_trade), "%b %e, %y")';
        }

        $query = "SELECT " . $periodToSelect . " AS " . $this->params["periodTitle"] . ",
                        data_value as Amount FROM data_table WHERE 
                        element_description='" . $this->params["report_titles"][$this->params["report"]] . "'";

        if ($this->params["filterPeriod"] != "ally") {

            if ($this->params["filterPeriod"] != "ytd") {

                $lastData = DB::select("SELECT DATE(time_of_trade-" . $intervalPeriod . ") AS last_date FROM data_table
                 WHERE element_description='" . $this->params["report_titles"][$this->params["report"]] . "' ORDER BY DATE(time_of_trade)
                  DESC LIMIT 1");

                if ($lastData) {
                    $lastDate = $lastData[0]->last_date;
                    $query = "SELECT " . $periodToSelect . " AS " . $this->params["periodTitle"] . ",
                        data_value as Amount FROM data_table WHERE 
                        element_description='" . $this->params["report_titles"][$this->params["report"]] . "'
                        AND DATE(time_of_trade) >= '" . $lastDate . "' AND DATE(time_of_trade) <= CURDATE()
                         ORDER BY time_of_trade ASC";
                }
            } else {
                $query = "SELECT " . $periodToSelect . " AS " . $this->params["periodTitle"] . ",
                        data_value as Amount FROM data_table WHERE 
                        element_description='" . $this->params["report_titles"][$this->params["report"]] . "'
                        AND YEAR(time_of_trade) = YEAR(CURDATE())  ORDER BY time_of_trade ASC";
            }
        }

        $this->src("mysql")
            ->query($query)
            ->params(array(
                "report_titles" => $this->params["report_titles"]
            ))->pipe($this->dataStore("datastore"));
    }

    public static function checkTrend($query)
    {
        $result = DB::select($query);
        $length = count($result);

        if ($length == 0) {
            return '#00873C';
        }
        $lowerLimit = $result[0]->Amount;
        $upperLimit = $result[$length - 1]->Amount;

        if ($lowerLimit <= $upperLimit) {
            return '#00873C';
        } else {
            return '#EB0F29';
        }
    }

    public static function getSafaricomPriceTrend()
    {

        $cachedSafaricomTrend = json_encode(
            DB::select("SELECT DATE_FORMAT(DATE(time_of_trade), '%b %e, %y') AS Year,
                        data_value as Amount FROM data_table WHERE 
                        element_description='Safaricom Share Price'")
        );

        return json_decode($cachedSafaricomTrend, FALSE);
    }

    public static function getEquityPriceTrend()
    {

        $cachedEquityTrend = json_encode(
            DB::select("SELECT DATE_FORMAT(DATE(time_of_trade), '%b %e, %y') AS Year,
                        data_value as Amount FROM data_table WHERE 
                        element_description='Equity Bank Share Price'")
        );

        return json_decode($cachedEquityTrend, FALSE);
    }
}

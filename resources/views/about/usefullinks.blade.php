@extends('layouts.app')
<!-- Carousel section -->
@section('content')
<section id="team-top-section">
	<div class="py-5 brown-bg ">
		<h3 class="text-light text-center"> <strong>Kestrel Capital</strong> </h3>
		<h1 class="text-light text-center"> <strong>Useful Links</strong> </h1>
	</div>
	<div class="overflow-sec brown-bg">
		
	</div>
	<div class="container">
		
		
		<div class="row">
				<div class="col-md-12 mb-4">
				<div class="card team-wrap shadow py-4">
					<div class="card-body ">
						
						<ol>
							<li>
								<a href="https://www.nse.co.ke/" style="color: #3E281A;">
									Nairobi Securities Exchange
								</a>
							</li>
							<li>
								<a href="https://www.cma.or.ke/" style="color: #3E281A;">
									Capital Markets Authority
								</a>
							</li>
							<li>
								<a href="https://www.cdsckenya.com/" style="color: #3E281A;">
									Central Depository & Settlement Corporation
								</a>
							</li>
							<li>
								<a href="https://www.centralbank.go.ke/" style="color: #3E281A;">
									Central Bank of Kenya
								</a>
							</li>
							<li>
								<a href="http://www.knbs.or.ke/" style="color: #3E281A;">
									Kenya National Bureau of Statistics
								</a>
							</li>
							<li>
								<a href="https://www.rba.go.ke/" style="color: #3E281A;">
									Retirement Benefits Authority
								</a>
							</li>
						</ol>
					</div>
				</div>
				
			</div>
			

			
		</div>
	</div>
</section>
@endsection

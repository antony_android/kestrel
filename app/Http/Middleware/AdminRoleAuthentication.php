<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use Closure;

class AdminRoleAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if ($user == null) {
            return $next($request);
        }

        $role = $user->userRole->role;
        if ($role != "ADMIN") {

            Session::flash('error', 'You are not authorized to use the functionality!');
            return redirect()->back();
        }

        return $next($request);
    }
}

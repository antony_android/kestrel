<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * @property integer $id
 * @property integer $user_role_id
 * @property string $email
 * @property string $name
 * @property string $created_at
 * @property integer $status
 * @property string $updated_at
 * @property string $password
 * @property string $email_verified_at
 * @property string $approved_by
 * @property UserRole $userRole
 */
class User extends Authenticatable
{

    use Notifiable;


    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = [
        'user_role_id',  'email', 'name', 'email_verified_at',
        'created_at', 'status', 'updated_at', 'created_by',
        'password', 'approved_by'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userRole()
    {
        return $this->belongsTo('App\Models\UserRole', 'user_role_id', 'user_role_id');
    }
}

@extends('layouts.app')
<!-- Carousel section -->
@section('content')
<section id="team-top-section">
  <div class="py-5 brown-bg ">
    <h3 class="text-light text-center"> <strong>WHAT WE DO</strong> </h3>
    <h1 class="text-light text-center"> <strong>Corporate Advisory</strong> </h1>
  </div>
  <div class="overflow-sec brown-bg">

  </div>
  <div class="container">


    <div class="row">
      <div class="col-md-12 mb-4">
        <div class="card team-wrap shadow pb-4">
          <div class="card-body ">
            <div class="p-4 mb-4">
              <p class="mb-0 box-header">
                Kestrel Capital is a trusted market leader in formulating and executing, customized solutions to our clients’ pressing business challenges.</p>

            </div>
            <div class="row services-font">
              <div class="col-md-6">
                <p>
                  We integrate our expertise in Sales & Trading and Research, to cut through complexities and offer our clients seamless, innovative solutions.
                </p>
              </div>
              <div class="col-md-6">
                <h3>Services</h3>
                <ul class="small-text">
                  <li>IPO’s and NSE Equity Listings</li>
                  <li>
                    Fixed Income Listings
                  </li>
                  <li>
                    Mergers & Acquisitions
                  </li>
                  <li>
                    Fairness Opinions
                  </li>


                </ul>
              </div>
            </div>


            <!-- <h3>Advisory Services Highlights</h3> -->
            <div class="w-100 " style="text-align: center;">
              <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width="100%" style='width:100.0%;border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
 mso-yfti-tbllook:1184;mso-padding-alt:0in 0in 0in 0in;mso-border-insideh:.5pt solid windowtext;
 mso-border-insidev:.5pt solid windowtext'>
                <thead>
                  <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
                    <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;
   mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                      <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
   line-height:normal'><b><span style='font-size:10.0pt;font-family:"inherit",serif;
   mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
   color:#444444;mso-ansi-language:EN-US'>Date<o:p></o:p></span></b></p>
                    </td>
                    <td width="22%" style='width:22.5%;border:solid windowtext 1.0pt;border-left:
   none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
   padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                      <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
   line-height:normal'><b><span style='font-size:10.0pt;font-family:"inherit",serif;
   mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
   color:#444444;mso-ansi-language:EN-US'>Company<o:p></o:p></span></b></p>
                    </td>
                    <td width="24%" style='width:24.16%;border:solid windowtext 1.0pt;
   border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
   solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                      <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
   line-height:normal'><b><span style='font-size:10.0pt;font-family:"inherit",serif;
   mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
   color:#444444;mso-ansi-language:EN-US'>Transaction<o:p></o:p></span></b></p>
                    </td>
                    <td width="24%" style='width:24.8%;border:solid windowtext 1.0pt;border-left:
   none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
   padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                      <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
   line-height:normal'><b><span style='font-size:10.0pt;font-family:"inherit",serif;
   mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
   color:#444444;mso-ansi-language:EN-US'>Role<o:p></o:p></span></b></p>
                    </td>
                    <td width="15%" style='width:15.52%;border:solid windowtext 1.0pt;
   border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
   solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                      <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
   line-height:normal'><b><span style='font-size:10.0pt;font-family:"inherit",serif;
   mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
   color:#444444;mso-ansi-language:EN-US'>Industry<o:p></o:p></span></b></p>
                    </td>
                  </tr>
                </thead>
                <tr style='mso-yfti-irow:1'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Feb-20<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Acorn Holdings<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Student Housing REITs Placement <o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Placement Agent <o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Real Estate <o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:2'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Jan-18<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Deacons<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Spin off of <span class=SpellE>Mr</span>
                        Price franchise<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Independent Financial Advisor<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Retail (Apparel and Home)<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:3'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Nov-17<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>ARM Cement<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Spin-off of Non-Cement businesses<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Independent Financial Advisor<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Construction Materials (Cement)<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:4'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Nov-17<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Kenya Airways<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Financial Restructuring and Open Offer<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Sponsoring Broker<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Airlines<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:5'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Aug-16<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Deacons Kenya<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Listing <span class=GramE>By</span> Introduction
                        on the NSE<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Lead Transaction Advisor<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Retail (Apparel and Home)<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:6'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Apr-15<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Housing Finance<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Rights Issue<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Lead Sponsoring Broker<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Banking<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:7'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Nov-14<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span class=SpellE><span style='font-size:10.0pt;
  font-family:Roboto;mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
  "Times New Roman";color:#626262;mso-ansi-language:EN-US'>Unga</span></span><span style='font-size:10.0pt;font-family:Roboto;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:#626262;mso-ansi-language:EN-US'>
                        Group<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Acquisition of Majority Stake in <span class=SpellE>Ennsvalley</span> Bakery<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Independent Financial Advisor<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Food products<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:8'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Aug-14<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Diamond Trust Bank<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Rights Issue<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Transaction Advisor and Sponsoring
                        Broker<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Banking<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:9'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Jul-14<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Nairobi Securities Exchange<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Initial Public Offering (IPO)<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Co-Sponsoring Broker<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Financial Exchanges &amp; Data<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:10'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>May-14<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>REA <span class=SpellE>Vipingo</span>
                        Plantation<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Takeover Offer<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Independent Financial Advisor<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Agricultural Products<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:11'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Dec-13<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Bridgestone Corporation<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Sale of Strategic Stake in Sameer
                        Africa<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Financial Advisor<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Consumer goods (Tires and Rubber)<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:12'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Nov-13<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>CMC Motors<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Takeover Offer<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Independent Financial Advisor<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Retail (Automotive)<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:13'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Jun-13<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span class=SpellE><span style='font-size:10.0pt;
  font-family:Roboto;mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
  "Times New Roman";color:#626262;mso-ansi-language:EN-US'>AccessKenya</span></span><span style='font-size:10.0pt;font-family:Roboto;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:#626262;mso-ansi-language:EN-US'>
                        <o:p></o:p>
                      </span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Takeover Offer<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Independent Financial Advisor<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Telecommunications and IT<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:14'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Jul-12<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Diamond Trust Bank<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Rights Issue<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Transaction Advisor and Sponsoring
                        Broker<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Banking<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:15'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Mar-12<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Kenya Airways<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Rights Issue<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Co-Sponsoring Broker<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Airlines<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:16'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Jul-11<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Trans Century<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Listing <span class=GramE>By</span>
                        Introduction<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Co-Sponsoring Broker<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Diversified Investment Holdings<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:17'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Jul-11<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>British American Insurance<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Initial Public Offer (IPO)<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Joint Lead Sponsoring Broker<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Insurance<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:18'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Nov-10<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Deacons Kenya<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Public Offer of Equity<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Transaction Advisor and Sponsoring
                        Broker<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Retail (Apparel and Home)<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:19'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Aug-10<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>TPS Eastern Africa (Serena Hotels)<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Rights Issue<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Transaction Advisor and Sponsoring
                        Broker<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Consumer services (Hotels)<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:20'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Oct-09<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Safaricom<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Medium Term Note<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Joint Sponsoring Broker<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Telecommunication<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:21'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Nov-07<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Kenya Oil Company<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span class=SpellE><span style='font-size:10.0pt;
  font-family:Roboto;mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
  "Times New Roman";color:#626262;mso-ansi-language:EN-US'>Kenol-Kobil</span></span><span style='font-size:10.0pt;font-family:Roboto;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:#626262;mso-ansi-language:EN-US'>
                        Merger<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Sponsoring Broker and Transaction
                        Advisor<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Oil Marketing<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:22'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Oct-07<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>NIC Bank<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Rights Offer<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Joint Lead Sponsoring Broker<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Banking<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:23'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Apr-07<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Access Kenya<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Initial Public Offer (IPO)<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Sponsoring Broker and Transaction
                        Advisor<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Telecommunications and IT<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:24'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Apr-06<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span class=SpellE><span style='font-size:10.0pt;
  font-family:Roboto;mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
  "Times New Roman";color:#626262;mso-ansi-language:EN-US'>KenGen</span></span><span style='font-size:10.0pt;font-family:Roboto;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:#626262;mso-ansi-language:EN-US'>
                        <o:p></o:p>
                      </span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Initial Public Offer (IPO)<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Co-Sponsoring Broker<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Electric Utilities<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:25'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Mar-06<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>TPS Eastern Africa (Serena Hotels)<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Exchange Offer<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Financial Advisor<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Consumer services (Hotels)<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:26'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Dec-05<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span class=SpellE><span style='font-size:10.0pt;
  font-family:Roboto;mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
  "Times New Roman";color:#626262;mso-ansi-language:EN-US'>Celtel</span></span><span style='font-size:10.0pt;font-family:Roboto;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:#626262;mso-ansi-language:EN-US'>
                        Kenya<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Medium Term Note<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Co-Sponsoring Broker<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Telecommunications<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:27'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Jun-05<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>East African Breweries<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Tanzania Cross-Listing on DSE<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Lead Financial Advisor<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Breweries<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:28'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Jul-04<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Kenya Commercial Bank<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Rights Offer<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Co-Sponsoring Broker<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Banking<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:29'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Sep-01<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span class=SpellE><span style='font-size:10.0pt;
  font-family:Roboto;mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
  "Times New Roman";color:#626262;mso-ansi-language:EN-US'>Mumias</span></span><span style='font-size:10.0pt;font-family:Roboto;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:#626262;mso-ansi-language:EN-US'>
                        Sugar Company<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Initial Public Offer (IPO)<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Joint Lead Sponsoring Broker<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Consumer goods<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:30'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Apr-01<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>E A. Development Bank<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Medium Term Note<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Joint Lead Sponsoring Broker<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Banking<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:31'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Mar-01<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Safaricom<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Medium Term Note<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Joint Lead Sponsoring Broker<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Telecommunications<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:32'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Dec-00<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Shelter Afrique<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Medium Term Note<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Joint Lead Sponsoring Broker<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Specialized Finance (Housing and Real
                        Estate)<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:33'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Apr-98<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Kenya Commercial Bank<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Rights Offer<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Lead Sponsoring Broker<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Banking<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:34'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Oct-97<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>East African Breweries<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Rights Offer<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Lead Sponsoring Broker<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Breweries<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:35'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Jun-97<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Athi River Mining<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Initial Public Offer (IPO)<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Co-Sponsoring Broker<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Construction Materials (Cement)<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:36'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Sep-96<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Kenya Commercial Bank<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Initial Public Offer (IPO)<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Co-Sponsoring Broker<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Banking<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:37'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>May-96<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>National Bank of Kenya<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Initial Public Offer (IPO)<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Co-Sponsoring Broker<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Banking<o:p></o:p></span></p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:38;mso-yfti-lastrow:yes'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Mar-96<o:p></o:p></span></p>
                  </td>
                  <td width="22%" style='width:22.5%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Kenya Airways<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.16%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Initial Public Offer (IPO)<o:p></o:p></span></p>
                  </td>
                  <td width="24%" style='width:24.8%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Co-Sponsoring Broker<o:p></o:p></span></p>
                  </td>
                  <td width="15%" style='width:15.52%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Airlines<o:p></o:p></span></p>
                  </td>
                </tr>
              </table>

            </div>

          </div>
        </div>

      </div>



    </div>
  </div>
</section>
@endsection
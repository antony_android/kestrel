<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Kestrel Capital</title>
    <link rel="icon" href="/favicon.ico" sizes="32x32" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link href="/css/styles.css" rel="stylesheet" />

    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
</head>

<style>
    .report-header {
        background: #3E281A;
        padding: 7px;
        text-align: center;
        color: #FFF;
        font-size: 20px;
    }
</style>

<body id="page-top">

    <header class="header shadow">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid d-flex justify-content-between">
                <a class="navbar-brand" href="/" style="text-decoration: none;color: #632423;">
                    <div class="logo-section pt-2" style="font-family: 'EB Garamond', serif; color: #632423; font-size: 28px; font-weight: bold;">
                        KESTREL CAPITAL
                    </div>
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse menu-items-mobi" id="navbarSupportedContent">

                    <ul class="menu-list navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item ">
                            <a href="/">Home</a>
                        </li>
                        <li class="nav-item dropdown ">
                            <a class="nav-link dropdown-toggle p-0" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false" style="padding-right: 10px!important;">
                                Our insights
                            </a>
                            <ul class="dropdown-menu shadow" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="/insights/sampleresearch">Sample Research</a></li>
                                <li><a class="dropdown-item" href="/insights/statistics/equity-nse-market-cap/period/Quarter/1y">Key Statistics</a></li>
                            </ul>
                        </li>

                        <li class="nav-item dropdown ">
                            <a class="nav-link dropdown-toggle p-0" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false" style="padding-right: 10px!important;">
                                What we Do
                            </a>
                            <ul class="dropdown-menu shadow" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="/services/sales">Sales & Trading</a></li>
                                <li><a class="dropdown-item" href="/services/research">Research</a></li>
                                <li><a class="dropdown-item" href="/services/corporateadvisory">Corporate Advisory</a></li>
                                <li><a class="dropdown-item" href="/services/privateplacements">Private Placements</a></li>

                            </ul>
                        </li>
                        <li class="nav-item dropdown ">
                            <a class="nav-link dropdown-toggle p-0" href="#" id="navbarDropdown3" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                About Us
                            </a>
                            <ul class="dropdown-menu shadow" aria-labelledby="navbarDropdown3">
                                <li><a class="dropdown-item" href="/about">About Kestrel Capital</a></li>
                                <li><a class="dropdown-item" href="/about/team">Our Team</a></li>
                                <li><a class="dropdown-item" href="/about/nonexecutive">Non Executive Directors</a></li>
                            </ul>
                        </li>

                        <li>
                            <a href="/contact">Contact Us</a>
                        </li>
                        <li class="custom_button_yellow custom_button_yellow_top " style="margin-right: 10px;">
                            <a target="_blank" href="https://web.kestrelcapital.com/" class="research-button">Research Portal</a>
                        </li>
                        <li class="custom_button_yellow ">
                            <a target="_blank" href="https://trading.kestrelcapital.com/TradeWeb/" class="research-button">Client Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

    </header>

    <div class="container" style="padding: 15px;">

        <div class="row mt-75">

            <div class="col-sm-8" style="background:#f5f5f5;border:1px solid #ddd;">

                <?php

                $quarterLinks = array(
                    'equity-nse-market-cap',
                    'equity-nse-free-float',
                    'equity-nse-free-percent',
                    'equity-foreign-portfolio-holding',
                    'equity-foreign-portfolio-holding-percent',
                    'equity-foreign-portfolio-holding-cbk'
                );

                $period = "period";
                $periodTitle = "Month";

                if (in_array($this->params['report'], $quarterLinks)) {

                    $periodTitle = "Quarter";
                }
                if (substr($this->params["report"], 0, strpos($this->params["report"], "-")) == "banking" || substr($this->params["report"], 0, strpos($this->params["report"], "-")) == "telecom") {
                    $period = "year";
                    $periodTitle = "Year";
                }
                ?>

                <div class="row">
                    <div class="offset-1 col-sm-11" style="padding-bottom: 10px;">
                        <a href="/insights/statistics/<?php echo $this->params["report"]; ?>/day/Day/1d">
                            <button class="btn btn-primary report-buttons <?php if ($this->params["filterPeriod"] == "1d") echo 'active-year'; ?>">1D</button>
                        </a>
                        <a href="/insights/statistics/<?php echo $this->params["report"]; ?>/day/Day/5d">
                            <button class="btn btn-primary report-buttons <?php if ($this->params["filterPeriod"] == "5d") echo 'active-year'; ?>">5D</button>
                        </a>
                        <a href="/insights/statistics/<?php echo $this->params["report"]; ?>/month/Month/1m">
                            <button class="btn btn-primary report-buttons <?php if ($this->params["filterPeriod"] == "1m") echo 'active-year'; ?>">1M</button>
                        </a>
                        <a href="/insights/statistics/<?php echo $this->params["report"]; ?>/month/Month/6m">
                            <button class="btn btn-primary report-buttons <?php if ($this->params["filterPeriod"] == "6m") echo 'active-year'; ?>">6M</button>
                        </a>
                        <a href="/insights/statistics/<?php echo $this->params["report"]; ?>/month/Month/ytd">
                            <button class="btn btn-primary report-buttons <?php if ($this->params["filterPeriod"] == "ytd") echo 'active-year'; ?>">YTD</button>
                        </a>
                        <?php if (substr($this->params["report"], 0, strpos($this->params["report"], "-")) == "equity" || substr($this->params["report"], 0, strpos($this->params["report"], "-")) == "fixed") { ?>
                            <a href="/insights/statistics/<?php echo $this->params["report"]; ?>/<?php echo $period; ?>/<?php echo $periodTitle; ?>/1y">
                                <button class="btn btn-primary report-buttons <?php if ($this->params["filterPeriod"] == "1y") echo 'active-year'; ?>">1Y</button>
                            </a>
                        <?php } ?>
                        <a href="/insights/statistics/<?php echo $this->params["report"]; ?>/year/Year/5y">
                            <button class="btn btn-primary report-buttons <?php if ($this->params["filterPeriod"] == "5y") echo 'active-year'; ?>">5Y</button>
                        </a>
                        <a href="/insights/statistics/<?php echo $this->params["report"]; ?>/year/Year/10y">
                            <button class="btn btn-primary report-buttons <?php if ($this->params["filterPeriod"] == "10y") echo 'active-year'; ?>">10Y</button>
                        </a>
                        <a href="/insights/statistics/<?php echo $this->params["report"]; ?>/<?php echo $period; ?>/<?php echo $periodTitle; ?>">
                            <button class="btn btn-primary report-buttons <?php if ($this->params["filterPeriod"] == "ally") echo 'active-year'; ?>">All</button>
                        </a>
                    </div>
                </div>

                <div class="row">

                    <div class="col-sm-12">

                        <?php

                        $xIndex = "Month";
                        $years = $this->params["filterPeriod"];

                        if ($years == 5 || $years == 10) {
                            $xIndex = "Year";
                        } else {
                            if (in_array($this->params['report'], $quarterLinks)) {

                                $xIndex = "Quarter";
                            }
                        }

                        if (substr($this->params["report"], 0, strpos($this->params["report"], "-")) == "banking" || substr($this->params["report"], 0, strpos($this->params["report"], "-")) == "telecom") {
                            $xIndex = "Year";
                        }

                        if ($this->params["filterPeriod"] == "ally") {
                            \koolreport\widgets\google\LineChart::create(array(
                                "title" => $this->params["report_titles"][$this->params["report"]],
                                "dataSource" => $this->dataStore("datastore"),
                                "colorScheme" => array(
                                    "#4682B4"
                                ),
                                "height" => "450px",
                                "options" => array(
                                    "legend" => "none"
                                ),
                                "columns" => array(
                                    $xIndex,
                                    "Amount" => array(
                                        "type" => "number",
                                        "decimals" => 2,
                                        "config" => array(
                                            "yAxisID" => "bar-y-sale"
                                        )
                                    ),
                                ),
                            ));
                        } else {
                            \koolreport\widgets\google\ColumnChart::create(array(
                                "title" => $this->params["report_titles"][$this->params["report"]],
                                "dataSource" => $this->dataStore("datastore"),
                                "colorScheme" => array(
                                    "#4682B4"
                                ),
                                "height" => "400px",
                                "options" => array(
                                    "legend" => "none"
                                ),
                                "columns" => array(
                                    $xIndex,
                                    "Amount" => array(
                                        "type" => "number",
                                        "decimals" => 2,
                                        "config" => array(
                                            "yAxisID" => "bar-y-sale"
                                        )
                                    ),
                                ),
                            ));
                        }

                        ?>
                    </div>
                </div>

            </div>
            <script>
                let capitalMarkets = '<div class="accordion" id="capitalmarkets">                        <div class="accordion-item">                            <h2 class="accordion-header" id="equityHeading">                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#equities" aria-expanded="true" aria-controls="collapseOne" style="color:#632423;">                                    <i class="fas fa-coins"></i> &nbsp; Equities                                </button>                            </h2>                            <div id="equities" class="accordion-collapse collapse <?php if (substr($this->params["report"], 0, strpos($this->params["report"], "-")) == "equity") {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        echo "show";
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    } ?>" aria-labelledby="equityHeading" data-bs-parent="#capitalmarkets">                                <div class="accordion-body">                                    <ul class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-sm-start menu-reports">                                        <li class="w-100 p-15">                                            <a href="/insights/statistics/equity-nse-market-cap/period/Quarter/1y" class="accordion-link"> <span class="d-sm-inline">                                                    - NSE market-cap (USD m)</span></a>                                        </li>                                        <li class="w-100 p-15">                                            <a href="/insights/statistics/equity-nse-free-float/period/Quarter/1y" class="accordion-link"> <span class="d-sm-inline">                                                    - NSE free-float (USD m)</span></a>                                        </li>                                        <li class="w-100 p-15">                                            <a href="/insights/statistics/equity-nse-free-percent/period/Quarter/1y" class="accordion-link"> <span class="d-sm-inline">                                                    - NSE free-float %</span></a>                                        </li>                                        <li class="w-100 p-15">                                            <a href="/insights/statistics/equity-foreign-portfolio-holding/period/Quarter/1y" class="accordion-link"> <span class="d-sm-inline">                                                    - Foreign porfolio holding (USD m)</span></a>                                        </li>                                        <li class="w-100 p-15">                                            <a href="/insights/statistics/equity-foreign-portfolio-holding-percent/period/Quarter/1y" class="accordion-link"> <span class="d-sm-inline">                                                    - Foreign porfolio holding % of NSE free-float</span></a>                                        </li>                                        <li class="w-100 p-15">                                            <a href="/insights/statistics/equity-foreign-portfolio-holding-cbk/period/Quarter/1y" class="accordion-link"> <span class="d-sm-inline">                                                    - Foreign portfolio holding % of CBK FX reserves</span></a>                                        </li>                                        <li class="w-100 p-15">                                            <a href="/insights/statistics/equity-nse-turnover/period/Month/1y" class="accordion-link"> <span class="d-sm-inline">                                                    - NSE turnover (USD m)</span></a>                                        </li>                                        <li class="w-100 p-15">                                            <a href="/insights/statistics/equity-average-daily-traded/period/Month/1y" class="accordion-link"> <span class="d-sm-inline">                                                    - Average daily value traded (USD m)</span></a>                                        </li>                                        <li class="w-100 p-15">                                            <a href="/insights/statistics/equity-foreign-net-flow/period/Month/1y" class="accordion-link"> <span class="d-sm-inline">                                                    - Foreign net flow (USD m)</span></a>                                        </li>                                        <li class="w-100 p-15">                                            <a href="/insights/statistics/equity-local-pension-industry/period/Month/1y" class="accordion-link"> <span class="d-sm-inline">                                                    - Local pension industry AUM (USD m)</span></a>                                        </li>                                        <li class="w-100 p-15">                                            <a href="/insights/statistics/equity-local-pension-allocation/period/Month/1y" class="accordion-link"> <span class="d-sm-inline">                                                    - Local pension industry equity allocation %</span></a>                                        </li>                                    </ul>                                </div>                            </div>                        </div>                        <div class="accordion-item">                            <h2 class="accordion-header" id="fixedincomeHeading">                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#fixedincome" aria-expanded="false" aria-controls="collapseTwo" style="color:#632423;">                                    <i class="fas fa-cash-register"></i> &nbsp; Fixed Income                                </button>                            </h2>                            <div id="fixedincome" class="accordion-collapse collapse <?php if (substr($this->params["report"], 0, strpos($this->params["report"], "-")) == "fixed") {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    echo "show";
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                }  ?>" aria-labelledby="fixedincomeHeading" data-bs-parent="#capitalmarkets">                                <div class="accordion-body">                                    <ul class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-sm-start menu-reports">                                        <li class="w-100 p-15">                                            <a href="/insights/statistics/fixed-income-domestic-debt/period/Month/1y" class="accordion-link"> <span class="d-sm-inline">                                                    - Outstanding government domestic debt (USDm)</span></a>                                        </li>                                        <li class="w-100 p-15">                                            <a href="/insights/statistics/fixed-income-treasury-bills/period/Month/1y" class="accordion-link"> <span class="d-sm-inline">                                                    - Treasury bills auction performance rate %</span></a>                                        </li>                                        <li class="w-100 p-15">                                            <a href="/insights/statistics/fixed-income-treasury-bonds/period/Month/1y" class="accordion-link"> <span class="d-sm-inline">                                                    - Treasury bonds auction perfromance rate %</span></a>                                        </li>                                        <li class="w-100 p-15">                                            <a href="/insights/statistics/fixed-income-government-bond/period/Month/1y" class="accordion-link"> <span class="d-sm-inline">                                                    - 10 year government bond yield %</span></a>                                        </li>                                    </ul>                                </div>                            </div>                        </div>                    </div>';

                let industry = '<div class="accordion" id="capitalmarkets"><div class="accordion-item"><h2 class="accordion-header" id="bankingHeading">    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#banking" aria-expanded="true" aria-controls="banking" style="color:#632423;"><i class="fas fa-landmark"></i> &nbsp; Banking    </button></h2><div id="banking" class="accordion-collapse collapse <?php if (substr($this->params["report"], 0, strpos($this->params["report"], "-")) == "banking") {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                        echo "show";
                                                                                                                                                                                                                                                                                                                                                                                                                                                                    } ?>" aria-labelledby="bankingHeading" data-bs-parent="#capitalmarkets">    <div class="accordion-body"><ul class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-sm-start menu-reports">    <li class="w-100 p-15"><a href="/insights/statistics/banking-total-assets/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- Total assets (USD bn)</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/banking-total-assets-gdp-percent/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- Total assets % GDP</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/banking-total-loans/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- Total loans (USD bn)</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/banking-total-loans-assets/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- Total loans % of assets</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/banking-total-loans-gdp/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- Total loans % GDP</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/banking-total-deposits/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- Total deposits (USD bn)</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/banking-total-deposits-gdp/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- Total deposits % of GDP</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/banking-nim/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- NIM %</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/banking-npl/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- NPL %</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/banking-coverage-ratio/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- Coverage ratio %</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/banking-equity-total-assets/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- Equity to total assets %</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/banking-npl-net-equity/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- NPL net of provisions to equity %</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/banking-roae/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- ROaE %</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/banking-roaa/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- ROaA %</span></a>    </li></ul>    </div></div>    </div>    <div class="accordion-item"><h2 class="accordion-header" id="telecomsHeading">    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#telecoms" aria-expanded="true" aria-controls="telecoms" style="color:#632423;"><i class="fas fa-broadcast-tower"></i> &nbsp; Telecoms    </button></h2><div id="telecoms" class="accordion-collapse collapse <?php if (substr($this->params["report"], 0, strpos($this->params["report"], "-")) == "telecom") {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            echo "show";
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        }  ?>" aria-labelledby="telecomsHeading" data-bs-parent="#capitalmarkets">    <div class="accordion-body"><ul class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-sm-start menu-reports">    <li class="w-100 p-15"><a href="/insights/statistics/telecom-active-mobile-users/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- Active mobile phone users (m)</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/telecom-active-mobile-users-penetration/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- Active mobile phone users penetration %</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/telecom-active-prepaid-users/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- Pre-paid users share of active mobile phone users %</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/telecom-active-safaricom-share/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- Safaricom share of active mobile phone users %</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/telecom-active-domestic-minutes-use/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- Average minutes of use domestic calls (m)</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/telecom-active-safaricom-domestic-minutes/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- Safaricom share of domestic minutes of use %</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/telecom-active-total-calls/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- Total number of calls (m)</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/telecom-calls-per-user/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- Number of calls per active user</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/telecom-minutes-per-call/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- Minutes of use per call</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/telecom-domestic-sms/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- Total number of domestic SMS (m)</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/telecom-mobile-money-users/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- Active mobile money users (m)</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/telecom-mobile-money-users-penetration/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- Active mobile money users penetration %</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/telecom-mobile-money-transfers/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- Total value of mobile money transfers (USDm)</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/telecom-mobile-money-deposits/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- Total value of mobile money deposits (USDm)</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/telecom-mobile-money-deposit-turnover/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- Mobile money deposit turnover (x)</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/telecom-data-users/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- Total data users (m)</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/telecom-data-users-penetration/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- Data users penetration %</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/telecom-mobile-data-users/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- Total mobile data users (m)</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/telecom-safaricom-mobile-data-users/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- Safaricom share of mobile data users %</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/telecom-optic-data-users/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- Fixed fibre optic data users</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/telecom-fixed-optic-ten-mbps/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- Fixed fibre optic users subcribed to >10Mbps</span></a>    </li>    <li class="w-100 p-15"><a href="/insights/statistics/telecom-safaricom-fixed-data-users/year/Year/5y" class="accordion-link"> <span class="d-sm-inline">- Safaricom share of fixed data users %</span></a>    </li></ul>    </div></div>    </div></div>';
                $(document).ready(function() {
                    $("#category").on('change', function(e) {
                        console.log("selected")
                        let selected = $("#category").val();

                        if (selected == "Capital Markets") {
                            $("#accordion-wrapper").html(capitalMarkets);
                        } else {
                            $("#accordion-wrapper").html(industry);
                        }

                    })
                });
            </script>

            <?php if (substr($this->params["report"], 0, strpos($this->params["report"], "-")) == "equity" || substr($this->params["report"], 0, strpos($this->params["report"], "-")) == "fixed") { ?>
                <script>
                    $(document).ready(function() {
                        $("#accordion-wrapper").html(capitalMarkets);
                    });
                </script>
            <?php } else { ?>
                <script>
                    $(document).ready(function() {
                        $("#accordion-wrapper").html(industry);
                    });
                </script>
            <?php } ?>

            <div class="col-sm-4">
                <div class="d-flex flex-column align-items-sm-start p-rep-3 text-white min-vh-100">
                    <div class="custom-selectbox">
                        <select class="custom-control" id="category" name="category">
                            <option value="Capital Markets" <?php if (substr($this->params["report"], 0, strpos($this->params["report"], "-")) == "equity" || substr($this->params["report"], 0, strpos($this->params["report"], "-")) == "fixed") echo "selected";  ?>>Capital Markets</option>
                            <option value="Industry" <?php if (substr($this->params["report"], 0, strpos($this->params["report"], "-")) == "banking" || substr($this->params["report"], 0, strpos($this->params["report"], "-")) == "telecom") echo "selected";  ?>>Industry</option>
                        </select>
                    </div>
                    <div id="accordion-wrapper" style="width: 100%;">
                        <div class="accordion" id="capitalmarkets">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="equityHeading">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#equities" aria-expanded="true" aria-controls="equities" style="color:#632423;">
                                        <i class="fas fa-coins"></i> &nbsp; Equities
                                    </button>
                                </h2>
                                <div id="equities" class="accordion-collapse collapse <?php if (substr($this->params["report"], 0, strpos($this->params["report"], "-")) == "equity") echo "show";  ?>" aria-labelledby="equityHeading" data-bs-parent="#capitalmarkets">
                                    <div class="accordion-body">
                                        <ul class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-sm-start menu-reports">
                                            <li class="w-100 p-15">
                                                <a href="/insights/statistics/equity-nse-market-cap/period/Quarter/1y" class="accordion-link"> <span class="d-sm-inline">
                                                        - NSE market-cap (USD m)</span></a>
                                            </li>
                                            <li class="w-100 p-15">
                                                <a href="/insights/statistics/equity-nse-free-float/period/Quarter/1y" class="accordion-link"> <span class="d-sm-inline">
                                                        - NSE free-float (USD m)</span></a>
                                            </li>
                                            <li class="w-100 p-15">
                                                <a href="/insights/statistics/equity-nse-free-percent/period/Quarter/1y" class="accordion-link"> <span class="d-sm-inline">
                                                        - NSE free-float %</span></a>
                                            </li>
                                            <li class="w-100 p-15">
                                                <a href="/insights/statistics/equity-foreign-portfolio-holding/period/Quarter/1y" class="accordion-link"> <span class="d-sm-inline">
                                                        - Foreign porfolio holding (USD m)</span></a>
                                            </li>
                                            <li class="w-100 p-15">
                                                <a href="/insights/statistics/equity-foreign-portfolio-holding-percent/period/Quarter/1y" class="accordion-link"> <span class="d-sm-inline">
                                                        - Foreign porfolio holding % of NSE free-float</span></a>
                                            </li>
                                            <li class="w-100 p-15">
                                                <a href="/insights/statistics/equity-foreign-portfolio-holding-cbk/period/Quarter/1y" class="accordion-link"> <span class="d-sm-inline">
                                                        - Foreign portfolio holding % of CBK FX reserves</span></a>
                                            </li>
                                            <li class="w-100 p-15">
                                                <a href="/insights/statistics/equity-nse-turnover/period/Month/1y" class="accordion-link"> <span class="d-sm-inline">
                                                        - NSE turnover (USD m)</span></a>
                                            </li>
                                            <li class="w-100 p-15">
                                                <a href="/insights/statistics/equity-average-daily-traded/period/Month/1y" class="accordion-link"> <span class="d-sm-inline">
                                                        - Average daily value traded (USD m)</span></a>
                                            </li>
                                            <li class="w-100 p-15">
                                                <a href="/insights/statistics/equity-foreign-net-flow/period/Month/1y" class="accordion-link"> <span class="d-sm-inline">
                                                        - Foreign net flow (USD m)</span></a>
                                            </li>
                                            <li class="w-100 p-15">
                                                <a href="/insights/statistics/equity-local-pension-industry/period/Month/1y" class="accordion-link"> <span class="d-sm-inline">
                                                        - Local pension industry AUM (USD m)</span></a>
                                            </li>
                                            <li class="w-100 p-15">
                                                <a href="/insights/statistics/equity-local-pension-allocation/period/Month/1y" class="accordion-link"> <span class="d-sm-inline">
                                                        - Local pension industry equity allocation %</span></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="fixedincomeHeading">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#fixedincome" aria-expanded="false" aria-controls="fixedincome" style="color:#632423;">
                                        <i class="fas fa-cash-register"></i> &nbsp; Fixed Income
                                    </button>
                                </h2>
                                <div id="fixedincome" class="accordion-collapse collapse <?php if (substr($this->params["report"], 0, strpos($this->params["report"], "-")) == "fixed") echo "show";  ?>" aria-labelledby="fixedincomeHeading" data-bs-parent="#capitalmarkets">
                                    <div class="accordion-body">
                                        <ul class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-sm-start menu-reports">
                                            <li class="w-100 p-15">
                                                <a href="/insights/statistics/fixed-income-domestic-debt/period/Month/1y" class="accordion-link"> <span class="d-sm-inline">
                                                        - Outstanding government domestic debt (USDm)</span></a>
                                            </li>
                                            <li class="w-100 p-15">
                                                <a href="/insights/statistics/fixed-income-treasury-bills/period/Month/1y" class="accordion-link"> <span class="d-sm-inline">
                                                        - Treasury bills auction performance rate %</span></a>
                                            </li>
                                            <li class="w-100 p-15">
                                                <a href="/insights/statistics/fixed-income-treasury-bonds/period/Month/1y" class="accordion-link"> <span class="d-sm-inline">
                                                        - Treasury bonds auction perfromance rate %</span></a>
                                            </li>
                                            <li class="w-100 p-15">
                                                <a href="/insights/statistics/fixed-income-government-bond/period/Month/1y" class="accordion-link"> <span class="d-sm-inline">
                                                        - 10 year government bond yield %</span></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section id="footerSection" class="brown-bg py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-4" data-aos="fade-up">
                    <h4 class="text-light mb-4">Useful Links</h4>
                    <ul class="footer_list">
                        <li>
                            <a target="_blank" href="https://www.nse.co.ke/">
                                Nairobi Securities Exchange
                            </a>
                        </li>
                        <li>
                            <a target="_blank" href="https://www.cma.or.ke/">
                                Capital Markets Authority
                            </a>
                        </li>
                        <li>
                            <a target="_blank" href="https://www.cdsckenya.com/">
                                Central Depository & Settlement Corporation
                            </a>
                        </li>
                        <li>
                            <a target="_blank" href="https://www.centralbank.go.ke/">
                                Central Bank of Kenya
                            </a>
                        </li>
                        <li>
                            <a target="_blank" href="http://www.knbs.or.ke/">
                                Kenya National Bureau of Statistics
                            </a>
                        </li>
                        <li>
                            <a target="_blank" href="https://www.rba.go.ke/">
                                Retirement Benefits Authority
                            </a>
                        </li>

                    </ul>
                </div>
                <div class="col-md-4" data-aos="fade-up">
                    <h4 class="text-light mb-4">Contact Us</h4>
                    <div class="contact-list">
                        <div class="icon-section">
                            <svg height="20" viewBox="0 0 512 512" width="20" xmlns="http://www.w3.org/2000/svg">
                                <path d="m256 75c-24.8125 0-45 20.1875-45 45s20.1875 45 45 45 45-20.1875 45-45-20.1875-45-45-45zm0 0" />
                                <path d="m256 0c-66.167969 0-120 53.832031-120 120 0 22.691406 6.371094 44.796875 18.429688 63.925781l101.570312 162.074219 101.570312-162.074219c12.058594-19.128906 18.429688-41.234375 18.429688-63.925781 0-66.167969-53.832031-120-120-120zm0 195c-41.351562 0-75-33.648438-75-75s33.648438-75 75-75 75 33.648438 75 75-33.648438 75-75 75zm0 0" />
                                <path d="m182.996094 512h145.957031l-11.535156-91h-123.175781zm0 0" />
                                <path d="m197.992188 391h50.914062l-42.488281-67.386719zm0 0" />
                                <path d="m343.828125 391h118.175781l-37.5-90h-92.21875zm0 0" />
                                <path d="m49.996094 391h117.765625l11.25-90h-91.515625zm0 0" />
                                <path d="m263.09375 391h50.476562l-8.527343-66.523438zm0 0" />
                                <path d="m164.011719 421h-126.515625l-37.496094 91h152.765625zm0 0" />
                                <path d="m474.503906 421h-126.832031l11.539063 91h152.789062zm0 0" />
                            </svg>
                        </div>
                        <div class="footer_text">

                            Kestrel Capital (East Africa) Ltd<br>
                            2nd Floor, Orbit Place<br>
                            Westlands Road<br>
                            P.O. Box 40005 – 00100<br>
                            Nairobi, Kenya<br>
                        </div>
                    </div>

                    <!--  -->
                    <div class="contact-list mt-2">
                        <div class="icon-section">
                            <svg height="20" width="20" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                <g>
                                    <g>
                                        <path d="M436.992,74.953c-99.989-99.959-262.08-99.935-362.039,0.055s-99.935,262.08,0.055,362.039s262.08,99.935,362.039-0.055
			c48.006-48.021,74.968-113.146,74.953-181.047C511.986,188.055,485.005,122.951,436.992,74.953z M387.703,356.605
			c-0.011,0.011-0.022,0.023-0.034,0.034v-0.085l-12.971,12.885c-16.775,16.987-41.206,23.976-64.427,18.432
			c-23.395-6.262-45.635-16.23-65.877-29.525c-18.806-12.019-36.234-26.069-51.968-41.899
			c-14.477-14.371-27.483-30.151-38.827-47.104c-12.408-18.242-22.229-38.114-29.184-59.051
			c-7.973-24.596-1.366-51.585,17.067-69.717l15.189-15.189c4.223-4.242,11.085-4.257,15.326-0.034
			c0.011,0.011,0.023,0.022,0.034,0.034l47.957,47.957c4.242,4.223,4.257,11.085,0.034,15.326c-0.011,0.011-0.022,0.022-0.034,0.034
			l-28.16,28.16c-8.08,7.992-9.096,20.692-2.389,29.867c10.185,13.978,21.456,27.131,33.707,39.339
			c13.659,13.718,28.508,26.197,44.373,37.291c9.167,6.394,21.595,5.316,29.525-2.56l27.221-27.648
			c4.223-4.242,11.085-4.257,15.326-0.034c0.011,0.011,0.022,0.022,0.034,0.034l48.043,48.128
			C391.911,345.502,391.926,352.363,387.703,356.605z" />
                                    </g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                            </svg>
                        </div>
                        <div class="footer_text">
                            254 (0) 20 2251758 <br>
                            254 (0) 20 2251893
                        </div>
                    </div>

                    <!--  -->
                    <div class="contact-list mt-2">
                        <div class="icon-section">
                            <svg height="20" width="20" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 27.442 27.442" style="enable-background:new 0 0 27.442 27.442;" xml:space="preserve">
                                <g>
                                    <path d="M19.494,0H7.948C6.843,0,5.951,0.896,5.951,1.999v23.446c0,1.102,0.892,1.997,1.997,1.997h11.546
		c1.103,0,1.997-0.895,1.997-1.997V1.999C21.491,0.896,20.597,0,19.494,0z M10.872,1.214h5.7c0.144,0,0.261,0.215,0.261,0.481
		s-0.117,0.482-0.261,0.482h-5.7c-0.145,0-0.26-0.216-0.26-0.482C10.612,1.429,10.727,1.214,10.872,1.214z M13.722,25.469
		c-0.703,0-1.275-0.572-1.275-1.276s0.572-1.274,1.275-1.274c0.701,0,1.273,0.57,1.273,1.274S14.423,25.469,13.722,25.469z
		 M19.995,21.1H7.448V3.373h12.547V21.1z" />
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                            </svg>

                        </div>
                        <div class="footer_text">
                            254 (0) 722205897 <br>
                            254 (0) 733333544
                        </div>
                    </div>

                    <!--  -->
                    <div class="contact-list mt-2">
                        <div class="icon-section">
                            <!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                            <svg version="1.1" height="20" width="20" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                <g>
                                    <g>
                                        <polygon points="339.392,258.624 512,367.744 512,144.896 		" />
                                    </g>
                                </g>
                                <g>
                                    <g>
                                        <polygon points="0,144.896 0,367.744 172.608,258.624 		" />
                                    </g>
                                </g>
                                <g>
                                    <g>
                                        <path d="M480,80H32C16.032,80,3.36,91.904,0.96,107.232L256,275.264l255.04-168.032C508.64,91.904,495.968,80,480,80z" />
                                    </g>
                                </g>
                                <g>
                                    <g>
                                        <path d="M310.08,277.952l-45.28,29.824c-2.688,1.76-5.728,2.624-8.8,2.624c-3.072,0-6.112-0.864-8.8-2.624l-45.28-29.856
			L1.024,404.992C3.488,420.192,16.096,432,32,432h448c15.904,0,28.512-11.808,30.976-27.008L310.08,277.952z" />
                                    </g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                            </svg>


                        </div>
                        <div class="footer_text">
                            info@kestrelcapital.com
                        </div>
                    </div>

                </div>
                <div class="col-md-4" data-aos="fade-up">
                    <h4 class="text-light mb-4 visit-us">Visit Us</h4>

                    <div class="mapouter">
                        <div class="gmap_canvas">
                            <iframe width="350" height="250" id="gmap_canvas" src="https://maps.google.com/maps?q=Orbit%20Place&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><br>
                            <style>
                                .mapouter {
                                    position: relative;
                                    text-align: right;
                                    height: 250px;
                                    width: 100%;
                                }
                            </style>
                            <style>
                                .gmap_canvas {
                                    overflow: hidden;
                                    background: none !important;
                                    height: 250px;
                                    width: 98%;
                                    border-radius: 10px;
                                }
                            </style>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <section class="brown-bg py-3">
        <div class="container copyright-section">
            <div class="row">
                <div class="col-md-12 text-light">
                    <div class="marg text-center small-text">
                        Copyright <?= date('Y') ?> Kestrel Capital
                    </div>

                </div>

            </div>
        </div>
    </section>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>
    <!-- Core theme JS-->
    <!-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->
    <!-- <script src="/js/scripts.js"></script> -->
    <script src="/js/kestrel.js"></script>
</body>

</html>
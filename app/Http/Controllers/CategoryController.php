<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

use App\Models\Category;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{

    const CATEGORIES_BLADE = 'admin/categories';
    const CATEGORIES_HOME = '/admin/elements/categories';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $categories = Category::all();
        return view(
            CategoryController::CATEGORIES_BLADE,
            [
                'categories' => $categories,
                'selectedCategory' => null
            ]
        );
    }

    public function save(Request $request)
    {

        $data = $request->validate([
            'category_name' => 'required', 'string', 'max:255',
            'ui_position' => ['required', 'string', 'max:255'],
        ]);

        $data['user_id'] = Auth::user()->id;
        $data['status'] = 1;
        $data['category_slug'] = $this->generateSlug($data['category_name']);

        $selected = $request->selected_category;

        if ($selected) {
            tap(Category::where('category_id', $selected)->update($data));
        } else {
            tap(new Category($data))->save();
        }

        Session::flash('success', 'Category details saved successfully!');
        return redirect(self::CATEGORIES_HOME);
    }

    public function search(Request $request)
    {

        $categories = Category::where(
            'category_name',
            'like',
            '%' . $request->keyword . '%'
        )->get();
        return json_encode($categories);
    }

    public function edit($id)
    {
        $categoryId = $id;
        $selectedCategory = Category::find($categoryId);
        $categories = Category::all();

        return view(
            CategoryController::CATEGORIES_BLADE,
            [
                'categories' => $categories,
                'selectedCategory' => $selectedCategory
            ]
        );
    }

    public function del(Request $request)
    {

        $categoryId = $request->deletecategory;
        $category = Category::find($categoryId);

        if ($category->subCategories()->exists()) {
            Session::flash('error', 'Cannot delete a category that has dependent sub categories!');
            return redirect()->back();
        }
        if ($category->elems()->exists()) {
            Session::flash('error', 'Cannot delete a category that has dependent data elements!');
            return redirect()->back();
        }

        $category->delete();

        Session::flash('success', 'Category deleted successfully!');

        return redirect(self::CATEGORIES_HOME);
    }

    private function generateSlug($name)
    {

        while (strpos($name, " ") !== false) {
            $name = str_replace(" ", "-", $name);
        }
        while (strpos($name, "_") !== false) {
            $name = str_replace("_", "-", $name);
        }
        return strtolower($name);
    }
}

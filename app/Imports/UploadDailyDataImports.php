<?php

namespace App\Imports;

use App\Models\UploadDailyData;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class UploadDailyDataImports implements ToModel, WithHeadingRow, WithValidation
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {

        return new UploadDailyData([
            'date'     => $row['Date'],
            'value'    => $row['Price (KES)'],
        ]);
    }

    public function rules(): array
    {
        return [
            'date' => 'required|string',
            '1' => 'required|numeric'
        ];
    }
}

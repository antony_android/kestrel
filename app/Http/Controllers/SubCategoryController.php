<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

use App\Models\SubCategory;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;

class SubCategoryController extends Controller
{

    const SUBCATEGORIES_BLADE = 'admin/sub_categories';
    const SUBCATEGORIES_HOME = '/admin/elements/sub-categories';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $subCategories = SubCategory::all();
        $categories = Category::all();

        return view(
            self::SUBCATEGORIES_BLADE,
            [
                'subCategories' => $subCategories,
                'categories' => $categories,
                'selectedSubCategory' => null
            ]
        );
    }

    public function save(Request $request)
    {

        $data = $request->validate([
            'sub_category_name' => 'required', 'string', 'max:255',
            'category_id' => ['required'],
        ]);

        $data['user_id'] = Auth::user()->id;
        $data['status'] = 1;
        $data['icon_class'] = $request->icon_class;
        $data['sub_category_slug'] = $this->generateSlug($data['sub_category_name']);

        $selected = $request->selected_subcategory;

        if ($selected) {
            tap(SubCategory::where('sub_category_id', $selected)->update($data));
        } else {
            tap(new SubCategory($data))->save();
        }

        Session::flash('success', 'Sub Category details saved successfully!');
        return redirect(self::SUBCATEGORIES_HOME);
    }

    public function search(Request $request)
    {

        $subCategories = DB::select(Db::raw("select * from sub_category inner join category  
            on category.category_id = sub_category.category_id where sub_category_name
             like '%" . $request->keyword . "%' or category_name like '%" . $request->keyword . "%'"));

        return json_encode($subCategories);
    }

    public function edit($id)
    {
        $categoryId = $id;
        $selectedSubCategory = SubCategory::find($categoryId);
        $subCategories = SubCategory::all();
        $categories = Category::all();

        return view(
            self::SUBCATEGORIES_BLADE,
            [
                'subCategories' => $subCategories,
                'categories' => $categories,
                'selectedSubCategory' => $selectedSubCategory
            ]
        );
    }

    public function del(Request $request)
    {

        $subCategoryId = $request->deletesubcategory;
        $subCategory = SubCategory::find($subCategoryId);

        if ($subCategory->elems()->exists()) {
            Session::flash('error', 'Cannot delete a Sub Category that has dependent data elements!');
            return redirect()->back();
        }
        $subCategory->delete();

        Session::flash('success', 'Sub Category deleted successfully!');

        return redirect(self::SUBCATEGORIES_HOME);
    }

    private function generateSlug($name)
    {

        while (strpos($name, " ") !== false) {
            $name = str_replace(" ", "-", $name);
        }
        while (strpos($name, "_") !== false) {
            $name = str_replace("_", "-", $name);
        }
        return strtolower($name);
    }
}

@extends('layouts.app')

@section('content')
<div class="row justify-content-center m-15">
    <div class="col-sm-10">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        Users
                    </div>
                    <div class="col-6">
                        <button class="btn btn-save btn-add" onclick="formCollapseHandler('add-user-form')">New User</button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form id="add-user-form" method="POST" action="/admin/users/save">
                    @csrf
                    @if($selectedUser != null)
                    <input type="hidden" id="selected_user" name="selected_user" value="{{$selectedUser->id}}" />
                    @endif
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6 users-f">
                                <div class="form-group">
                                    <label for="name">{{ __('Name') }} <i class="fas fa-asterisk asterisk"></i></label>
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="@if($selectedUser != null){{$selectedUser->name}}@else{{ old('name') }}@endif" required autocomplete="name" autofocus>
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6 users-f">
                                <div class="form-group">
                                    <label for="user_role_id">{{ __('Role') }} <i class="fas fa-asterisk asterisk"></i></label>
                                    <select name="user_role_id" id="user_role_id" required class="form-control @error('user_type') is-invalid @enderror" required autofocus>
                                        <option value=""></option>
                                        @foreach($user_roles as $role)
                                        @if($selectedUser != null)
                                        <option @if($selectedUser->user_role_id == $role->user_role_id) selected @endif value="{{$role->user_role_id}}">{{$role->role}}</option>
                                        @else
                                        <option value="{{$role->user_role_id}}">{{$role->role}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                    @error('user_role_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 users-f">
                                <div class="form-group">
                                    <label for="email">{{ __('E-Mail Address') }} <i class="fas fa-asterisk asterisk"></i></label>
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="@if($selectedUser != null){{$selectedUser->email}}@else{{ old('email') }}@endif" required autocomplete="email">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6 users-f">
                                <div class="form-group">
                                    <label for="status">{{ __('Status') }} <i class="fas fa-asterisk asterisk"></i></label>
                                    <select class="form-control @error('status') is-invalid @enderror" id="status" name="status">
                                        @if($selectedUser != null)
                                        <option @if($selectedUser->status == '1') selected @endif value="1">Approved & Active</option>
                                        <option @if($selectedUser->status == '0') selected @endif value="0">Unapproved</option>
                                        <option @if($selectedUser->status == '2') selected @endif value="2">Locked</option>
                                        @else
                                        <option value="1">Approved & Active</option>
                                        <option value="0">Unapproved</option>
                                        <option value="2">Locked</option>
                                        @endif
                                    </select>
                                    @error('status')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                        </div>

                        @if($selectedUser == null)
                        <div class="row">
                            <div class="col-sm-6 users-f">
                                <div class="form-group">
                                    <label for="password">{{ __('Password') }} <i class="fas fa-asterisk asterisk"></i></label>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-6 users-f">
                                <div class="form-group">
                                    <label for="password-confirm">{{ __('Confirm Password') }} <i class="fas fa-asterisk asterisk"></i></label>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                </div>
                            </div>
                        </div>
                        @endif

                        <div class="form-group row mb-0" style="margin-top: 5px;">
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Save') }}
                                </button>
                                @if($selectedUser != null)
                                <a class="btn cancel-bt bt-margin" href="/admin/users">
                                    Cancel
                                </a>
                                @endif
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="container cnt-bd">
                <div class="row search-mg">
                    <div class="col-md-12 search-col">
                        <button class="btn float-right search-bt" onclick="searchUsers()">Search</button>
                        <input type="text" class="form-control float-right search-txt" id="search-user" name="search-user" placeholder="Search" />
                    </div>
                </div>
                <div class="row t-header">
                    <div class="col-3">Name</div>
                    <div class="col-4">Email Address</div>
                    <div class="col-2">Role</div>
                    <div class="col-1">Status</div>
                    <div class="col-2"></div>
                </div>
                <div id="users-data">
                    @foreach ($users as $user)
                    <div class="row t-content">
                        <div class="col-3">
                            {{ $user->name }}
                        </div>
                        <div class="col-4">
                            {{ $user->email }}
                        </div>
                        <div class="col-2">
                            {{ $user->userRole->role }}
                        </div>
                        <div class="col-1">
                            <?php $userStatus = "Deleted"; ?>
                            @if( $user->status == 0 )
                            <?php $userStatus = "Inactive"; ?>
                            @elseif( $user->status == 1 )
                            <?php $userStatus = "Active"; ?>
                            @elseif( $user->status == 2 )
                            <?php $userStatus = "Locked"; ?>
                            @endif
                            {{ $userStatus }}
                        </div>
                        <div class="col-2 edit-butts">
                            <span>
                                <a href="/admin/users/edit/{{$user->id}}">
                                    <img src="/img/edit.png" width="15px" alt="Edit" />
                                </a>
                                <button rel="{{$user->id}}" class="btn deluser" data-bs-toggle="modal" data-bs-target="#usersModal">
                                    <img src="/img/delete.png" width="15px" alt="Delete" />
                                </button>
                                @if($user->approved_by == null || $user->status == 0)
                                <a href="/admin/users/approve/{{$user->id}}" title="Approve User">
                                    <img src="/img/approve.png" width="15px" alt="Approve" />
                                </a>
                                @endif
                            </span>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="d-flex justify-content-center">
                    {!! $users->links() !!}
                </div>
            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="usersModal" tabindex="-1" role="dialog" aria-labelledby="usersModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="usersModalLabel">Confirm Delete</h5>
                    <button type="button" class="btn-close bt-close" data-bs-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    Are you sure you would like to delete the selected user?
                </div>
                <div class="modal-footer">
                    <div class="row" style="width:100%">
                        <div class="col-8">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                        </div>
                        <div class="col-4">
                            <form action="/admin/users/del" method="post">
                                @csrf
                                <input type="hidden" id="del-user" name="deleteuser" />
                                <button type="submit" class="btn btn-primary">Confirm</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@if(!is_null($selectedUser))
<script>
    $(document).ready(function() {

        $('#add-user-form').show();
    });
</script>
@endif
@endsection
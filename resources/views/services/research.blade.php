@extends('layouts.app')
<!-- Carousel section -->
@section('content')
<section id="team-top-section">
	<div class="py-5 brown-bg ">
		<h3 class="text-light text-center"> <strong>WHAT WE DO</strong> </h3>
		<h1 class="text-light text-center"> <strong>Research</strong> </h1>
	</div>
	<div class="overflow-sec brown-bg">

	</div>
	<div class="container">

		<div class="row">
			<div class="col-md-12 mb-4">
				<div class="card team-wrap shadow pb-4">
					<div class="card-body ">
						<div class="p-4 mb-4">
							<p class="mb-0 box-header">
								We offer well curated, timely, insights on companies, industries and equity & fixed income markets. Our strength lies in our localized knowledge, excellent access to both listed & non-listed companies and key Government agencies. </p>

						</div>
						<div class="row services-font">
							<div class="col-md-6">
								<p>
									Kestrel Capital pioneered fundamental market and company research in Kenya in 1995. </p>

								<p>Our clients can easily access our research & customize what to receive, via our online research platform that offers access to; research publications, company annual reports, company investor presentations, company conference call transcripts, economic & fixed income datasets and bespoke notes from management meetings, all going as far back as 20 years.
								</p>
							</div>
							<div class="col-md-6">
								<h3>Services</h3>
								<ul class="small-text">
									<li>Industry & Company reports – covering 90% of the NSE market capitalization</li>
									<li>
										Corporate access for listed and non-listed companies in East Africa through conference calls, meeting with management, country visits and management roadshows
									</li>
									<li>
										Analyst calls - trading idea generation
									</li>
									<li>
										Analyst local and international roads shows
									</li>
									<li>Investor relations support – we work closely with IR teams to build company visibility amongst investors and offer customized reports detailing investor trading patterns</li>

								</ul>
							</div>

						</div>


						<h3>Research Publication</h3>
						<div class="table-section">
							<table class="table table-striped table-bordered small-text">
								<thead>
									<th>
										Research report type
									</th>
									<th>
										Publication frequency
									</th>
									<th>
										Content
									</th>
								</thead>
								<tbody>
									<tr>
										<th>Morning Brief</th>
										<td>Every morning</td>
										<td>Key economic and company news. Specific focus on answering “why is this news important, and what action should investors take”. </td>
									</tr>
									<tr>
										<th>Evening Daily Brief</th>
										<td>Every day at close of trading</td>
										<td>Review of equity and FX market performance and key corporate and economic news. Focus being on answering “what should an investor know before the opening of the next trading session”. </td>
									</tr>
									<tr>
										<th>Weekly Equity, Fixed Income and FX Markets Report</th>
										<td>Every Monday</td>
										<td>Summary of key weekly economic, company & fixed income news and trading trend over the week – “identify short-term trading trends, drivers and themes”. </td>
									</tr>
									<tr>
										<th>Monthly Report</th>
										<td>First week of every new month</td>
										<td>Summary equity and fixed income trading statistics for the month highlighting; key price movements, turnover activity, foreign investor net flows and SSA market performance analysis – “medium-term outlook for equity and fixed income markets”.</td>
									</tr>
									<tr>
										<th>Company results announcements brief</th>
										<td>Within 24 hours of results announcement</td>
										<td>Summary of performance, analysis of performance against forecasts and change if any, in analyst investment recommendation.</td>
									</tr>
									<tr>
										<th>Kenya Investment Strategy </th>
										<td>Twice a year – January & July</td>
										<td>Economic, equity and fixed income markets review and outlook – “NSE All Share index return forecast and view on equity & fixed income portfolio allocation”. </td>
									</tr>
									<tr>
										<th>Sector & Company Valuation reports </th>
										<td>All Fair Values/analyst recommendations are no more than 6 months old</td>
										<td>Sector outlook, company performance forecasts, fair value estimates and peer company relative analysis.</td>
									</tr>
									<tr>
										<th>Special Reports </th>
										<td>Ad-hoc</td>
										<td>Focus on key news that is relevant to investors e.g. key regulatory changes, M&A activity, outlook on government debt sustainability, changes in tax laws etc.</td>
									</tr>


								</tbody>
							</table>

							<a href="https://web.kestrelcapital.com/" target="_blank">
								<button type="button" class="btn btn-success " style="background-color: #402B1B; border-color: #402B1B; color: #fff; font-weight: lighter;">Research Portal</button>
							</a>
						</div>
					</div>
				</div>

			</div>



		</div>
	</div>
</section>
@endsection
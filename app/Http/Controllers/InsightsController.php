<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class InsightsController extends Controller
{
	public function sampleresearch()
	{
		$recommendation = DB::connection('mysql2')->select("SELECT * FROM kestra.recommendation 
		WHERE public=1 ORDER BY recommendation_id DESC LIMIT 5");
		return view('insights.sampleresearch')->with('recommendation', $recommendation);
	}

	public function savemail(Request $request)
	{
		DB::insert('INSERT INTO mailing_list (email, created_at) VALUES (?, ?)', 
		[$request->email, date('Y-m-d H:i:s')]);

		return redirect('/insights/sampleresearch?show_mailing_list=false');
	}
}

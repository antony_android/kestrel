@extends('layouts.app')
@section('content')

<div class="row justify-content-center m-15">

    <div class="col-sm-10">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        Data Elements
                    </div>
                    <div class="col-6">
                        <button class="btn btn-save btn-add" onclick="formCollapseHandler('add-element-form')">New Element</button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form action="/admin/elements/save" method="post" id="add-element-form">
                    <div class="container">
                        <div class="row">
                            @csrf
                            @if ($errors->any())
                            <div class="alert alert-danger" role="alert">
                                Please fix the following errors
                            </div>
                            @endif
                            @if($selectedElement != null)
                            <input type="hidden" id="selected_element" name="selected_element" value="{{$selectedElement->elem_id}}" />
                            @endif
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="category_id">Category <i class="fas fa-asterisk asterisk"></i></label>
                                    <select class="form-control @error('category_id') is-invalid @enderror" id="category_id" name="category_id">
                                        <option value="">Select Category</option>
                                        @foreach($categories as $category)
                                        @if($selectedElement != null)
                                        <option @if($selectedElement->category_id == $category->category_id) selected @endif value="{{$category->category_id}}">{{$category->category_name}}</option>
                                        @else
                                        <option value="{{$category->category_id}}">{{$category->category_name}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                    @error('category_id')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="sub_category_id">Sub Category <i class="fas fa-asterisk asterisk"></i></label>
                                    <select class="form-control @error('sub_category_id') is-invalid @enderror" id="sub_category_id" name="sub_category_id">
                                        <option value="">Select Sub Category</option>
                                        @foreach($subCategories as $subCategory)
                                        @if($selectedElement != null)
                                        <option @if($selectedElement->sub_category_id == $subCategory->sub_category_id) selected @endif value="{{$subCategory->sub_category_id}}">{{$subCategory->sub_category_name}}</option>
                                        @else
                                        <option value=""></option>
                                        @endif
                                        @endforeach
                                    </select>
                                    @error('sub_category_id')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="elem_description">Element Name <i class="fas fa-asterisk asterisk"></i></label>
                                    <input type="text" class="form-control @error('elem_description') is-invalid @enderror" id="elem_description" name="elem_description" placeholder="Name" required value="@if($selectedElement != null){{$selectedElement->elem_description}}@else{{ old('elem_description') }}@endif">
                                    @error('elem_description')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="ui_position">UI Position <i class="fas fa-asterisk asterisk"></i></label>
                                    <select class="form-control @error('ui_position') is-invalid @enderror" id="ui_position" name="ui_position">
                                        <option value="">Select UI Position</option>
                                        <option value="statistics" @if($selectedElement !=null) @if($selectedElement->ui_position == 'statistics') selected @endif @endif>Statistics</option>
                                        <option value="price-trends" @if($selectedElement !=null) @if($selectedElement->ui_position == 'price-trends') selected @endif @endif>Price Trends</option>
                                    </select>
                                    @error('ui_position')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="priority">Priority</label>
                                    <input type="text" class="form-control @error('priority') is-invalid @enderror" id="priority" name="priority" placeholder="Priority (Larger Numbers have Higher Priority" required value="@if($selectedElement != null){{$selectedElement->priority}}@else{{ old('priority') }}@endif">
                                    @error('priority')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-primary bt-margin">Save</button>
                                @if($selectedElement != null)
                                <a class="btn cancel-bt bt-margin" href="/admin/elements">
                                    Cancel
                                </a>
                                @endif
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="container cnt-bd">
                <div class="row search-mg">
                    <div class="col-md-12 search-col">
                        <button class="btn float-right search-bt" onclick="searchElements()">Search</button>
                        <input type="text" class="form-control float-right search-txt" id="search-element" name="search-element" placeholder="Search" />
                    </div>
                </div>
                <div class="row t-header">
                    <div class="col-sm-6">Name</div>
                    <div class="col-sm-2">Sub Category</div>
                    <div class="col-sm-2">Category</div>
                    <div class="col-sm-2"></div>
                </div>
                <div id="elements-data">
                    @foreach ($elements as $element)
                    <div class="row t-content">
                        <div class="col-sm-6">
                            {{ $element->elem_description }}
                        </div>
                        <div class="col-sm-2">
                            {{ $element->subCategory->sub_category_name }}
                        </div>
                        <div class="col-sm-2">
                            {{ $element->category->category_name }}
                        </div>
                        <div class="col-sm-2 edit-butts">
                            <span>
                                <a href="/admin/elements/edit/{{$element->elem_id}}">
                                    <img src="/img/edit.png" width="15px" alt="Edit" />
                                </a>
                                <button rel="{{$element->elem_id}}" class="btn delelement" data-bs-toggle="modal" data-bs-target="#elementsModal">
                                    <img src="/img/delete.png" width="15px" alt="Delete" />
                                </button>
                            </span>
                        </div>
                    </div>
                    @endforeach
                    <div class="d-flex justify-content-center">
                        {!! $elements->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="elementsModal" tabindex="-1" role="dialog" aria-labelledby="elementsModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="elementsModalLabel">Confirm Delete</h5>
                    <button type="button" class="btn-close bt-close" data-bs-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    Are you sure you would like to delete the selected element?
                </div>
                <div class="modal-footer">
                    <div class="row" style="width:100%">
                        <div class="col-8">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                        </div>
                        <div class="col-4">
                            <form action="/admin/elements/del" method="post">
                                @csrf
                                <input type="hidden" id="del-element" name="deleteelement" />
                                <button type="submit" class="btn btn-primary">Confirm</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@if(!is_null($selectedElement))
<script>
    $(document).ready(function() {

        $('#add-element-form').show();
    });
</script>
@else
<script>
    function getSubCategories() {
        var category_id = $("#category_id").val();

        $.ajax({
            type: "POST",
            data: {
                "_token": $('meta[name="csrf-token"]').attr('content'),
                "category_id": category_id
            },
            url: "/admin/elements/sub-categories",
            success: function(data) {
                data = $.parseJSON(data);
                let html = '<option value=""></option>';
                for (let item of data) {
                    let subCategoryId = item.sub_category_id;
                    let name = item.sub_category_name;
                    html += '<option value="' + subCategoryId + '">' + name + '</option>';
                }
                $("#sub_category_id").html(html);
            }
        });
    }

    $("#category_id").change(
        function() {
            getSubCategories();
        }
    );
</script>
@endif
@endsection
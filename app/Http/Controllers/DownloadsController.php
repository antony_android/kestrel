<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  Session;

class DownloadsController extends Controller
{
    //


    public function download(Request $request, $file_name) {

   $url = 'https://web.kestrelcapital.com/assets/uploads/files/'.$file_name;

   $file_headers = get_headers($url);

	if($file_headers[0] == 'HTTP/1.1 404 Not Found'){

     // echo "The file $file_name does not exist";
      //$request->session()->flash('flash_message', 'File  was not  found');
		Session::flash('flash_message', 'File  was not  found');
		Session::flash('flash_type', 'alert-danger');
      return  redirect()->back();

	}  else {

	header('Content-Type: application/octet-stream');
	header("Content-Transfer-Encoding: Binary"); 
	header("Content-disposition: attachment; filename=\"".$file_name."\""); 
	$file=  readfile($url);
	exit;

	}
	
}



}

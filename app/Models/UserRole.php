<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $user_role_id
 * @property string $role
 * @property string $created
 * @property string $modified
 * @property integer $status
 * @property int $created_by
 * @property User[] $users
 */
class UserRole extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'user_role';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'user_role_id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['role', 'created', 'modified', 'status', 'created_by'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany('App\\Models\User', null, 'user_role_id');
    }
}

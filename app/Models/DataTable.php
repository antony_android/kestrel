<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $elem_id
 * @property string data_table_type
 * @property string $elem_description
 * @property integer year
 * @property string $month
 * @property int $month_id
 * @property string $time_of_trade
 * @property string $period
 * @property int $period_id
 * @property string $data_value
 * @property integer $category_id
 * @property integer $sub_category_id
 * @property string $ui_position
 * @property string $slug
 * @property string $default_period
 * @property string $default_period_slug
 * @property string $default_period_duration
 * @property string $created
 * @property string $modified
 * @property integer $user_id
 * @property integer $upload_id
 * @property Category $category
 * @property SubCategory $subCategory
 */

class DataTable extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'data_table';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'data_table_id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'data_table_id', 'data_table_type', 'element_description',
        'month', 'month_id', 'time_of_trade', 'period', 'year',
        'period_id', 'data_value', 'category_id', 'upload_id',
        'sub_category_id', 'ui_position', 'slug', 'default_period',
        'default_period_slug', 'default_period_duration', 'user_id',
        'created', 'modified'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id', 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subCategory()
    {
        return $this->belongsTo('App\Models\SubCategory', 'sub_category_id', 'sub_category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function elem()
    {
        return $this->belongsTo('App\Models\Elem', 'slug', 'slug');
    }
}

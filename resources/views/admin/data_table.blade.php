@extends('layouts.app')

@section('content')
<div class="row justify-content-center m-15">
    <div class="col-sm-10">
        <div class="card" style="padding-bottom: 10px;">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        Data Records
                    </div>
                    <div class="col-6">
                        <button class="btn btn-save btn-add" style="background: #0A66C2;" onclick="formCollapseHandler('upload-data-form')">Upload Data Set</button>
                        <button class="btn btn-save btn-add" style="margin-right: 10px;" onclick="formCollapseHandler('add-data-form')">New Record</button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form id="add-data-form" class="form-upload" method="POST" action="/admin/insights/save">
                    @csrf
                    @if($selectedData != null)
                    <input type="hidden" id="selected_data" name="selected_data" value="{{$selectedData->data_table_id}}" />
                    @endif
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="category_id">Data Element <i class="fas fa-asterisk asterisk"></i></label>
                                    <select class="form-control @error('elem_id') is-invalid @enderror" id="elem_id" name="elem_id">
                                        <option value="">Select Data Element</option>
                                        @foreach($elements as $element)
                                        @if($selectedData != null)
                                        <option @if($selectedData->element_description == $element->elem_description) selected @endif value="{{$element->elem_id}}">{{$element->elem_description}}</option>
                                        @else
                                        <option value="{{$element->elem_id}}">{{$element->elem_description}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                    @error('elem_id')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="year">Year (Ignore if N/A)</label>
                                    <select class="form-control @error('year') is-invalid @enderror" id="year" name="year">
                                        <option value="">Select Year</option>
                                        <option value="2009" @if($selectedData !=null) @if($selectedData->year == '2009') selected @endif @endif>2009</option>
                                        <option value="2010" @if($selectedData !=null) @if($selectedData->year == '2010') selected @endif @endif>2010</option>
                                        <option value="2011" @if($selectedData !=null) @if($selectedData->year == '2011') selected @endif @endif>2011</option>
                                        <option value="2012" @if($selectedData !=null) @if($selectedData->year == '2012') selected @endif @endif>2012</option>
                                        <option value="2013" @if($selectedData !=null) @if($selectedData->year == '2013') selected @endif @endif>2013</option>
                                        <option value="2014" @if($selectedData !=null) @if($selectedData->year == '2014') selected @endif @endif>2014</option>
                                        <option value="2015" @if($selectedData !=null) @if($selectedData->year == '2015') selected @endif @endif>2015</option>
                                        <option value="2016" @if($selectedData !=null) @if($selectedData->year == '2016') selected @endif @endif>2016</option>
                                        <option value="2017" @if($selectedData !=null) @if($selectedData->year == '2017') selected @endif @endif>2017</option>
                                        <option value="2018" @if($selectedData !=null) @if($selectedData->year == '2018') selected @endif @endif>2018</option>
                                        <option value="2019" @if($selectedData !=null) @if($selectedData->year == '2019') selected @endif @endif>2019</option>
                                        <option value="2020" @if($selectedData !=null) @if($selectedData->year == '2020') selected @endif @endif>2020</option>
                                        <option value="2021" @if($selectedData !=null) @if($selectedData->year == '2021') selected @endif @endif>2021</option>
                                        <option value="2022" @if($selectedData !=null) @if($selectedData->year == '2022') selected @endif @endif>2022</option>
                                        <option value="2023" @if($selectedData !=null) @if($selectedData->year == '2023') selected @endif @endif>2023</option>
                                        <option value="2024" @if($selectedData !=null) @if($selectedData->year == '2024') selected @endif @endif>2024</option>
                                        <option value="2025" @if($selectedData !=null) @if($selectedData->year == '2025') selected @endif @endif>2025</option>
                                        <option value="2026" @if($selectedData !=null) @if($selectedData->year == '2026') selected @endif @endif>2026</option>
                                        <option value="2027" @if($selectedData !=null) @if($selectedData->year == '2027') selected @endif @endif>2027</option>
                                        <option value="2028" @if($selectedData !=null) @if($selectedData->year == '2028') selected @endif @endif>2028</option>
                                        <option value="2029" @if($selectedData !=null) @if($selectedData->year == '2029') selected @endif @endif>2029</option>
                                        <option value="2030" @if($selectedData !=null) @if($selectedData->year == '2030') selected @endif @endif>2030</option>
                                    </select>
                                    @error('year')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="period">Quarter (Ignore if N/A)</label>
                                    <select class="form-control @error('period') is-invalid @enderror" id="period" name="period">
                                        <option value="">Select Quarter</option>
                                        <option value="1" @if($selectedData !=null) @if(substr($selectedData->period, 0,1) == 1) selected @endif @endif>First</option>
                                        <option value="2" @if($selectedData !=null) @if(substr($selectedData->period, 0,1) == 2) selected @endif @endif>Second</option>
                                        <option value="3" @if($selectedData !=null) @if(substr($selectedData->period, 0,1) == 3) selected @endif @endif>Third</option>
                                        <option value="4" @if($selectedData !=null) @if(substr($selectedData->period, 0,1) == 4) selected @endif @endif>Fourth</option>
                                    </select>
                                    @error('period')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="month">Month (Ignore if N/A)</label>
                                    <select class="form-control @error('month') is-invalid @enderror" id="month" name="month">
                                        <option value="">Select Month</option>
                                        <option value="Jan" @if($selectedData !=null) @if($selectedData->month == 'Jan') selected @endif @endif>January</option>
                                        <option value="Feb" @if($selectedData !=null) @if($selectedData->month == 'Feb') selected @endif @endif>February</option>
                                        <option value="Mar" @if($selectedData !=null) @if($selectedData->month == 'Mar') selected @endif @endif>March</option>
                                        <option value="Apr" @if($selectedData !=null) @if($selectedData->month == 'Apr') selected @endif @endif>April</option>
                                        <option value="May" @if($selectedData !=null) @if($selectedData->month == 'May') selected @endif @endif>May</option>
                                        <option value="Jun" @if($selectedData !=null) @if($selectedData->month == 'Jun') selected @endif @endif>June</option>
                                        <option value="Jul" @if($selectedData !=null) @if($selectedData->month == 'Jul') selected @endif @endif>July</option>
                                        <option value="Aug" @if($selectedData !=null) @if($selectedData->month == 'Aug') selected @endif @endif>August</option>
                                        <option value="Sep" @if($selectedData !=null) @if($selectedData->month == 'Sep') selected @endif @endif>September</option>
                                        <option value="Oct" @if($selectedData !=null) @if($selectedData->month == 'Oct') selected @endif @endif>October</option>
                                        <option value="Nov" @if($selectedData !=null) @if($selectedData->month == 'Nov') selected @endif @endif>November</option>
                                        <option value="Dec" @if($selectedData !=null) @if($selectedData->month == 'Dec') selected @endif @endif>December</option>
                                    </select>
                                    @error('month')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="data_value">{{ __('Data Value') }} <i class="fas fa-asterisk asterisk"></i></label>
                                    <input id="data_value" type="text" class="form-control @error('data_value') is-invalid @enderror" name="data_value" value="@if($selectedData != null){{$selectedData->data_value}}@else{{ old('data_value') }}@endif" required autocomplete="data_value">
                                    @error('data_value')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="time_of_trade">{{ __('Time (Leave Blank if N/A)') }}</label>
                                    <input id="time_of_trade" type="text" class="form-control @error('time_of_trade') is-invalid @enderror" name="time_of_trade" value="@if($selectedData != null){{$selectedData->time_of_trade}}@else{{ old('time_of_trade') }}@endif" autocomplete="time_of_trade">
                                    @error('time_of_trade')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-9">
                            <button type="submit" class="btn btn-primary bt-margin">Save</button>
                            @if($selectedData != null)
                            <a class="btn cancel-bt bt-margin" href="/admin/insights">
                                Cancel
                            </a>
                            @endif
                        </div>

                    </div>
                </form>

                <form id="upload-data-form" method="POST" class="form-upload" action="/admin/insights/upload" enctype="multipart/form-data">
                    @csrf
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="category_id">Data Element <i class="fas fa-asterisk asterisk"></i></label>
                                    <select class="form-control @error('elem_id') is-invalid @enderror" id="element_id" name="element_id">
                                        <option value="">Select Data Element</option>
                                        @foreach($elements as $element)
                                        @if($selectedData != null)
                                        <option @if($selectedData->element_description == $element->elem_description) selected @endif value="{{$element->elem_id}}">{{$element->elem_description}}</option>
                                        @else
                                        <option value="{{$element->elem_id}}">{{$element->elem_description}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                    @error('elem_id')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="type_of_data">Type of Data <i class="fas fa-asterisk asterisk"></i></label>
                                    <select class="form-control @error('type_of_data') is-invalid @enderror" id="type_of_data" name="type_of_data">
                                        <option value="">Select Type of Data</option>
                                        <option value="Quarterly">Periodic Data (E,g. Quarterly)</option>
                                        <option value="Daily">Daily Data</option>
                                    </select>
                                    @error('type_of_data')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="year">Select File</label>
                                    <input type="file" class="form-control" name="data" id="data" />
                                </div>
                            </div>
                            <div class="col-sm-6" id="excel-template">
                                <label for="">Excel Template</label>
                                <img src="/img/daily_template.png" alt="" />
                            </div>
                        </div>

                        <div class="col-sm-9">
                            <button type="submit" style="background: #F64B4B;border-color: #F64B4B;" class="btn btn-primary bt-margin">Upload</button>
                        </div>

                    </div>
                </form>
            </div>

            <div class="container cnt-bd">

                <form method="GET" action="/admin/insights/filter">
                    @csrf
                    <div class="row search-mg data-table">
                        <div class="col-sm-4">
                            <select class="form-control @error('filter_elem_description') is-invalid @enderror" id="filter_elem_description" name="filter_elem_description">
                                <option value="">Data Element</option>
                                @foreach($elements as $element)
                                @if(isset($elemDesc))
                                <option value="{{$element->elem_description}}" @if($elemDesc==$element->elem_description) selected @endif>{{$element->elem_description}}</option>
                                @else
                                <option value="{{$element->elem_description}}">{{$element->elem_description}}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <select class="form-control @error('filter_sub_category_name') is-invalid @enderror" id="filter_sub_category_name" name="filter_sub_category_name">
                                <option value="">Type</option>
                                @foreach($subCategories as $subCategory)
                                @if(isset($subCategoryName))
                                <option value="{{$subCategory->sub_category_name}}" @if($subCategoryName==$subCategory->sub_category_name) selected @endif>{{$subCategory->sub_category_name}}</option>
                                @else
                                <option value="{{$subCategory->sub_category_name}}">{{$subCategory->sub_category_name}}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <select class="form-control @error('filter_month') is-invalid @enderror" id="filter_month" name="filter_month">
                                <option value="">Month</option>
                                <option value="Jan" @if(isset($month)) @if($month=='Jan' ) selected @endif @endif>January</option>
                                <option value="Feb" @if(isset($month)) @if($month=='Feb' ) selected @endif @endif>February</option>
                                <option value="Mar" @if(isset($month)) @if($month=='Mar' ) selected @endif @endif>March</option>
                                <option value="Apr" @if(isset($month)) @if($month=='Apr' ) selected @endif @endif>April</option>
                                <option value="May" @if(isset($month)) @if($month=='May' ) selected @endif @endif>May</option>
                                <option value="Jun" @if(isset($month)) @if($month=='Jun' ) selected @endif @endif>June</option>
                                <option value="Jul" @if(isset($month)) @if($month=='Jul' ) selected @endif @endif>July</option>
                                <option value="Aug" @if(isset($month)) @if($month=='Aug' ) selected @endif @endif>August</option>
                                <option value="Sep" @if(isset($month)) @if($month=='Sep' ) selected @endif @endif>September</option>
                                <option value="Oct" @if(isset($month)) @if($month=='Oct' ) selected @endif @endif>October</option>
                                <option value="Nov" @if(isset($month)) @if($month=='Nov' ) selected @endif @endif>November</option>
                                <option value="Dec" @if(isset($month)) @if($month=='Dec' ) selected @endif @endif>December</option>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <select class="form-control @error('filter_quarter') is-invalid @enderror" id="filter_quarter" name="filter_quarter">
                                <option value="">Quarter</option>
                                <option value="1" @if(isset($quarter)) @if($quarter==1) selected @endif @endif>First</option>
                                <option value="2" @if(isset($quarter)) @if($quarter==2) selected @endif @endif>Second</option>
                                <option value="3" @if(isset($quarter)) @if($quarter==3) selected @endif @endif>Third</option>
                                <option value="4" @if(isset($quarter)) @if($quarter==4) selected @endif @endif>Fourth</option>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <select class="form-control @error('filter_year') is-invalid @enderror" id="filter_year" name="filter_year">
                                <option value="">Year</option>
                                <option value="2009" @if(isset($year)) @if($year=='2009' ) selected @endif @endif>2009</option>
                                <option value="2010" @if(isset($year)) @if($year=='2010' ) selected @endif @endif>2010</option>
                                <option value="2011" @if(isset($year)) @if($year=='2011' ) selected @endif @endif>2011</option>
                                <option value="2012" @if(isset($year)) @if($year=='2012' ) selected @endif @endif>2012</option>
                                <option value="2013" @if(isset($year)) @if($year=='2013' ) selected @endif @endif>2013</option>
                                <option value="2014" @if(isset($year)) @if($year=='2014' ) selected @endif @endif>2014</option>
                                <option value="2015" @if(isset($year)) @if($year=='2015' ) selected @endif @endif>2015</option>
                                <option value="2016" @if(isset($year)) @if($year=='2016' ) selected @endif @endif>2016</option>
                                <option value="2017" @if(isset($year)) @if($year=='2017' ) selected @endif @endif>2017</option>
                                <option value="2018" @if(isset($year)) @if($year=='2018' ) selected @endif @endif>2018</option>
                                <option value="2019" @if(isset($year)) @if($year=='2019' ) selected @endif @endif>2019</option>
                                <option value="2020" @if(isset($year)) @if($year=='2020' ) selected @endif @endif>2020</option>
                                <option value="2021" @if(isset($year)) @if($year=='2021' ) selected @endif @endif>2021</option>
                                <option value="2022" @if(isset($year)) @if($year=='2022' ) selected @endif @endif>2022</option>
                                <option value="2023" @if(isset($year)) @if($year=='2023' ) selected @endif @endif>2023</option>
                                <option value="2024" @if(isset($year)) @if($year=='2024' ) selected @endif @endif>2024</option>
                                <option value="2025" @if(isset($year)) @if($year=='2025' ) selected @endif @endif>2025</option>
                                <option value="2026" @if(isset($year)) @if($year=='2026' ) selected @endif @endif>2026</option>
                                <option value="2027" @if(isset($year)) @if($year=='2027' ) selected @endif @endif>2027</option>
                                <option value="2028" @if(isset($year)) @if($year=='2028' ) selected @endif @endif>2028</option>
                                <option value="2029" @if(isset($year)) @if($year=='2029' ) selected @endif @endif>2029</option>
                                <option value="2030" @if(isset($year)) @if($year=='2030' ) selected @endif @endif>2030</option>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <input id="filter_time_of_trade" type="text" class="form-control @error('filter_time_of_trade') is-invalid @enderror" name="filter_time_of_trade" value="@if(isset($timeOfTrade)){{$timeOfTrade}}@endif" placeholder="Time">
                                @error('filter_time_of_trade')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-sm-1" style="margin-left: -15px;">
                            <button class="btn search-bt" type="submit">Filter</button>
                        </div>
                    </div>
                </form>
                <div style="display: flex;width: 230px;">
                    <div class="selector">
                        <input id="select-all" class="select-all" type="checkbox" />
                        <label for="select-all">Select All</label>
                    </div>
                    <div style="margin-left: 10px;margin:auto;">
                        <button type="button" class="btn-danger" id="delete-selected" style="border: 1px solid #CC0000;">Delete Selected</button>
                    </div>
                </div>
                <div class="row t-header">
                    <div class="col-sm-4">Data Element</div>
                    <div class="col-sm-1">Type</div>
                    <div class="col-sm-2">Period</div>
                    <div class="col-sm-2">Time</div>
                    <div class="col-sm-2">Value</div>
                    <div class="col-sm-1"></div>
                </div>
                <div id="data">
                    @foreach ($data as $record)
                    <div class="row t-content">
                        <div class="col-sm-4">
                            <input type="checkbox" class="elem-checkbox" value="{{ $record->data_table_id }}" />
                            {{ $record->element_description }}
                        </div>
                        <div class="col-sm-1">
                            {{ ucwords(strtolower($record->data_table_type)) }}
                        </div>
                        <?php
                        $period = $record->month;
                        if (is_null($period)) {
                            $period = $record->period;
                        }

                        $year = $record->year;
                        if (!is_null($year)) {
                            $period = $period . ", " . $year;
                        }

                        ?>
                        <div class="col-sm-2">
                            {{ ($period != null)?$period:"N/A" }}
                        </div>
                        <div class="col-sm-2">
                            {{ ($record->time_of_trade != null)?$record->time_of_trade:"N/A" }}
                        </div>
                        <div class="col-sm-2">
                            {{ $record->data_value }}
                        </div>

                        <div class="col-sm-1 edit-butts">
                            <span>
                                <a href="/admin/insights/edit/{{$record->data_table_id}}">
                                    <img src="/img/edit.png" width="15px" alt="Edit" />
                                </a>
                                <button rel="{{$record->data_table_id}}" class="btn deldata" data-bs-toggle="modal" data-bs-target="#dataModal" style="padding: 0.2rem;">
                                    <img src="/img/delete.png" width="15px" alt="Delete" />
                                </button>
                            </span>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="d-flex justify-content-center">
                    {!! $data->links() !!}
                </div>
            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="dataModal" tabindex="-1" role="dialog" aria-labelledby="dataModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="dataModalLabel">Confirm Delete</h5>
                    <button type="button" class="btn-close bt-close" data-bs-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    Are you sure you would like to delete the selected data record?
                </div>
                <div class="modal-footer">
                    <div class="row" style="width:100%">
                        <div class="col-8">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                        </div>
                        <div class="col-4">
                            <form action="/admin/insights/del" method="post">
                                @csrf
                                <input type="hidden" id="del-data" name="deletedata" />
                                <button type="submit" class="btn btn-primary">Confirm</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        @if(!is_null($selectedData))
        $('#add-data-form').show();
        @endif
        $('#time_of_trade').datetimepicker({
            format: 'Y-m-d H:i'
        });
        $('#filter_time_of_trade').datetimepicker({
            format: 'Y-m-d H:i'
        });
    });
</script>

<script>
    $(document).ready(function() {

        $('#delete-selected').hide();
        $('#select-all').change(
            function() {

                if ($('#select-all').is(":checked")) {

                    $('.elem-checkbox').prop('checked', true);
                    $('#delete-selected').show();
                } else {
                    $('.elem-checkbox').prop('checked', false);
                    $('#delete-selected').hide();
                }
            }
        );

        $('.elem-checkbox').change(
            function() {
                if ($(this).prop('checked') === true) {
                    $('#delete-selected').show();
                } else {

                    let atLeastOneIsSelected = false;
                    $('.elem-checkbox').each(function() {
                        if ($(this).prop('checked') === true) {
                            atLeastOneIsSelected = true;
                            return false;
                        }
                    });

                    if (atLeastOneIsSelected) {
                        $('#delete-selected').show();
                    } else {
                        $('#delete-selected').hide();
                    }
                }
            }
        );

        $('#excel-template').hide();

        $('#type_of_data').change(
            function() {
                let selected = $('#type_of_data').val();
                if (selected == 'Daily') {
                    $('#excel-template').html('<label for="">Excel Template</label><img src="/img/daily_template.png" alt="" />');
                    $('#excel-template').show();
                } else if (selected == 'Quarterly') {
                    $('#excel-template').html('<label for="">Excel Template</label><img src="/img/periodic_template.png" alt="" />');
                    $('#excel-template').show();
                }
            }
        );

        $('#delete-selected').click(
            function() {

                var selected = [];
                $('.elem-checkbox').each(function() {
                    if ($(this).prop('checked') === true) {
                        selected.push($(this).val());
                    }
                });

                if (confirm("Are you sure you want to delete the selected records?")) {
                    $.ajax({
                        type: "POST",
                        data: {
                            "_token": $('meta[name="csrf-token"]').attr('content'),
                            "ids": selected
                        },
                        url: "/admin/insights/delete-items",
                        success: function(data) {
                            if (data.status == 200) {
                                alert("Selected records deleted successfully");
                                location.reload();
                            } else {
                                alert("An unspecified error occured. No records were deleted!")
                            }
                        }
                    });
                } else {
                    return;
                }
            }
        );
    });
</script>
@endsection
@extends('layouts.app')
@section('content')

<div class="row justify-content-center m-15">

    <div class="col-sm-10">
        <div class="card">
            <div class="card-header">
                Roles
            </div>
            <div class="card-body">
                <div class="container cnt-bd">
                    <div id="userroles-data">
                        @foreach ($userroles as $userrole)
                        <div class="row t-content">
                            <div class="col-6">
                                {{ $userrole->role }}
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="userrolesModal" tabindex="-1" role="dialog" aria-labelledby="userrolesModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="userrolesModalLabel">Confirm Delete</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Are you sure you would like to delete the selected role?
            </div>
            <div class="modal-footer">
                <div class="row" style="width:100%">
                    <div class="col-8">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                    <div class="col-4">
                        <form action="/userrole/del" method="post">
                            @csrf
                            <input type="hidden" id="del-userrole" name="deleteuserrole" />
                            <button type="submit" class="btn btn-primary">Confirm</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
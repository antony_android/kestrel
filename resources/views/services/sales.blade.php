@extends('layouts.app')
<!-- Carousel section -->
@section('content')
<section id="team-top-section">
	<div class="py-5 brown-bg ">
		<h3 class="text-light text-center"> <strong>WHAT WE DO</strong> </h3>
		<h1 class="text-light text-center"> <strong>Sales & Trading</strong> </h1>
	</div>
	<div class="overflow-sec brown-bg">

	</div>
	<div class="container">


		<div class="row">
			<div class="col-md-12 mb-4">
				<div class="card team-wrap  shadow pb-4">
					<div class="card-body ">
						<div class="p-4 mb-4">
							<p class="mb-0 box-header">
								Frontier Markets are inherently illiquid and trading costs are high. To profitably navigate Frontier
								Markets, requires a trusted set of steady hands with the requisite experience and a deep network. </p>
							<p>
						</div>

						<div class="row services-font">
							<div class="col-md-6">

								<p>For over 25 years, Local and International Investors have turned to Kestrel Capital for Equity and
									Fixed Income sales and trading services. </p>
								<p>Kestrel Capital’s sales and trading team members, individually, have +10 years’ sales &amp; trading
									experience, primarily acquired working at Kestrel Capital. </p>
								<p>
									The Sales and Trading team’s strong relationships with underlying buy-side Emerging Markets,
									Frontier and Africa-specific funds, sell-side international brokerage houses and local pension funds
									places Kestrel Capital in a unique position of being able to source and match client trades efficiently
									and confidentially. </p>
							</div>
							<div class="col-md-6">
								<h3>Services</h3>
								<ul style="">
									<li>Equities and Fixed income trade execution </li>
									<li>
										Sourcing and placing of block trades
									</li>
									<li>
										Trading idea generation
									</li>
									<li>
										Fixed income research
									</li>
									<li>Execution of trades on the NSE’s Unquoted Securities Platform </li>
									<li>Execution of Securities Lending and Borrowing </li>
								</ul>
							</div>
						</div>

					</div>
				</div>

			</div>

		</div>
	</div>
</section>
@endsection
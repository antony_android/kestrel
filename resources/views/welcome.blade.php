@extends('layouts.app')

<!-- Carousel section -->



@section('content')

@include('components.carousel')
<!-- What we Do section -->

<section id="whatwedo" class="py-2" data-aos="fade-up">
    <div class="container">
        <h1 class="mb-4 mt-3">
            What we Do
        </h1>
        <div class="row">
            <div class="col-md-3">
                <div class="what-we-do-card">
                    <h4 class="light-brown">Sales & Trading</h4>
                    <div class="small-border"></div>
                    <p class="">
                        Frontier Markets are inherently illiquid and trading costs are high. To profitably navigate
                        Frontier Markets, requires a trusted set of steady hands with the requisite experience and a
                        deep network...
                    </p>

                    <a href="/services/sales">Learn more <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14"
                            fill="currentColor" class="bi bi-chevron-right" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
                        </svg></a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="what-we-do-card">
                    <h4 class="light-brown">Research</h4>
                    <div class="small-border"></div>
                    <p class="">
                        Kestrel Capital pioneered fundamental market and research in Kenya in 1995. Assisting clients
                        with their most critical decisions we offer well curated, timely, insights on companies through
                        several...
                    </p>

                    <a href="/services/research">Learn more <svg xmlns="http://www.w3.org/2000/svg" width="14"
                            height="14" fill="currentColor" class="bi bi-chevron-right" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
                        </svg></a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="what-we-do-card">
                    <h4 class="light-brown">Corporate Advisory</h4>
                    <div class="small-border"></div>
                    <p class="">
                        Kestrel Capital is a trusted market leader in formulating and executing, customized solutions to
                        our clients’ pressing business challenges. We integrate our expertise in Sales & Trading and
                        Research...
                    </p>

                    <a href="/services/corporateadvisory">Learn more <svg xmlns="http://www.w3.org/2000/svg" width="14"
                            height="14" fill="currentColor" class="bi bi-chevron-right" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
                        </svg></a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="what-we-do-card">
                    <h4 class="light-brown">Private Placement</h4>
                    <div class="small-border"></div>
                    <p class="">
                        Kestrel Capital originates, structures and executes private placements of a variety of
                        securities: equities, debt and related products. Private placements allow private companies to
                        broaden their...
                    </p>

                    <a href="/services/privateplacements">Learn more <svg xmlns="http://www.w3.org/2000/svg" width="14"
                            height="14" fill="currentColor" class="bi bi-chevron-right" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
                        </svg></a>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="whatwedo" class="py-2" data-aos="fade-up">
    <div class="container">
        <h1 class="mb-4 mt-3" style="font-size: 30px;">
            Equities Price Perfomance
        </h1>
        <div class="row">
            <div class="col-sm-6">

                <div class="sample-data-card">
                    <h4 class="light-brown">Safaricom</h4>
                    <div class="small-border"></div>

                    <?php

$data = App\Reports\TrendsReport::getSafaricomPriceTrend();
$safQuery = "SELECT DATE_FORMAT(DATE(time_of_trade), '%b %e, %y') AS Year,
                        data_value as Amount FROM data_table WHERE
                        element_description='Safaricom Share Price'";

$equityQuery = "SELECT DATE_FORMAT(DATE(time_of_trade), '%b %e, %y') AS Year,
                        data_value as Amount FROM data_table WHERE
                        element_description='Equity Bank Share Price'";

\koolreport\widgets\google\LineChart::create(array(
    "title" => "Safaricom Share Price",
    "dataSource" => $data,
    "colorScheme" => array(
        App\Reports\TrendsReport::checkTrend($safQuery),
    ),
    "height" => "250px",
    "options" => array(
        "legend" => "none",
    ),
    "columns" => array(
        "Year",
        "Amount" => array(
            "label" => "Safaricom Share Price",
            "type" => "number",
            "config" => array(
                "yAxisID" => "bar-y-sale",
            ),
        ),
    ),
));
?>

                    <a href="/insights/trends/price-trends-price-trends-safaricom-share-price/year/Year">View more <svg
                            xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor"
                            class="bi bi-chevron-right" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
                        </svg></a>

                </div>
            </div>

            <div class="col-sm-6">

                <div class="sample-data-card">
                    <h4 class="light-brown">Equity Bank</h4>
                    <div class="small-border"></div>

                    <?php

$data = App\Reports\TrendsReport::getEquityPriceTrend();

\koolreport\widgets\google\LineChart::create(array(
    "title" => "Equity Bank Share Price",
    "dataSource" => $data,
    "colorScheme" => array(
        App\Reports\TrendsReport::checkTrend($equityQuery),
    ),
    "height" => "250px",
    "options" => array(
        "legend" => "none",
    ),
    "columns" => array(
        "Year",
        "Amount" => array(
            "label" => "Equity Bank Share Price",
            "type" => "number",
            "config" => array(
                "yAxisID" => "bar-y-sale",
            ),
        ),
    ),
));
?>

                    <a href="/insights/trends/price-trends-price-trends-equity-bank-share-price/year/Year">View more
                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor"
                            class="bi bi-chevron-right" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
                        </svg></a>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="whatwedo" class="py-2" data-aos="fade-up">
    <div class="container">
        <h1 class="mb-4 mt-3" style="font-size: 30px;">
            Capital Markets and Industry Statistics
        </h1>
        <div class="row">
            <div class="col-sm-6">

                <div class="sample-data-card">
                    <h4 class="light-brown">Capital Markets (Equities)</h4>
                    <div class="small-border"></div>

                    <?php

$data = App\Reports\StatisticsReport::getNseMarketCapitalization();

\koolreport\widgets\google\ColumnChart::create(array(
    "title" => "Equities - NSE market-cap (USD m)",
    "dataSource" => $data,
    "colorScheme" => array(
        "#4682B4",
    ),
    "height" => "250px",
    "options" => array(
        "legend" => "none",
    ),
    "columns" => array(
        "Quarter",
        "Amount" => array(
            "label" => "NSE market-cap (USD m)",
            "type" => "number",
            "config" => array(
                "yAxisID" => "bar-y-sale",
            ),
        ),
    ),
));
?>

                    <a href="/insights/statistics/capital-markets-equities-nse-market-cap-usd-m/period/Quarter/1y">View
                        more <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor"
                            class="bi bi-chevron-right" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
                        </svg></a>

                </div>
            </div>

            <div class="col-sm-6">

                <div class="sample-data-card">
                    <h4 class="light-brown">Industry (Banking)</h4>
                    <div class="small-border"></div>

                    <?php

$data = App\Reports\StatisticsReport::getBankingTotalAssets();

\koolreport\widgets\google\LineChart::create(array(
    "title" => "Banking - Total assets (USD bn)",
    "dataSource" => $data,
    "colorScheme" => array(
        "#FF9900",
    ),
    "height" => "250px",
    "options" => array(
        "legend" => "none",
        "hAxis" => array(
            "slantedText" => "true",
            "allowContainerBoundaryTextCutoff" => true,
            "slantedTextAngle" => 40,
        ),
    ),
    "columns" => array(
        "Year" => array(
            "type" => "string",
            "formatValue" => function ($value) {
                return str_replace(",", "", $value);
            },
        ),
        "Amount" => array(
            "label" => "Total assets (USD bn)",
            "type" => "number",
            "config" => array(
                "yAxisID" => "bar-y-sale",
            ),
        ),
    ),
));
?>

                    <a href="/insights/statistics/industry-banking-total-assets-usd-bn/year/Year/5y">View more <svg
                            xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor"
                            class="bi bi-chevron-right" viewBox="0 0 16 16">
                            <path fill-rule="evenodd"
                                d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
                        </svg></a>
                </div>
            </div>
        </div>
    </div>
</section>

@if ( Session::has('flash_message') )
<div class="alert {{ Session::get('flash_type') }}">
    <h3>{{ Session::get('flash_message') }}</h3>
</div>

@endif

<section id="sample-research" class="py-4">
    <div class="container">
        <h1 class="mb-0 text-center" data-aos="fade-up">
            Sample Research
        </h1>
        <p class="mb-2 text-center" data-aos="fade-up">Free download</p>
        <div class="yellow-marker"></div>
        <div class="row" data-aos="fade-up">
            <div class="col-md-3">
                <div class="sample-research-image">

                </div>
            </div>
            <div class="col-md-6">
                <ul class="sample-research-list">
                    @foreach($recommendation as $doc)
                    <li>
                        <img src="/img/check-all.svg"><a href="<?=url('download/' . $doc->report_document)?>">
                            {{ $doc->title }}</a>
                    </li>
                    @endforeach
                </ul>
                <div class="text-center research-sec m-auto">
                    <a href="/insights/sampleresearch" class="research-button"
                        style="color: #3E281A; font-size: 12px; text-decoration: none;">
                        VIEW/DOWNLOAD ALL
                    </a>
                </div>
            </div>
            <div class="col-md-3">

                <div class="p-10 right-side" style="background: #402B1B;border-radius: 10px;">
                    <div class="circles-section d-flex justify-content-around team-sp">
                        <div data-aos="fade-up">
                            <div class="left-circle">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 505.4 505.4"
                                    style="enable-background:new 0 0 505.4 505.4; height: 50px" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M437.1,233.45c14.8-10.4,24.6-27.7,24.6-47.2c0-31.9-25.8-57.7-57.7-57.7c-31.9,0-57.7,25.8-57.7,57.7
            c0,19.5,9.7,36.8,24.6,47.2c-12.7,4.4-24.3,11.2-34.1,20c-13.5-11.5-29.4-20.3-46.8-25.5c21.1-12.8,35.3-36.1,35.3-62.6
            c0-40.4-32.7-73.1-73.1-73.1c-40.4,0-73.1,32.8-73.1,73.1c0,26.5,14.1,49.8,35.3,62.6c-17.2,5.2-32.9,13.9-46.3,25.2
            c-9.8-8.6-21.2-15.3-33.7-19.6c14.8-10.4,24.6-27.7,24.6-47.2c0-31.9-25.8-57.7-57.7-57.7s-57.7,25.8-57.7,57.7
            c0,19.5,9.7,36.8,24.6,47.2C28.5,247.25,0,284.95,0,329.25v6.6c0,0.2,0.2,0.4,0.4,0.4h122.3c-0.7,5.5-1.1,11.2-1.1,16.9v6.8
            c0,29.4,23.8,53.2,53.2,53.2h155c29.4,0,53.2-23.8,53.2-53.2v-6.8c0-5.7-0.4-11.4-1.1-16.9H505c0.2,0,0.4-0.2,0.4-0.4v-6.6
            C505.2,284.85,476.8,247.15,437.1,233.45z M362.3,186.15c0-23,18.7-41.7,41.7-41.7s41.7,18.7,41.7,41.7
            c0,22.7-18.3,41.2-40.9,41.7c-0.3,0-0.5,0-0.8,0s-0.5,0-0.8,0C380.5,227.45,362.3,208.95,362.3,186.15z M194.9,165.35
            c0-31.5,25.6-57.1,57.1-57.1s57.1,25.6,57.1,57.1c0,30.4-23.9,55.3-53.8,57c-1.1,0-2.2,0-3.3,0c-1.1,0-2.2,0-3.3,0
            C218.8,220.65,194.9,195.75,194.9,165.35z M59.3,186.15c0-23,18.7-41.7,41.7-41.7s41.7,18.7,41.7,41.7c0,22.7-18.3,41.2-40.9,41.7
            c-0.3,0-0.5,0-0.8,0s-0.5,0-0.8,0C77.6,227.45,59.3,208.95,59.3,186.15z M125.5,320.15H16.2c4.5-42.6,40.5-76,84.2-76.3
            c0.2,0,0.4,0,0.6,0s0.4,0,0.6,0c20.8,0.1,39.8,7.8,54.5,20.3C141.7,279.75,131,298.95,125.5,320.15z M366.8,359.95
            c0,20.5-16.7,37.2-37.2,37.2h-155c-20.5,0-37.2-16.7-37.2-37.2v-6.8c0-62.1,49.6-112.9,111.3-114.7c1.1,0.1,2.3,0.1,3.4,0.1
            s2.3,0,3.4-0.1c61.7,1.8,111.3,52.6,111.3,114.7V359.95z M378.7,320.15c-5.5-21.1-16-40-30.3-55.6c14.8-12.8,34-20.5,55-20.7
            c0.2,0,0.4,0,0.6,0s0.4,0,0.6,0c43.7,0.3,79.7,33.7,84.2,76.3H378.7z" />
                                        </g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                </svg>
                            </div>
                            <div class="text-light mt-3 text-center">
                                <a href="/about/team" style="color: unset; text-decoration: none;">OUR TEAM</a>
                            </div>
                        </div>
                    </div>
                    <div class="p-10 team-sp">
                        <div data-aos="fade-up" class="people">THE PEOPLE</div>
                        <div class="sub-people" data-aos="fade-up">Who safeguard your interests</div>
                    </div>

                    <div data-aos="fade-up">
                        <div class="right-circle" style="margin-top: 20px;">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-people"
                                viewBox="0 0 16 16">
                                <path
                                    d="M15 14s1 0 1-1-1-4-5-4-5 3-5 4 1 1 1 1h8zm-7.978-1A.261.261 0 0 1 7 12.996c.001-.264.167-1.03.76-1.72C8.312 10.629 9.282 10 11 10c1.717 0 2.687.63 3.24 1.276.593.69.758 1.457.76 1.72l-.008.002a.274.274 0 0 1-.014.002H7.022zM11 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm3-2a3 3 0 1 1-6 0 3 3 0 0 1 6 0zM6.936 9.28a5.88 5.88 0 0 0-1.23-.247A7.35 7.35 0 0 0 5 9c-4 0-5 3-5 4 0 .667.333 1 1 1h4.216A2.238 2.238 0 0 1 5 13c0-1.01.377-2.042 1.09-2.904.243-.294.526-.569.846-.816zM4.92 10A5.493 5.493 0 0 0 4 13H1c0-.26.164-1.03.76-1.724.545-.636 1.492-1.256 3.16-1.275zM1.5 5.5a3 3 0 1 1 6 0 3 3 0 0 1-6 0zm3-2a2 2 0 1 0 0 4 2 2 0 0 0 0-4z" />
                            </svg>
                        </div>
                        <div class="text-light mt-3 text-center">
                            <a href="/about/nonexecutive" style="color: unset; text-decoration: none;">NON EXECUTIVE
                                DIRECTORS</a>
                        </div>
                    </div>

                </div>

                <!--     <div class="right-side">
                    <div class="white-section thumbs-section d-flex justify-content-between" data-aos="fade-up">
                     <!--    <div>
                            <p class="mb-1">TRANSACTION HISTORY</p>
                            <a href="/about/transactionhistory">VIEW <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-chevron-right" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
                                </svg></a>
                        </div>
                        <div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-clock-history" viewBox="0 0 16 16">
                                <path d="M8.515 1.019A7 7 0 0 0 8 1V0a8 8 0 0 1 .589.022l-.074.997zm2.004.45a7.003 7.003 0 0 0-.985-.299l.219-.976c.383.086.76.2 1.126.342l-.36.933zm1.37.71a7.01 7.01 0 0 0-.439-.27l.493-.87a8.025 8.025 0 0 1 .979.654l-.615.789a6.996 6.996 0 0 0-.418-.302zm1.834 1.79a6.99 6.99 0 0 0-.653-.796l.724-.69c.27.285.52.59.747.91l-.818.576zm.744 1.352a7.08 7.08 0 0 0-.214-.468l.893-.45a7.976 7.976 0 0 1 .45 1.088l-.95.313a7.023 7.023 0 0 0-.179-.483zm.53 2.507a6.991 6.991 0 0 0-.1-1.025l.985-.17c.067.386.106.778.116 1.17l-1 .025zm-.131 1.538c.033-.17.06-.339.081-.51l.993.123a7.957 7.957 0 0 1-.23 1.155l-.964-.267c.046-.165.086-.332.12-.501zm-.952 2.379c.184-.29.346-.594.486-.908l.914.405c-.16.36-.345.706-.555 1.038l-.845-.535zm-.964 1.205c.122-.122.239-.248.35-.378l.758.653a8.073 8.073 0 0 1-.401.432l-.707-.707z" />
                                <path d="M8 1a7 7 0 1 0 4.95 11.95l.707.707A8.001 8.001 0 1 1 8 0v1z" />
                                <path d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5z" />
                            </svg>

                        </div>

                    </div>
                    <div class="brown-section thumbs-section d-flex justify-content-between" data-aos="fade-up">
                        <div>
                            <p class="mb-1">OVERVIEW OF NSE</p>
                            <a href="/about/overviewnse">VIEW <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-chevron-right" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
                                </svg></a>
                        </div>
                        <div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-cash-coin" viewBox="0 0 16 16">
                                <path fill-rule="evenodd" d="M11 15a4 4 0 1 0 0-8 4 4 0 0 0 0 8zm5-4a5 5 0 1 1-10 0 5 5 0 0 1 10 0z" />
                                <path d="M9.438 11.944c.047.596.518 1.06 1.363 1.116v.44h.375v-.443c.875-.061 1.386-.529 1.386-1.207 0-.618-.39-.936-1.09-1.1l-.296-.07v-1.2c.376.043.614.248.671.532h.658c-.047-.575-.54-1.024-1.329-1.073V8.5h-.375v.45c-.747.073-1.255.522-1.255 1.158 0 .562.378.92 1.007 1.066l.248.061v1.272c-.384-.058-.639-.27-.696-.563h-.668zm1.36-1.354c-.369-.085-.569-.26-.569-.522 0-.294.216-.514.572-.578v1.1h-.003zm.432.746c.449.104.655.272.655.569 0 .339-.257.571-.709.614v-1.195l.054.012z" />
                                <path d="M1 0a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1h4.083c.058-.344.145-.678.258-1H3a2 2 0 0 0-2-2V3a2 2 0 0 0 2-2h10a2 2 0 0 0 2 2v3.528c.38.34.717.728 1 1.154V1a1 1 0 0 0-1-1H1z" />
                                <path d="M9.998 5.083 10 5a2 2 0 1 0-3.132 1.65 5.982 5.982 0 0 1 3.13-1.567z" />
                            </svg>
                        </div>
                    </div>
              <!--       <div class="white-section thumbs-section d-flex justify-content-between" data-aos="fade-up">
                        <div>
                            <p class="mb-1">USEFUL LINKS</p>
                            <a href="/about/usefullinks">VIEW <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-right" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
                                </svg></a>
                        </div>
                        <div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" class="bi bi-link-45deg" viewBox="0 0 16 16">
                                <path d="M4.715 6.542 3.343 7.914a3 3 0 1 0 4.243 4.243l1.828-1.829A3 3 0 0 0 8.586 5.5L8 6.086a1.002 1.002 0 0 0-.154.199 2 2 0 0 1 .861 3.337L6.88 11.45a2 2 0 1 1-2.83-2.83l.793-.792a4.018 4.018 0 0 1-.128-1.287z" />
                                <path d="M6.586 4.672A3 3 0 0 0 7.414 9.5l.775-.776a2 2 0 0 1-.896-3.346L9.12 3.55a2 2 0 1 1 2.83 2.83l-.793.792c.112.42.155.855.128 1.287l1.372-1.372a3 3 0 1 0-4.243-4.243L6.586 4.672z" />
                            </svg>
                        </div>
                    </div> -->
            </div>
        </div>
    </div>
    </div>
</section>


<!-- <section id="thepeople">

    <div class="p-10 left-side">
        <div class="row">

            <div class="col-sm-4">
                <div class="circles-section d-flex justify-content-around">
                    <div data-aos="fade-up">
                        <div class="left-circle">
                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 505.4 505.4" style="enable-background:new 0 0 505.4 505.4; height: 50px" xml:space="preserve">
                                <g>
                                    <g>
                                        <path d="M437.1,233.45c14.8-10.4,24.6-27.7,24.6-47.2c0-31.9-25.8-57.7-57.7-57.7c-31.9,0-57.7,25.8-57.7,57.7
            c0,19.5,9.7,36.8,24.6,47.2c-12.7,4.4-24.3,11.2-34.1,20c-13.5-11.5-29.4-20.3-46.8-25.5c21.1-12.8,35.3-36.1,35.3-62.6
            c0-40.4-32.7-73.1-73.1-73.1c-40.4,0-73.1,32.8-73.1,73.1c0,26.5,14.1,49.8,35.3,62.6c-17.2,5.2-32.9,13.9-46.3,25.2
            c-9.8-8.6-21.2-15.3-33.7-19.6c14.8-10.4,24.6-27.7,24.6-47.2c0-31.9-25.8-57.7-57.7-57.7s-57.7,25.8-57.7,57.7
            c0,19.5,9.7,36.8,24.6,47.2C28.5,247.25,0,284.95,0,329.25v6.6c0,0.2,0.2,0.4,0.4,0.4h122.3c-0.7,5.5-1.1,11.2-1.1,16.9v6.8
            c0,29.4,23.8,53.2,53.2,53.2h155c29.4,0,53.2-23.8,53.2-53.2v-6.8c0-5.7-0.4-11.4-1.1-16.9H505c0.2,0,0.4-0.2,0.4-0.4v-6.6
            C505.2,284.85,476.8,247.15,437.1,233.45z M362.3,186.15c0-23,18.7-41.7,41.7-41.7s41.7,18.7,41.7,41.7
            c0,22.7-18.3,41.2-40.9,41.7c-0.3,0-0.5,0-0.8,0s-0.5,0-0.8,0C380.5,227.45,362.3,208.95,362.3,186.15z M194.9,165.35
            c0-31.5,25.6-57.1,57.1-57.1s57.1,25.6,57.1,57.1c0,30.4-23.9,55.3-53.8,57c-1.1,0-2.2,0-3.3,0c-1.1,0-2.2,0-3.3,0
            C218.8,220.65,194.9,195.75,194.9,165.35z M59.3,186.15c0-23,18.7-41.7,41.7-41.7s41.7,18.7,41.7,41.7c0,22.7-18.3,41.2-40.9,41.7
            c-0.3,0-0.5,0-0.8,0s-0.5,0-0.8,0C77.6,227.45,59.3,208.95,59.3,186.15z M125.5,320.15H16.2c4.5-42.6,40.5-76,84.2-76.3
            c0.2,0,0.4,0,0.6,0s0.4,0,0.6,0c20.8,0.1,39.8,7.8,54.5,20.3C141.7,279.75,131,298.95,125.5,320.15z M366.8,359.95
            c0,20.5-16.7,37.2-37.2,37.2h-155c-20.5,0-37.2-16.7-37.2-37.2v-6.8c0-62.1,49.6-112.9,111.3-114.7c1.1,0.1,2.3,0.1,3.4,0.1
            s2.3,0,3.4-0.1c61.7,1.8,111.3,52.6,111.3,114.7V359.95z M378.7,320.15c-5.5-21.1-16-40-30.3-55.6c14.8-12.8,34-20.5,55-20.7
            c0.2,0,0.4,0,0.6,0s0.4,0,0.6,0c43.7,0.3,79.7,33.7,84.2,76.3H378.7z" />
                                    </g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                            </svg>
                        </div>
                        <div class="text-light mt-3 text-center">
                            <a href="/about/team" style="color: unset; text-decoration: none;">OUR TEAM</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <h1 data-aos="fade-up" class="people">THE PEOPLE</h1>
                <div class="sub-people" data-aos="fade-up">Who safeguard your interests</div>
            </div>
            <div class="col-sm-4">
                <div data-aos="fade-up">
                    <div class="right-circle">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-people" viewBox="0 0 16 16">
                            <path d="M15 14s1 0 1-1-1-4-5-4-5 3-5 4 1 1 1 1h8zm-7.978-1A.261.261 0 0 1 7 12.996c.001-.264.167-1.03.76-1.72C8.312 10.629 9.282 10 11 10c1.717 0 2.687.63 3.24 1.276.593.69.758 1.457.76 1.72l-.008.002a.274.274 0 0 1-.014.002H7.022zM11 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm3-2a3 3 0 1 1-6 0 3 3 0 0 1 6 0zM6.936 9.28a5.88 5.88 0 0 0-1.23-.247A7.35 7.35 0 0 0 5 9c-4 0-5 3-5 4 0 .667.333 1 1 1h4.216A2.238 2.238 0 0 1 5 13c0-1.01.377-2.042 1.09-2.904.243-.294.526-.569.846-.816zM4.92 10A5.493 5.493 0 0 0 4 13H1c0-.26.164-1.03.76-1.724.545-.636 1.492-1.256 3.16-1.275zM1.5 5.5a3 3 0 1 1 6 0 3 3 0 0 1-6 0zm3-2a2 2 0 1 0 0 4 2 2 0 0 0 0-4z" />
                        </svg>
                    </div>
                    <div class="text-light mt-3 text-center">
                        <a href="/about/team" style="color: unset; text-decoration: none;">NON EXECUTIVE DIRECTORS</a>
                    </div>
                </div>
            </div>

        </div>

    </div>

    </div>
</section> -->

@endsection
<?php

$servername = env('DB_HOST', '127.0.0.1');

$username = env('DB_USERNAME', 'Kestrelad');
$password = env('DB_PASSWORD', '@2095K3strel');
$dbname = env('DB_DATABASE', 'kestreldb');

$conn = new mysqli($servername, $username, $password, $dbname);

$sql = "SELECT DISTINCT category_name, sub_category_name, element_description, 
 slug FROM data_table INNER JOIN category USING(category_id) INNER JOIN 
 sub_category USING(sub_category_id) WHERE data_table.ui_position = 'statistics'";

$trendsSQL = "SELECT DISTINCT category_name, sub_category_name, element_description, 
 slug FROM data_table INNER JOIN category USING(category_id) INNER JOIN 
 sub_category USING(sub_category_id) WHERE data_table.ui_position = 'price-trends'";

 $result = $conn->query($sql);

$trendsResult = $conn->query($trendsSQL);

$reportTitles = array();
$trendsTitles = array();
$periodSlugs = array();
$categories = array();
$trendsCategories = array();
$data = array();
$trendsData = array();

if ($result->num_rows > 0) {

    while ($row = $result->fetch_assoc()) {
        $reportTitles[$row['slug']] = $row['element_description'];
    }
}

if ($trendsResult->num_rows > 0) {

    while ($row = $trendsResult->fetch_assoc()) {
        $trendsTitles[$row['slug']] = $row['element_description'];
    }
}

$periodSlugsSQL = "SELECT DISTINCT slug FROM data_table WHERE 
    `period` IS NOT NULL AND month IS NULL";
$periodSlugsResult = $conn->query($periodSlugsSQL);

if ($periodSlugsResult->num_rows > 0) {

    while ($row = $periodSlugsResult->fetch_assoc()) {
        array_push($periodSlugs, $row['slug']);
    }
}

$categoriesSQL = "SELECT category_id, category_name, category_slug FROM category
 WHERE ui_position ='statistics'";
$categoriesResult = $conn->query($categoriesSQL);

if ($categoriesResult->num_rows > 0) {

    while ($row = $categoriesResult->fetch_assoc()) {

        $category = $row['category_name'];
        $categoryId = $row['category_id'];
        $categorySlug = $row['category_slug'];

        $categories[$categoryId] = $category;

        $subCategoriesSQL = "SELECT sub_category_id, sub_category_name, icon_class,
         sub_category_slug FROM sub_category WHERE category_id = '$categoryId'";

        $subCategoriesResult = $conn->query($subCategoriesSQL);

        $subsData = array();
        if ($subCategoriesResult->num_rows > 0) {

            while ($row = $subCategoriesResult->fetch_assoc()) {

                $subCategoryName = $row['sub_category_name'];
                $subCategoryId = $row['sub_category_id'];
                $subCategorySlug = $row['sub_category_slug'];
                $iconClass = $row['icon_class'];

                $dataSQL = "SELECT DISTINCT d.element_description, d.slug, d.default_period, 
                 d.default_period_slug, d.default_period_duration FROM data_table d INNER JOIN 
                 elem e ON d.element_description = e.elem_description WHERE 
                 d.sub_category_id = '$subCategoryId' AND d.ui_position = 'statistics' ORDER BY 
                 e.priority DESC";

                $dataResult = $conn->query($dataSQL);
                $titles = array();

                if ($dataResult->num_rows > 0) {

                    while ($row = $dataResult->fetch_assoc()) {
                        $titles[$row['slug']] = array(
                            'element_description' => $row['element_description'],
                            'default_period' => $row['default_period'],
                            'default_period_slug' => $row['default_period_slug'],
                            'default_period_duration' => $row['default_period_duration'],
                        );
                    }
                }

                $subsData[$subCategoryName] = array(
                    'slug' => $subCategorySlug,
                    'icon_class' => $iconClass,
                    'sub_category_data' => $titles
                );
            }
        }

        $data[$category] = array('slug' => $categorySlug, 'category_data' => $subsData);
    }
}

$trendsCategoriesSQL = "SELECT category_id, category_name, category_slug FROM category
 WHERE ui_position ='price-trends'";
$trendsCategoriesResult = $conn->query($trendsCategoriesSQL);

if ($trendsCategoriesResult->num_rows > 0) {

    while ($row = $trendsCategoriesResult->fetch_assoc()) {

        $category = $row['category_name'];
        $categoryId = $row['category_id'];
        $categorySlug = $row['category_slug'];

        $trendsCategories[$categoryId] = $category;

        $trendsSubCategoriesSQL = "SELECT sub_category_id, sub_category_name, icon_class,
         sub_category_slug FROM sub_category WHERE category_id = '$categoryId'";

        $trendsSubCategoriesResult = $conn->query($trendsSubCategoriesSQL);

        $trendsSubData = array();
        if ($trendsSubCategoriesResult->num_rows > 0) {

            while ($row = $trendsSubCategoriesResult->fetch_assoc()) {

                $subCategoryName = $row['sub_category_name'];
                $subCategoryId = $row['sub_category_id'];
                $subCategorySlug = $row['sub_category_slug'];
                $iconClass = $row['icon_class'];

                $dataSQL = "SELECT DISTINCT d.element_description, d.slug, d.default_period, 
                 d.default_period_slug, d.default_period_duration FROM data_table d INNER JOIN 
                 elem e ON d.element_description = e.elem_description WHERE 
                 d.sub_category_id = '$subCategoryId' AND d.ui_position = 'price-trends' ORDER BY 
                 e.priority DESC";

                $dataResult = $conn->query($dataSQL);
                $titles = array();

                if ($dataResult->num_rows > 0) {

                    while ($row = $dataResult->fetch_assoc()) {
                        $titles[$row['slug']] = array(
                            'element_description' => $row['element_description'],
                            'default_period' => $row['default_period'],
                            'default_period_slug' => $row['default_period_slug'],
                            'default_period_duration' => $row['default_period_duration'],
                        );
                    }
                }

                $trendsSubData[$subCategoryName] = array(
                    'slug' => $subCategorySlug,
                    'icon_class' => $iconClass,
                    'sub_category_data' => $titles
                );
            }
        }

        $trendsData[$category] = array('slug' => $categorySlug, 'category_data' => $trendsSubData);
    }
}

$conn->close();

return [
    'report_titles' => $reportTitles,
    'trends_titles' => $trendsTitles,
    'report_period_slugs' => $periodSlugs,
    'categories' => $categories,
    'trends_categories' => $trendsCategories,
    'data' => $data,
    'trends_data' => $trendsData
];

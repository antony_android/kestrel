<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

use App\Models\SubCategory;
use App\Models\DataTable;
use App\Models\Elem;
use App\Imports\UploadDataImports;
use App\Imports\UploadDailyDataImports;

use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class DataTableController extends Controller
{

    const DATA_BLADE = 'admin/data_table';
    const DATA_HOME = '/admin/insights';

    const MONTH_IDS = [
        "Jan" => 1, "Feb" => 2, "Mar" => 3, "Apr" => 4, "May" => 5,
        "Jun" => 6, "Jul" => 7, "Aug" => 8, "Sep" => 9, "Oct" => 10,
        "Nov" => 11, "Dec" => 12, null => null
    ];

    const ID_MONTHS = [
        "1" => "Jan", "2" => "Feb", "3" => "Mar", "4" => "Apr", "5" => "May",
        "6" => "Jun", "7" => "Jul", "8" => "Aug", "9" => "Sep", "10" => "Oct",
        "11" => "Nov", "12" => "Dec", null => null
    ];

    const MONTH_PERIOD_IDS = [
        "Jan" => 1, "Feb" => 1, "Mar" => 1, "Apr" => 2, "May" => 2,
        "Jun" => 2, "Jul" => 3, "Aug" => 3, "Sep" => 3, "Oct" => 4,
        "Nov" => 4, "Dec" => 4, null => null
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = DataTable::orderBy('data_table_id', 'DESC')->paginate(10);
        $elements = Elem::all();
        $subCategories = SubCategory::all();


        return view(
            self::DATA_BLADE,
            [
                'elements' => $elements,
                'data' => $data,
                'subCategories' => $subCategories,
                'selectedData' => null
            ]
        );
    }

    public function save(Request $request)
    {

        $data = $request->validate([
            'data_value' => ['required', 'numeric']
        ]);

        $elemId = $request->elem_id;
        if (is_null($elemId)) {

            Session::flash('error', 'You must select the data element to which
             the record belongs!');
            return redirect()->back();
        }

        $period = $request->period;
        $year = $request->year;
        if (strlen($period) > 0) {

            if (strlen($year) == 0) {
                Session::flash('error', 'You must select the year if a quarter is selected!');
                return redirect()->back();
            }
            $data['period_id'] = $period;
            $period = $period . "Q" . substr($year, 2, 2);
        }

        if (strlen($request->month) > 0 && strlen($year) > 0 && strlen($period) == 0) {
            $period = $request->month . "-" . substr($year, 2, 2);
            $data['period_id'] = self::MONTH_PERIOD_IDS[$request->month];
        }

        if (strlen($request->month) > 0) {

            $data['month_id'] = self::MONTH_IDS[$request->month];
        }

        $element = Elem::findOrFail($elemId);
        $subCategory = SubCategory::findOrFail($element->sub_category_id);

        $data['data_table_type'] = strtoupper($subCategory->sub_category_name);
        $data['element_description'] = $element->elem_description;
        $data['category_id'] = $element->category_id;
        $data['sub_category_id'] = $element->sub_category_id;
        $data['ui_position'] = $element->ui_position;
        $data['year'] = $year;
        $data['month'] = $request->month;
        $data['time_of_trade'] = $request->time_of_trade;
        $data['period'] = $period;
        $data['user_id'] = Auth::user()->id;
        $data['slug'] = $element->slug;
        $data['default_period'] = $element->default_period;
        $data['default_period_slug'] = $element->default_period_slug;
        $data['default_period_duration'] = $element->default_period_duration;

        if (
            strlen($data['time_of_trade']) > 0
            && strlen($data['year']) == 0
        ) {
            $data['year'] = substr($data['time_of_trade'], 0, 4);
        }
        if (
            strlen($data['time_of_trade']) > 0
            && strlen($data['month']) == 0
        ) {
            $monthD = ltrim(substr($data['time_of_trade'], 5, 2), "0");
            $monthWord = self::ID_MONTHS[$monthD];
            $periodId = self::MONTH_PERIOD_IDS[$monthWord];

            $data['month'] = $monthWord;
            $data['month_id'] = $monthD;
            $data['period'] = $periodId;
        }
        if (
            strlen($data['time_of_trade']) == 0
            && strlen($data['month']) > 0
        ) {
            $monthDe = $data['month_id'];

            if (strlen($monthDe) == 1) {
                $monthDe = "0" . $monthDe;
            }
            $timeOfTrade = $year . "-" . $monthDe . "-01";
            $data['time_of_trade'] = $timeOfTrade;
        }
        if (
            strlen($data['time_of_trade']) == 0
            && strlen($data['month']) == 0
            && strlen($data['period_id']) > 0
        ) {
            if ($data['period_id'] == 1) {

                $timeOfTrade = $year . "-01-01";
            } else if ($data['period_id'] == 2) {

                $timeOfTrade = $year . "-04-01";
            } else if ($data['period_id'] == 3) {

                $timeOfTrade = $year . "-07-01";
            } else if ($data['period_id'] == 4) {

                $timeOfTrade = $year . "-10-01";
            }
            $data['time_of_trade'] = $timeOfTrade;
        }

        $selected = $request->selected_data;

        if ($selected) {
            tap(DataTable::where('data_table_id', $selected)->update($data));
        } else {
            tap(new DataTable($data))->save();
        }

        Session::flash('success', 'Data record saved successfully!');
        return redirect(self::DATA_HOME);
    }


    public function upload(Request $request)
    {
        $data = $request->all();
        $elementId = $data["element_id"];
        $typeOfData = $data["type_of_data"];
        $uploadData = [];
        $hasMonth = false;
        $hasYear = false;
        $hasPeriod = false;

        $element = Elem::findOrFail($elementId);
        $subCategory = SubCategory::findOrFail($element->sub_category_id);

        if ($request->file('data') && $typeOfData == "Quarterly") {

            $data = Excel::toArray(new UploadDataImports, $request->file('data'));

            if (!empty($data[0]) && count($data[0])) {
                $rowCount = 0;
                foreach ($data[0] as $key => $value) {

                    if (strlen(trim($value['year'])) == 0) {
                        continue;
                    }
                    $uploadData['data_table_type'] = strtoupper($subCategory->sub_category_name);
                    $uploadData['element_description'] = $element->elem_description;
                    $uploadData['category_id'] = $element->category_id;
                    $uploadData['sub_category_id'] = $element->sub_category_id;
                    $uploadData['ui_position'] = $element->ui_position;
                    $uploadData['user_id'] = Auth::user()->id;
                    $uploadData['slug'] = $element->slug;
                    $uploadData['default_period'] = $element->default_period;
                    $uploadData['default_period_slug'] = $element->default_period_slug;
                    $uploadData['default_period_duration'] = $element->default_period_duration;

                    $rowCount++;
                    if ($rowCount == 0) {
                        continue;
                    }
                    if (!empty($value)) {
                        $colCount = 0;

                        foreach ($value as $v) {
                            $colCount++;
                            if ($colCount == 1) {
                                $uploadData['year'] = $v;
                                if (strlen($v) > 0) {
                                    $hasYear = true;
                                    $year = $v;
                                }
                            } else if ($colCount == 2) {
                                $uploadData['period'] = $v;
                                $uploadData['period_id'] = substr($v, 0, 1);
                                if (strlen($v) > 0) {
                                    $hasPeriod = true;
                                }
                            } else if ($colCount == 3) {
                                $uploadData['month'] = $v;
                                $uploadData['month_id'] = self::MONTH_PERIOD_IDS[$v];
                                if (strlen($v) > 0) {
                                    $hasMonth = true;
                                    $month = $v;
                                }
                            } else if ($colCount == 4) {
                                $uploadData['data_value'] = $v;
                            }

                            if ($hasYear && $hasMonth && !$hasPeriod) {
                                $period = $month . "-" . substr($year, 2, 2);
                                $data['period_id'] = self::MONTH_PERIOD_IDS[$request->month];
                                $uploadData['period'] = $period;
                            }
                        }
                    }
                    if (strlen($uploadData['month']) > 0) {
                        $monthDe = $uploadData['month_id'];

                        if (strlen($monthDe) == 1) {
                            $monthDe = "0" . $monthDe;
                        }
                        $timeOfTrade = $uploadData['year'] . "-" . $monthDe . "-01";
                        $uploadData['time_of_trade'] = $timeOfTrade;
                    }
                    if (
                        strlen($uploadData['month']) == 0
                        && strlen($uploadData['period_id']) > 0
                    ) {
                        if ($uploadData['period_id'] == 1) {

                            $timeOfTrade = $uploadData['year'] . "-01-01";
                        } else if ($uploadData['period_id'] == 2) {

                            $timeOfTrade = $uploadData['year'] . "-04-01";
                        } else if ($uploadData['period_id'] == 3) {

                            $timeOfTrade = $uploadData['year'] . "-07-01";
                        } else if ($uploadData['period_id'] == 4) {

                            $timeOfTrade = $uploadData['year'] . "-10-01";
                        }
                        $uploadData['time_of_trade'] = $timeOfTrade;
                    }
                    if (!empty($uploadData)) {
                        tap(new DataTable($uploadData))->save();
                        $hasMonth = false;
                        $hasYear = false;
                        $hasPeriod = false;
                        $uploadData = [];
                    }
                }
            }
        } else if ($request->file('data') && $typeOfData == "Daily") {

            $data = Excel::toArray(new UploadDailyDataImports, $request->file('data'));

            if (!empty($data[0]) && count($data[0])) {
                $rowCount = 0;
                foreach ($data[0] as $key => $value) {

                    if (strlen(trim($value['date'])) == 0) {
                        continue;
                    }
                    $uploadData['data_table_type'] = strtoupper($subCategory->sub_category_name);
                    $uploadData['element_description'] = $element->elem_description;
                    $uploadData['category_id'] = $element->category_id;
                    $uploadData['sub_category_id'] = $element->sub_category_id;
                    $uploadData['ui_position'] = $element->ui_position;
                    $uploadData['user_id'] = Auth::user()->id;
                    $uploadData['slug'] = $element->slug;
                    $uploadData['default_period'] = $element->default_period;
                    $uploadData['default_period_slug'] = $element->default_period_slug;
                    $uploadData['default_period_duration'] = $element->default_period_duration;

                    $rowCount++;
                    if ($rowCount == 0) {
                        continue;
                    }
                    if (!empty($value)) {
                        $colCount = 0;

                        foreach ($value as $v) {
                            $colCount++;
                            if ($colCount == 1) {
                                $date = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($v)->format('Y-m-d h:i:s');
                                $uploadData['time_of_trade'] = $date;
                                $uploadData['year'] = substr($date,  0, 4);
                                $monthD = ltrim(substr($uploadData['time_of_trade'], 5, 2), "0");
                                $monthWord = self::ID_MONTHS[$monthD];
                                $periodId = self::MONTH_PERIOD_IDS[$monthWord];

                                $uploadData['month'] = $monthWord;
                                $uploadData['month_id'] = $monthD;
                                $uploadData['period'] = $periodId;
                            } else if ($colCount == 2) {
                                $uploadData['data_value'] = $v;
                            }
                        }
                        if (!empty($uploadData)) {

                            DB::statement("INSERT INTO data_table VALUES(
                                null,
                                '" . $uploadData['data_table_type'] . "',
                                '" . $uploadData['element_description'] . "',
                                '" . $uploadData['year'] . "',
                                '" . $uploadData['month'] . "',
                                '" . $uploadData['month_id'] . "',
                                '" . $uploadData['time_of_trade'] . "',
                                '" . $uploadData['period'] . "',
                                null,
                                '" . $uploadData['data_value'] . "',
                                '" . $uploadData['category_id'] . "',
                                '" . $uploadData['sub_category_id'] . "',
                                '" . $uploadData['ui_position'] . "',
                                '" . $uploadData['slug'] . "',
                                '" . $uploadData['default_period'] . "',
                                '" . $uploadData['default_period_slug'] . "',
                                '" . $uploadData['default_period_duration'] . "',
                                '" . $uploadData['user_id'] . "',
                                null,
                                now(),
                                now()
                            )");
                            $uploadData = [];
                        }
                    }
                }
            }
        }
        Session::flash('success', 'Uploaded the data successfully!');
        return redirect(self::DATA_HOME);
    }

    public function search(Request $request)
    {

        $data = DB::select(Db::raw("select * from data_table inner join category  
            on data_table.category_id = category.category_id inner join sub_category  
            on data_table.sub_category_id = sub_category.sub_category_id where element_description
             like '%" . $request->keyword . "%' or category_name like '%" . $request->keyword . "%'
              or sub_category_name like '%" . $request->keyword . "%' 
              or data_table.ui_position like '%" . $request->keyword . "%'"));

        return json_encode($data);
    }

    public function edit($id)
    {
        $dataTableId = $id;
        $selectedData = DataTable::find($dataTableId);
        $data = DataTable::orderBy('data_table_id', 'DESC')->paginate(10);
        $elements = Elem::all();
        $subCategories = SubCategory::all();

        return view(
            self::DATA_BLADE,
            [
                'elements' => $elements,
                'data' => $data,
                'subCategories' => $subCategories,
                'selectedData' => $selectedData
            ]
        );
    }

    public function del(Request $request)
    {

        $dataTableId = $request->deletedata;
        $data = DataTable::find($dataTableId);

        $data->delete();

        Session::flash('success', 'Data record deleted successfully!');

        return redirect(self::DATA_HOME);
    }

    public function deleteItems(Request $request)
    {

        $deleteIds = $request->ids;

        foreach ($deleteIds as $id) {
            $record = DataTable::find($id);
            $record->delete();
        }
        return Response::json(['status' => 200, 'message' => 'Records deleted successfully']);
    }

    public function filter(Request $request)
    {
        $elemDesc = $request->filter_elem_description;
        $subCategoryName = $request->filter_sub_category_name;
        $month = $request->filter_month;
        $quarter = $request->filter_quarter;
        $year = $request->filter_year;
        $timeOfTrade = $request->filter_time_of_trade;

        if (!is_null($timeOfTrade)) {
            $timeOfTrade = $timeOfTrade . ':00';
        }

        if (strlen($quarter) > 0) {

            if (strlen($year) == 0) {
                Session::flash('error', 'You must select the year if a quarter is selected!');
                return redirect()->back();
            }
            $quarter = $quarter . "Q" . substr($year, 2, 2);
        }

        $query  =  DB::table('data_table')
            ->select(
                'data_table_id',
                'data_table_type',
                'element_description',
                'year',
                'month',
                "time_of_trade",
                'period',
                'data_value',
                'category_id',
                'ui_position',
                'slug',
                'default_period',
                'default_period_slug',
                'default_period_duration'
            );

        if (!empty($elemDesc)) {
            $query = $query->where('element_description', '=', $elemDesc);
        }
        if (!empty($month)) {
            $query = $query->where('month', '=', $month);
        }
        if (!empty($quarter)) {
            $query = $query->where('period', '=', $quarter);
        }
        if (!empty($subCategoryName)) {
            $query = $query->where('data_table_type', '=', strtoupper($subCategoryName));
        }
        if (!empty($year)) {
            $query = $query->where('year', '=', $year);
        }
        if (!empty($timeOfTrade)) {
            $query = $query->where('time_of_trade', '=', $timeOfTrade);
        }

        $data = $query->paginate(10);
        $data->appends([
            '_token' => $request->_token,
            'filter_elem_description' => $elemDesc,
            'filter_sub_category_name' => $subCategoryName,
            'filter_quarter' => $quarter,
            'filter_month' => $month,
            'filter_year' => $year,
            'filter_time_of_trade' => $timeOfTrade
        ]);

        $elements = Elem::all();
        $subCategories = SubCategory::all();

        return view(
            self::DATA_BLADE,
            [
                'elements' => $elements,
                'data' => $data,
                'subCategories' => $subCategories,
                'selectedData' => null,
                'elemDesc' => $elemDesc,
                'subCategoryName' => $subCategoryName,
                'month' => $month,
                'quarter' => $quarter,
                'timeOfTrade' => $timeOfTrade,
                'year' => $year
            ]
        );
    }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMessage extends Mailable
{
    use Queueable, SerializesModels;


    public $fullName;
    public $emailAddress;
    public $subject;
    public $msg;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(
        $data
    ) {
        $this->fullName = $data["fullname"];
        $this->emailAddress = $data['email_address'];
        $this->subject = $data['subject'];
        $this->msg = $data['message'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('webmaster@kestracapital.com')
            ->subject($this->subject)
            ->view('emails.message');
    }
}

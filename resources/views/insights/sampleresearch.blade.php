@extends('layouts.app')
<!-- Carousel section -->
@section('content')
<section id="team-top-section">
	<div class="py-5 brown-bg ">
		<h3 class="text-light text-center"> <strong>OUR INSIGHTS</strong> </h3>
		<h1 class="text-light text-center"> <strong>Sample Research</strong> </h1>
	</div>
	<div class="overflow-sec brown-bg">

	</div>
	<div class="container">


		<div class="row">
			<div class="col-md-12 mb-4">
				<div class="card team-wrap shadow pb-4">
					<div class="card-body ">
						@if ( Session::has('flash_message') )
						<div class="alert {{ Session::get('flash_type') }}">
							<h3>{{ Session::get('flash_message') }}</h3>
						</div>

						@endif

						@foreach($recommendation as $doc)
						<div class="sample-research">

							<h1 style="">{{ $doc->title }}</h1>
							<?php

							$fp = explode('.', $doc->description);
							echo $fp[0] . '.';
							?>
							<br />
							<br />
							<a href="<?= url('download/' . $doc->report_document) ?>" class="btn btn-success " style="background-color: #FFD938; border-color: #FFD938; color: #000"> <i class="bi bi-download mr-2"></i> <img src="/img/check-all.svg"> Download </a>

							<div class="published"> Published on :
								<?php
								$date = new DateTime($doc->date_of_report);

								echo $date->format('F , Y');

								?></div>

						</div>
						@endforeach

					</div>
				</div>

			</div>



		</div>
	</div>
</section>

<!-- Modal -->
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content" style="border:0px; border-radius: 10px; overflow: hidden;">

			<div class="modal-body p-0" style="overflow: hidden;">
				<div style="width: 100%; height: 150px; background-image: url('/img/about.jpeg'); background-size: cover; background-position: center;">
					<button type="button" class="btn-close pop-sr" data-bs-dismiss="modal" aria-label="Close" style="color: yellow; float: right;">X</button>
				</div>
				<div class="p-5">
					<div class="text-center mb-3" style="font-weight: lighter; font-size: 24px;">
						Sign up to get our insights
					</div>
					<div style="width: 100%;display: flex;justify-content: center;">
						<a href="https://web.kestrelcapital.com/" target="_blank">
							<button type="button" class="btn btn-success " style="background-color: #402B1B; border-color: #402B1B; color: #fff; font-weight: lighter;">Research Portal</button>
						</a>
					</div>
				</div>

			</div>

		</div>
	</div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
@if(!isset($_GET['show_mailing_list']))

<script type="text/javascript">
	$(window).on('load', function() {
		$('#exampleModal').modal('show');
	});
</script>
@else

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
	$(window).on('load', function() {
		//   swal("All Done!", "You may now receive our insights on mail!", "success");
	});
</script>
@endif
@endsection
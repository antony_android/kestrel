<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $icon_class
 * @property integer $category_id
 * @property string $sub_category_name
 * @property string $sub_category_slug
 * @property string $created
 * @property string $modified
 * @property integer $status
 * @property integer $user_id
 * @property Category $category
 */

class SubCategory extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'sub_category';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'sub_category_id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'category_id', 'sub_category_name', 'sub_category_slug',
        'icon_class', 'created', 'modified', 'status', 'user_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id', 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function elems()
    {
        return $this->hasMany('App\Models\Elem', 'sub_category_id', 'sub_category_id');
    }
}

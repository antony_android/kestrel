<style type="text/css">
	.navbar-light .navbar-nav .nav-link {
		color: #212121;
	}

	@media only screen and (max-width: 600px) {
		li.nav-item a {
			padding: 5px 15px !important;
			color: #212121 !important;
			text-decoration: none;
		}

		.custom_button_yellow {
			margin-bottom: 10px;
		}

		.custom_button_yellow_top {
			margin-top: 10px;
		}
	}
</style>

<header class="header shadow">
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<div class="container-fluid d-flex justify-content-between">
			<a class="navbar-brand" href="/" style="text-decoration: none;color: #632423;">
				<div class="logo-section pt-2" style="font-family: 'EB Garamond', serif; color: #632423; font-size: 28px; font-weight: bold;">
					KESTREL CAPITAL
				</div>
			</a>
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">

				<ul class="menu-list navbar-nav me-auto mb-2 mb-lg-0">

					<li class="nav-item">
						<a href="/">Home</a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle p-0" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false" style="padding-right: 10px!important;">
							Our Insights
						</a>
						<ul class="dropdown-menu shadow" aria-labelledby="navbarDropdown">
							<li><a class="dropdown-item" href="/insights/sampleresearch">Sample Research</a></li>
							<li><a class="dropdown-item" href="/insights/statistics/capital-markets-equities-nse-market-cap-usd-m/period/Quarter/1y">Key Statistics</a></li>
							<li><a class="dropdown-item" href="/insights/trends/price-trends-price-trends-safaricom-share-price/year/Year">Price Trends</a></li>
						</ul>
					</li>

					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle p-0" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false" style="padding-right: 10px!important;">
							What we Do
						</a>
						<ul class="dropdown-menu shadow" aria-labelledby="navbarDropdown">
							<li><a class="dropdown-item" href="/services/sales">Sales & Trading</a></li>
							<li><a class="dropdown-item" href="/services/research">Research</a></li>
							<li><a class="dropdown-item" href="/services/corporateadvisory">Corporate Advisory</a></li>
							<li><a class="dropdown-item" href="/services/privateplacements">Private Placements</a></li>

						</ul>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle p-0" href="#" id="navbarDropdown3" role="button" data-bs-toggle="dropdown" aria-expanded="false">
							About Us
						</a>
						<ul class="dropdown-menu shadow" aria-labelledby="navbarDropdown3">
							<li><a class="dropdown-item" href="/about">About Kestrel Capital</a></li>
							<li><a class="dropdown-item" href="/about/team">Our Team</a></li>
							<li><a class="dropdown-item" href="/about/nonexecutive">Non Executive Directors</a></li>

						</ul>
					</li>

					<li>
						<a href="/contact">Contact Us</a>
					</li>
					@if(Auth::user())
					<li class="nav-item dropdown" style="margin-right: 10px;">
						<a class="nav-link dropdown-toggle p-0" href="#" id="navbarDropdown3" role="button" data-bs-toggle="dropdown" aria-expanded="false">
							<i class="fas fa-user"></i> Admin
						</a>
						<ul class="dropdown-menu shadow" aria-labelledby="navbarDropdown3">
							<li><a class="dropdown-item" href="/admin/insights">Insights Data</a></li>
							@if(Auth::user()->user_role_id == 1)
							<li><a class="dropdown-item" href="/admin/elements">Data Elements</a></li>
							<li><a class="dropdown-item" href="/admin/elements/categories">Data Elements Categories</a></li>
							<li><a class="dropdown-item" href="/admin/elements/sub-categories">Data Elements Sub Categories</a></li>
							<li><a class="dropdown-item" href="/admin/users/roles">User Roles</a></li>
							<li><a class="dropdown-item" href="/admin/users">Users</a></li>
							@endif
							<li><a class="dropdown-item" href="/admin/user/change-password">Change Password</a></li>
							<li><a class="dropdown-item" href="/admin/users/logout">Logout</a></li>
						</ul>
					</li>
					@endif
					<li class=" custom_button_yellow custom_button_yellow_top" style="margin-right: 10px;">
						<a target="_blank" href="https://web.kestrelcapital.com/" class="research-button">Research Portal</a>
					</li>
					<li class=" custom_button_yellow">
						<a target="_blank" href="https://trading.kestrelcapital.com/TradeWeb/" class="research-button">Client Login</a>
					</li>

				</ul>
			</div>
		</div>
	</nav>

</header>
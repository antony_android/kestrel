<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Kestrel Capital</title>
  <link rel="icon" href="/favicon.ico" sizes="32x32" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
  <link href="/css/styles.css" rel="stylesheet" />
  <link href="/css/jquery.datetimepicker.css" rel="stylesheet" />
  <link href="/css/force-scrollbar-visible" rel="stylesheet" />

  <script src=" /js/jquery.js">
  </script>
  <script src="/js/kestrel.js"></script>

  <script src="/js/php-date-formatter.min.js"></script>
  <script src="/js/jquery.mousewheel.js"></script>
  <script src="/js/jquery.datetimepicker.full.min.js"></script>
</head>

<body id="page-top">
  @include('components.header')
  @include('flash-message')
  @yield('content')
  @include('components.footer')

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
  <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

  <script>
    AOS.init();
  </script>
  <!-- Core theme JS-->
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script src="/js/scripts.js"></script>
  <script src="/js/force-scrollbar-visible.js"></script>

  <script>
    $(document).ready(function() {
      $(".alert-success").fadeTo(2000, 500).slideUp(500, function() {
        $(".alert-success").slideUp(500);
      });
    });
  </script>

  @yield('extra-js')


</body>

</html>
<script>
  $(document).ready(function() {
    $(body).css("overflow", "visible");
  });
</script>
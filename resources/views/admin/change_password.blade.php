@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center m-15">
        <div class="col-sm-10">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-key login-fa"></i>
                    {{ __('Change Password') }}
                </div>

                <div class="card-body">
                    <form method="POST" action="/admin/user/changepassword/save">
                        @csrf

                        @foreach ($errors->all() as $error)
                        <p class="text-danger">{{ $error }}</p>
                        @endforeach

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Current Password <i class="fas fa-asterisk asterisk"></i></label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="current_password" autocomplete="current-password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">New Password <i class="fas fa-asterisk asterisk"></i></label>

                            <div class="col-md-6">
                                <input id="new_password" type="password" class="form-control" name="new_password" autocomplete="current-password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Confirm New Password <i class="fas fa-asterisk asterisk"></i></label>

                            <div class="col-md-6">
                                <input id="new_confirm_password" type="password" class="form-control" name="new_confirm_password" autocomplete="current-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Update Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
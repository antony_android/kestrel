<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

use App\Models\SubCategory;
use App\Models\Category;
use App\Models\DataTable;
use App\Models\Elem;

use Illuminate\Support\Facades\Auth;

class ElemController extends Controller
{

    const ELEMENTS_BLADE = 'admin/elements';
    const ELEMENTS_HOME = '/admin/elements';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $elements = Elem::paginate(10);
        $subCategories = SubCategory::all();
        $categories = Category::all();

        return view(
            self::ELEMENTS_BLADE,
            [
                'elements' => $elements,
                'subCategories' => $subCategories,
                'categories' => $categories,
                'selectedElement' => null
            ]
        );
    }

    public function save(Request $request)
    {

        $data = $request->validate([
            'elem_description' => 'required', 'string', 'max:255',
            'ui_position' => ['required'],
            'category_id' => ['required'],
            'sub_category_id' => ['required'],
        ]);

        $data['user_id'] = Auth::user()->id;
        $data['status'] = 1;
        $data['slug'] = $this->generateSlug(
            $data['elem_description'],
            $data['category_id'],
            $data['sub_category_id']
        );
        $data['default_period_slug'] = 'year';
        $data['default_period'] = 'Year';
        $data['priority'] = $request->priority;

        $selected = $request->selected_element;

        if ($selected) {
            tap(Elem::where('elem_id', $selected)->update($data));
        } else {
            tap(new Elem($data))->save();
        }

        Session::flash('success', 'Data Element details saved successfully!');
        return redirect(self::ELEMENTS_HOME);
    }

    public function search(Request $request)
    {

        $elements = DB::select(Db::raw("select * from elem inner join category  
            on elem.category_id = category.category_id inner join sub_category  
            on elem.sub_category_id = sub_category.sub_category_id where elem_description
             like '%" . $request->keyword . "%' or category_name like '%" . $request->keyword . "%'
              or sub_category_name like '%" . $request->keyword . "%' 
              or elem.ui_position like '%" . $request->keyword . "%'"));

        return json_encode($elements);
    }

    public function edit($id)
    {
        $elemId = $id;
        $selectedElement = Elem::find($elemId);
        $elements = Elem::paginate(10);
        $subCategories = SubCategory::all();
        $categories = Category::all();

        return view(
            self::ELEMENTS_BLADE,
            [
                'elements' => $elements,
                'subCategories' => $subCategories,
                'categories' => $categories,
                'selectedElement' => $selectedElement
            ]
        );
    }

    public function del(Request $request)
    {

        $elemId = $request->deleteelement;
        $element = Elem::find($elemId);

        $elemsExists = DataTable::where('slug', '=', $element->slug)->first();

        if (!is_null($elemsExists)) {
            Session::flash('error', 'Cannot delete a data element that has dependent data items!');
            return redirect()->back();
        }
        $element->delete();

        Session::flash('success', 'Data element deleted successfully!');

        return redirect(self::ELEMENTS_HOME);
    }

    private function generateSlug($name, $categoryId, $subCategoryId)
    {

        $categorySlug = "";
        $subCategorySlug = "";
        $category = Category::findOrFail($categoryId);
        if (!is_null($category)) {
            $categorySlug = $category->category_slug;
        }

        $subCategory = SubCategory::findOrFail($subCategoryId);
        if (!is_null($subCategory)) {
            $subCategorySlug = $subCategory->sub_category_slug;
        }

        $processableItems = array(
            "<" => "", ">" => "", "%" => "percent", ")" => "", "(" => "", " " => "-", "_" => "-"
        );

        $slug =  $name;
        foreach ($processableItems as $key => $value) {

            while (strpos($slug, $key) !== false) {
                $slug = str_replace($key, $value, $slug);
            }
        }

        return $categorySlug . "-" . $subCategorySlug . "-" . strtolower($slug);
    }

    public function getSubCategories(Request $request)
    {

        $subCategories = DB::select(Db::raw("select * from sub_category where 
            category_id = '" . $request->category_id . "'"));

        return json_encode($subCategories);
    }
}

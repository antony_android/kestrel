<?php

namespace App\Imports;

use App\Models\UploadData;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class UploadDataImports implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {

        return new UploadData([
            'year'     => $row['Year'],
            'period'    => $row['Quarter'],
            'month'    => $row['Month'],
            'value'    => $row['Value'],
        ]);
    }
}

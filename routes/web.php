<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;

use App\Reports\StatisticsReport;
use App\Reports\TrendsReport;
use Illuminate\Support\Facades\Auth;

Route::get('/', 'WelcomeController@index');
Route::get('/admins', 'AdminsController@index');
Route::get('/about', 'AboutController@index');
Route::get('/about/team', 'AboutController@team');
Route::get('/about/nonexecutive', 'AboutController@nonexecutive');
Route::get('/about/overviewnse',  'AboutController@overviewnse');
Route::get('/about/transactionhistory', 'AboutController@transactionhistory');
Route::post('/send-message', 'ContactController@sendEmail');
Route::get('/about/usefullinks', 'AboutController@usefullinks');
// Contact Controller
Route::get('/contact', 'ContactController@index');
// Services
Route::get('/services/sales', 'ServicesController@sales');
Route::get('/services/research', 'ServicesController@research');
Route::get('/services/privateplacements', 'ServicesController@privateplacements');
Route::get('/services/corporateadvisory', 'ServicesController@corporateadvisory');
// Insights
Route::get('/insights/sampleresearch', 'InsightsController@sampleresearch');
Route::post('/insights/savemail', 'InsightsController@savemail');

Route::get('/admin/users/roles', 'UserRoleController@index')->name('index')->middleware(['admin']);
Route::post('/admin/users/role/save', 'UserRoleController@save')->name('save')->middleware(['admin']);
Route::post('/admin/users/roles/search', 'UserRoleController@search')->name('search')->middleware(['admin']);
Route::get('/admin/users/role/edit/{id}', 'UserRoleController@edit')->name('edit')->middleware(['admin']);
Route::post('/admin/users/role/del', 'UserRoleController@del')->name('del')->middleware(['admin']);

Route::get('/admin/elements/categories', 'CategoryController@index')->name('index')->middleware(['admin']);
Route::post('/admin/elements/categories/save', 'CategoryController@save')->name('save')->middleware(['admin']);
Route::post('/admin/elements/categories/search', 'CategoryController@search')->name('search')->middleware(['admin']);
Route::get('/admin/elements/categories/edit/{id}', 'CategoryController@edit')->name('edit')->middleware(['admin']);
Route::post('/admin/elements/categories/del', 'CategoryController@del')->name('del')->middleware(['admin']);

Route::get('/admin/elements/sub-categories', 'SubCategoryController@index')->name('index')->middleware(['admin']);
Route::post('/admin/elements/sub-categories/save', 'SubCategoryController@save')->name('save')->middleware(['admin']);
Route::post('/admin/elements/sub-categories/search', 'SubCategoryController@search')->name('search')->middleware(['admin']);
Route::get('/admin/elements/sub-categories/edit/{id}', 'SubCategoryController@edit')->name('edit')->middleware(['admin']);
Route::post('/admin/elements/sub-categories/del', 'SubCategoryController@del')->name('del')->middleware(['admin']);

Route::get('/admin/elements', 'ElemController@index')->name('index')->middleware(['admin']);
Route::post('/admin/elements/save', 'ElemController@save')->name('save')->middleware(['admin']);
Route::post('/admin/elements/search', 'ElemController@search')->name('search')->middleware(['admin']);
Route::get('/admin/elements/edit/{id}', 'ElemController@edit')->name('edit')->middleware(['admin']);
Route::post('/admin/elements/del', 'ElemController@del')->name('del')->middleware(['admin']);
Route::post('/admin/elements/sub-categories', 'ElemController@getSubCategories')->name('getSubCategories')->middleware(['admin']);

Route::get('/admin/insights', 'DataTableController@index')->name('index');
Route::post('/admin/insights/save', 'DataTableController@save')->name('save');
Route::post('/admin/insights/upload', 'DataTableController@upload')->name('upload');
Route::post('/admin/insights/search', 'DataTableController@search')->name('search');
Route::get('/admin/insights/edit/{id}', 'DataTableController@edit')->name('edit');
Route::post('/admin/insights/del', 'DataTableController@del')->name('del');
Route::get('/admin/insights/filter', 'DataTableController@filter')->name('filter');
Route::post('/admin/insights/delete-items', 'DataTableController@deleteItems')->name('deleteItems');

Route::get('/admin/users', 'UserController@index')->name('index')->middleware(['admin']);
Route::post('/admin/users/save', 'UserController@save')->name('save')->middleware(['admin']);
Route::post('/admin/users/search', 'UserController@search')->name('search')->middleware(['admin']);
Route::get('/admin/users/edit/{id}', 'UserController@edit')->name('edit')->middleware(['admin']);
Route::get('/admin/users/approve/{id}', 'UserController@approve')->name('approve')->middleware(['admin']);
Route::post('/admin/users/del', 'UserController@del')->name('del')->middleware(['admin']);
Route::get('/admin/user/change-password', 'UserController@changePassword')->name('changePassword');
Route::post('/admin/user/changepassword/save', 'UserController@changePasswordSave')->name('changePasswordSave');
Route::get('/admin/users/logout', 'UserController@logout')->name('logout');

Route::get('/insights/statistics/{report}/{period}/{periodTitle}/{years?}', function ($report, $period, $periodTitle, $filterPeriod = null) {

    if (is_null($filterPeriod)) {
        $filterPeriod = "ally";
    }
    $report = new StatisticsReport(
        array(
            "report" => $report,
            "period" => $period,
            "periodTitle" => $periodTitle,
            "filterPeriod" => $filterPeriod,
            "report_titles" => config('reports.report_titles')
        )
    );
    $report->run();
    return view("report", ["report" => $report]);
});


Route::get('/insights/trends/{report}/{period}/{periodTitle}/{years?}', function ($report, $period, $periodTitle, $filterPeriod = null) {

    if (is_null($filterPeriod)) {
        $filterPeriod = "ally";
    }

    $periodToSelect = $filterPeriod;

    if ($periodToSelect == "1d") {

        $periodToSelect = "HOUR(time_of_trade)";
        $intervalPeriod = " INTERVAL 1 DAY ";
    } else if ($periodToSelect == "1w") {

        $periodToSelect = 'DATE_FORMAT(DATE(time_of_trade), "%b %D")';
        $intervalPeriod = " INTERVAL 1 WEEK ";
    } else if ($periodToSelect == "1m") {

        $periodToSelect = 'DATE_FORMAT(DATE(time_of_trade), "%b %e, %y")';
        $intervalPeriod = " INTERVAL 1 MONTH ";
    } else if ($periodToSelect == "6m") {

        $periodToSelect = 'DATE_FORMAT(DATE(time_of_trade), "%b %e, %y")';
        $intervalPeriod = " INTERVAL 6 MONTH ";
    } else if ($periodToSelect == "ytd") {

        $periodToSelect = 'DATE_FORMAT(DATE(time_of_trade), "%b %e, %y")';
    } else if ($periodToSelect == "1y") {

        $periodToSelect = 'DATE_FORMAT(DATE(time_of_trade), "%b %e, %y")';
        $intervalPeriod = " INTERVAL 1 YEAR ";
    } else if ($periodToSelect == "5y") {

        $periodToSelect = 'DATE_FORMAT(DATE(time_of_trade), "%b %e, %y")';
        $intervalPeriod = " INTERVAL 5 YEAR ";
    } else if ($periodToSelect == "ally") {

        $periodToSelect = 'DATE_FORMAT(DATE(time_of_trade), "%b %e, %y")';
    }

    $reportTitles = config('reports.trends_titles');

    $query = "SELECT " . $periodToSelect . " AS " . $periodTitle . ",
                        data_value as Amount FROM data_table WHERE 
                        element_description='" . $reportTitles[$report] . "'";

    if ($filterPeriod != "ally") {

        if ($filterPeriod != "ytd") {

            $lastData = DB::select("SELECT DATE(time_of_trade-" . $intervalPeriod . ") AS last_date FROM data_table
                 WHERE element_description='" . $reportTitles[$report] . "' ORDER BY DATE(time_of_trade)
                  DESC LIMIT 1");

            if ($lastData) {
                $lastDate = $lastData[0]->last_date;
                $query = "SELECT " . $periodToSelect . " AS " . $periodTitle . ",
                        data_value as Amount FROM data_table WHERE 
                        element_description='" . $reportTitles[$report] . "'
                        AND DATE(time_of_trade) >= '" . $lastDate . "' AND DATE(time_of_trade) <= CURDATE()";
            }
        } else {
            $query = "SELECT " . $periodToSelect . " AS " . $periodTitle . ",
                        data_value as Amount FROM data_table WHERE 
                        element_description='" . $reportTitles[$report] . "'
                        AND YEAR(time_of_trade) = YEAR(CURDATE())";
        }
    }

    $report = new TrendsReport(
        array(
            "report" => $report,
            "period" => $period,
            "periodTitle" => $periodTitle,
            "filterPeriod" => $filterPeriod,
            "color_code" => TrendsReport::checkTrend($query),
            "report_titles" => $reportTitles
        )
    );
    $report->run();
    return view("report", ["report" => $report]);
});


Route::get('/download/{file}', 'DownloadsController@download');

Route::get('test/titles', function () {

    dd(config('reports.data'));
});

Route::get('generate/slug', function () {

    $data = DB::select('SELECT data_table_id, element_description, data_table_type,
     category_slug FROM data_table INNER JOIN category USING(category_id) WHERE 
     slug IS NULL');

    foreach ($data as $item) {

        $elemDesc = $item->element_description;
        $dataTableType = $item->data_table_type;
        $id = $item->data_table_id;
        $categorySlug = $item->category_slug;

        $processedElemDesc = strtolower(str_replace("<", "", str_replace(">", "", str_replace("%", "percent", str_replace(")", "", str_replace("(", "", str_replace(" ", "-", $elemDesc)))))));
        $processedDataTableType = strtolower(str_replace("_", "-", str_replace(" ", "-", $dataTableType)));

        $slug = $categorySlug . "-" . $processedDataTableType . "-" . $processedElemDesc;
        $affected = DB::table('data_table')
            ->where('data_table_id', $id)
            ->update(['slug' => $slug]);

        if ($affected) {
            echo "Updated the record(s) successfully. $affected rows affected.<br/>";
        }
    }
});

Route::get('generate/elems', function () {

    $data = DB::select('SELECT DISTINCT element_description, slug,
     ui_position, category_id, sub_category_id FROM data_table');

    foreach ($data as $item) {

        $elemDesc = $item->element_description;
        $slug = $item->slug;
        $uiPosition = $item->ui_position;
        $categoryId = $item->category_id;
        $subCategoryId = $item->sub_category_id;

        $affected = DB::table('elem')
            ->insertOrIgnore([
                'elem_description' => $elemDesc,
                'elem_slug' => $slug,
                'ui_position' => $uiPosition,
                'category_id' => $categoryId,
                'sub_category_id' => $subCategoryId,
            ]);

        if ($affected) {
            echo "Inserted the record(s) successfully. $affected rows affected.<br/>";
        }
    }
});

Route::get('update/elems', function () {

    $data = DB::select('SELECT DISTINCT element_description,
     default_period, default_period_slug, default_period_duration
      FROM data_table');

    foreach ($data as $item) {

        $elemDesc = $item->element_description;
        $defaultPeriod = $item->default_period;
        $defaultPeriodSlug = $item->default_period_slug;
        $defaultPeriodDuration = $item->default_period_duration;

        $affected = DB::table('elem')
            ->where('elem_description', '=', $elemDesc)
            ->update([
                'default_period' => $defaultPeriod,
                'default_period_slug' => $defaultPeriodSlug,
                'default_period_duration' => $defaultPeriodDuration,
            ]);

        if ($affected) {
            echo "Updated the record(s) successfully. $affected rows affected.<br/>";
        }
    }
});

Auth::routes();
Route::get('base_url',function(){
$url= config(app.url);
echo $url;

});

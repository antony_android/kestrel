@extends('layouts.app')
<!-- Carousel section -->
@section('content')
<section id="team-top-section">
	<div class="py-5 brown-bg ">
		<h3 class="text-light text-center"> <strong>Kestrel Capital</strong> </h3>
		<h1 class="text-light text-center"> <strong>Overview of Nairobi Securities Exchange</strong> </h1>
	</div>
	<div class="overflow-sec brown-bg">
		
	</div>
	<div class="container">
		
		
		<div class="row">
				<div class="col-md-12 mb-4">
				<div class="card team-wrap shadow py-4">
					<div class="card-body " style="font-size: 13px;">
						<p>
							The Nairobi Securities Exchange comprises approximately 66 listed companies with a daily trading volume of approximately USD 10 million and a total market capitalization of approximately USD 23 billion. <br><br>

Aside from equities, Government and corporate bonds are also traded on the Nairobi Securities Exchange. Automated bond trading started in November 2009 with the KES 25 billion KenGen bond. Average bond daily trading is approximately USD 10m.<br><br>

Trading hours are from 09:00 to 15:00. Delivery and settlement is done scripless via an electronic Central Depository System (CDS) which was installed in 2005. Settlement is T+3 on a delivery-vs-payment basis. The daily price movement for any security in a single trading session shall not be more than 10% except during major corporate announcements.<br><br>

The Nairobi Securities Exchange in 2006 introduced an Automated Trading System (ATS) which ensures that orders are matched automatically and are executed by stockbrokers on a first come/first serve basis. The ATS has now been linked to the Central Bank of Kenya and the CDS thereby allowing electronic trading of Government bonds.<br><br>

Securities lending, short selling and same day turn-around trades are not yet permitted. Futures trading of currencies and equities indices will commence in 2016 or 2017.<br><br>

Foreign ownership limits were lifted in July 2015 and foreigners can now hold over 75% of NSE listed companies.<br><br>

There are no foreign exchange controls in Kenya. Capital Gains Tax of 5% was introduced on 1 January 2015, applicable to all trades on the Nairobi Securities Exchange, however this will be amended to a withholding tax of 0.3% on all transactions from 1st January 2016. Dividend withholding tax for foreigners is a final 10%.<br><br>

More information on the Nairobi Securities Exchange and the Capital Markets Authority rules and regulations may be obtained from their websites which are indicated under Useful Links to the left.<br><br>

To view or download the Nairobi Securities Exchange Daily Price List, click on the Nairobi Securities Exchange website www.nse.co.ke
<br><br>
Live daily stock quotes for the Nairobi Securities Exchange can be obtained from Bloomberg and Reuters.<br><br>
						</p>
						
					</div>
				</div>
				
			</div>
			

			
		</div>
	</div>
</section>
@endsection
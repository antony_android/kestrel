<html>

<head></head>

<body>
    <div style="padding: 30px;">
        <div style="width:100%;height:70px;background:#3E281A;color:#FFF;font-size:15px;text-align:center;padding:10px 0px;">From Send Message Form</div>
        <div id="body" style="height: 400px;padding:20px;border:1px solid #ddd;">
            Dear Kestrel,

            <p>There is an inquiry from the Website at the Send Message Form at the Contact Us Page with the following details:</p>

            <p>
                <strong>Name: </strong> {{ $fullName }}
            </p>
            <p>
                <strong>Email Address: </strong> {{ $emailAddress }}
            </p>
            <p>
                <strong>Subject: </strong> {{ $subject }}
            </p>
            <p>
                <strong>Message: </strong> {{ $msg }}
            </p>
        </div>
        <div id="footer" style="width:100%;height:50px;background:#3E281A;color:#FFF;font-size:13px;text-align:center;padding:10px;">
            Copyright &copy; Kestrel Capital Limited
        </div>
    </div>
</body>

</html>
<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class WelcomeController extends Controller
{
	public function index()
	{
		$recommendation = DB::connection('mysql2')->select("select * from kestra.recommendation
					where public=1 order by recommendation_id  desc limit 8");
	   return view('welcome')->with('recommendation', $recommendation);
	}
}

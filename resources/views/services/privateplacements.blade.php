@extends('layouts.app')
<!-- Carousel section -->
@section('content')
<section id="team-top-section">
  <div class="py-5 brown-bg ">
    <h3 class="text-light text-center"> <strong>WHAT WE DO</strong> </h3>
    <h1 class="text-light text-center"> <strong>Private Placements </strong> </h1>
  </div>
  <div class="overflow-sec brown-bg">

  </div>
  <div class="container">


    <div class="row">
      <div class="col-md-12 mb-4">
        <div class="card team-wrap shadow pb-4">
          <div class="card-body">
            <div class="p-4 mb-4">
              <p class="mb-0 box-header">
                Kestrel Capital originates, structures and executes private placements of a variety of securities: equities, debt and related products. </p>
              <p>
            </div>
            <div class="row services-font">
              <div class="col-md-6">

                <p class="small-text">
                  Private placements allow private companies to broaden their sources of capital, ultimately benefiting the company to lower its cost of capital. Private placements also act as a stepping-stone to issuing securities to the general public by way of Listing.

                </p>
                <p class="small-text">
                  Investor appetite, both institutional and retail, for well-structured investment products is on the rise as investors seek diversification and early access to high growth sectors.
                </p>
              </div>
              <div class="col-md-6">
                <h3>Services</h3>
                <ul class="small-text">
                  <li>Commercial paper origination and placement </li>
                  <li>
                    Over the Counter (OTC) set-up and trading
                  </li>
                  <li>
                    Private Equity sell side advisory

                  </li>

                </ul>
              </div>
            </div>

            <!-- <h3>Private Placement Highlights</h3> -->
            <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width="100%" style='width:100.0%;border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
 mso-yfti-tbllook:1184;mso-padding-alt:0in 0in 0in 0in;mso-border-insideh:.5pt solid windowtext;
 mso-border-insidev:.5pt solid windowtext'>
              <thead>
                <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
                  <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;
   mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
   line-height:normal'><b><span style='font-size:10.0pt;font-family:"inherit",serif;
   mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
   color:#444444;mso-ansi-language:EN-US'>Date<o:p></o:p></span></b></p>
                  </td>
                  <td width="22%" style='width:22.18%;border:solid windowtext 1.0pt;
   border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
   solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
   line-height:normal'><b><span style='font-size:10.0pt;font-family:"inherit",serif;
   mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
   color:#444444;mso-ansi-language:EN-US'>Company<o:p></o:p></span></b></p>
                  </td>
                  <td width="24%" style='width:24.48%;border:solid windowtext 1.0pt;
   border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
   solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
   line-height:normal'><b><span style='font-size:10.0pt;font-family:"inherit",serif;
   mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
   color:#444444;mso-ansi-language:EN-US'>Transaction<o:p></o:p></span></b></p>
                  </td>
                  <td width="25%" style='width:25.12%;border:solid windowtext 1.0pt;
   border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
   solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
   line-height:normal'><b><span style='font-size:10.0pt;font-family:"inherit",serif;
   mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
   color:#444444;mso-ansi-language:EN-US'>Role<o:p></o:p></span></b></p>
                  </td>
                  <td width="15%" style='width:15.2%;border:solid windowtext 1.0pt;border-left:
   none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
   padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
   line-height:normal'><b><span style='font-size:10.0pt;font-family:"inherit",serif;
   mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
   color:#444444;mso-ansi-language:EN-US'>Industry<o:p></o:p></span></b></p>
                  </td>
                </tr>
              </thead>
              <tr style='mso-yfti-irow:1'>
                <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Jan-18<o:p></o:p></span></p>
                </td>
                <td width="22%" style='width:22.18%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Flame Tree Group<o:p></o:p></span></p>
                </td>
                <td width="24%" style='width:24.48%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Commercial Paper<o:p></o:p></span></p>
                </td>
                <td width="25%" style='width:25.12%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Sole Arranger and Placing Agent<o:p></o:p></span></p>
                </td>
                <td width="15%" style='width:15.2%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Consumer goods<o:p></o:p></span></p>
                </td>
              </tr>
              <tr style='mso-yfti-irow:2'>
                <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Jun-16<o:p></o:p></span></p>
                </td>
                <td width="22%" style='width:22.18%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>ARM Cement<o:p></o:p></span></p>
                </td>
                <td width="24%" style='width:24.48%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Equity Investment by CDC<o:p></o:p></span></p>
                </td>
                <td width="25%" style='width:25.12%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Independent Financial Advisor<o:p></o:p></span></p>
                </td>
                <td width="15%" style='width:15.2%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Construction Materials (Cement)<o:p></o:p></span></p>
                </td>
              </tr>
              <tr style='mso-yfti-irow:3'>
                <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Oct-15<o:p></o:p></span></p>
                </td>
                <td width="22%" style='width:22.18%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Stockport Exploration<o:p></o:p></span></p>
                </td>
                <td width="24%" style='width:24.48%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Private Placement of Equity<o:p></o:p></span></p>
                </td>
                <td width="25%" style='width:25.12%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Financial Advisor and Placing Agent<o:p></o:p></span></p>
                </td>
                <td width="15%" style='width:15.2%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Metals Exploration (Gold)<o:p></o:p></span></p>
                </td>
              </tr>
              <tr style='mso-yfti-irow:4'>
                <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Dec-14<o:p></o:p></span></p>
                </td>
                <td width="22%" style='width:22.18%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span class=SpellE><span style='font-size:10.0pt;
  font-family:Roboto;mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
  "Times New Roman";color:#626262;mso-ansi-language:EN-US'>Mayfox</span></span><span style='font-size:10.0pt;font-family:Roboto;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:#626262;mso-ansi-language:EN-US'>
                      Mining<o:p></o:p></span></p>
                </td>
                <td width="24%" style='width:24.48%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Private Placement of Equity<o:p></o:p></span></p>
                </td>
                <td width="25%" style='width:25.12%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Transaction Advisor and Sole Placing
                      Agent<o:p></o:p></span></p>
                </td>
                <td width="15%" style='width:15.2%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Metals Exploration (Gold)<o:p></o:p></span></p>
                </td>
              </tr>
              <tr style='mso-yfti-irow:5'>
                <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Feb-14<o:p></o:p></span></p>
                </td>
                <td width="22%" style='width:22.18%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>ARM Cement<o:p></o:p></span></p>
                </td>
                <td width="24%" style='width:24.48%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Commercial Paper<o:p></o:p></span></p>
                </td>
                <td width="25%" style='width:25.12%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Financial Advisor and Placing Agent<o:p></o:p></span></p>
                </td>
                <td width="15%" style='width:15.2%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Construction Materials (Cement)<o:p></o:p></span></p>
                </td>
              </tr>
              <tr style='mso-yfti-irow:6'>
                <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Jan-14<o:p></o:p></span></p>
                </td>
                <td width="22%" style='width:22.18%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>TPS Eastern Africa (Serena Hotels)<o:p></o:p></span></p>
                </td>
                <td width="24%" style='width:24.48%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Commercial Paper<o:p></o:p></span></p>
                </td>
                <td width="25%" style='width:25.12%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Financial Advisor and Placing Agent<o:p></o:p></span></p>
                </td>
                <td width="15%" style='width:15.2%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Consumer services (Hotels)<o:p></o:p></span></p>
                </td>
              </tr>
              <tr style='mso-yfti-irow:7'>
                <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Oct-13<o:p></o:p></span></p>
                </td>
                <td width="22%" style='width:22.18%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Stockport Exploration<o:p></o:p></span></p>
                </td>
                <td width="24%" style='width:24.48%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Private Placement of Convertible
                      Debentures<o:p></o:p></span></p>
                </td>
                <td width="25%" style='width:25.12%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Sole Placing Agent and Financial
                      Advisor<o:p></o:p></span></p>
                </td>
                <td width="15%" style='width:15.2%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Metals Exploration (Gold)<o:p></o:p></span></p>
                </td>
              </tr>
              <tr style='mso-yfti-irow:8'>
                <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Jul-13<o:p></o:p></span></p>
                </td>
                <td width="22%" style='width:22.18%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span class=SpellE><span style='font-size:10.0pt;
  font-family:Roboto;mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
  "Times New Roman";color:#626262;mso-ansi-language:EN-US'>KenolKobil</span></span><span style='font-size:10.0pt;font-family:Roboto;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:#626262;mso-ansi-language:EN-US'>
                      Petroleum<o:p></o:p></span></p>
                </td>
                <td width="24%" style='width:24.48%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Commercial Paper (private)<o:p></o:p></span></p>
                </td>
                <td width="25%" style='width:25.12%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Financial Advisor and Placing Agent<o:p></o:p></span></p>
                </td>
                <td width="15%" style='width:15.2%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Oil Marketing<o:p></o:p></span></p>
                </td>
              </tr>
              <tr style='mso-yfti-irow:9'>
                <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Mar-13<o:p></o:p></span></p>
                </td>
                <td width="22%" style='width:22.18%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Stockport Exploration<o:p></o:p></span></p>
                </td>
                <td width="24%" style='width:24.48%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Private Placement of Equity<o:p></o:p></span></p>
                </td>
                <td width="25%" style='width:25.12%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Placing Agent and Financial Advisor<o:p></o:p></span></p>
                </td>
                <td width="15%" style='width:15.2%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Metals Exploration (Gold)<o:p></o:p></span></p>
                </td>
              </tr>
              <tr style='mso-yfti-irow:10'>
                <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Jul-11<o:p></o:p></span></p>
                </td>
                <td width="22%" style='width:22.18%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span class=SpellE><span style='font-size:10.0pt;
  font-family:Roboto;mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
  "Times New Roman";color:#626262;mso-ansi-language:EN-US'>Kenol</span></span><span style='font-size:10.0pt;font-family:Roboto;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:#626262;mso-ansi-language:EN-US'>
                      <span class=SpellE>Kobil</span>
                      <o:p></o:p>
                    </span></p>
                </td>
                <td width="24%" style='width:24.48%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Commercial Paper (public)<o:p></o:p></span></p>
                </td>
                <td width="25%" style='width:25.12%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Sole Placing Agent<o:p></o:p></span></p>
                </td>
                <td width="15%" style='width:15.2%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Oil Marketing<o:p></o:p></span></p>
                </td>
              </tr>
              <tr style='mso-yfti-irow:11'>
                <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Aug-06<o:p></o:p></span></p>
                </td>
                <td width="22%" style='width:22.18%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Deacons Kenya<o:p></o:p></span></p>
                </td>
                <td width="24%" style='width:24.48%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Private Equity Placement<o:p></o:p></span></p>
                </td>
                <td width="25%" style='width:25.12%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Sole Placing Agent and Transaction
                      Advisor<o:p></o:p></span></p>
                </td>
                <td width="15%" style='width:15.2%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Retail (Apparel and Home)<o:p></o:p></span></p>
                </td>
              </tr>
              <tr style='mso-yfti-irow:12'>
                <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Oct-01<o:p></o:p></span></p>
                </td>
                <td width="22%" style='width:22.18%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>ICDC Investments<o:p></o:p></span></p>
                </td>
                <td width="24%" style='width:24.48%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Equity Offer<o:p></o:p></span></p>
                </td>
                <td width="25%" style='width:25.12%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Lead Sponsoring Broker<o:p></o:p></span></p>
                </td>
                <td width="15%" style='width:15.2%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Diversified Investment Holdings<o:p></o:p></span></p>
                </td>
              </tr>
              <tr style='mso-yfti-irow:13;mso-yfti-lastrow:yes'>
                <td width="13%" style='width:13.02%;border:solid windowtext 1.0pt;border-top:
  none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Feb-00<o:p></o:p></span></p>
                </td>
                <td width="22%" style='width:22.18%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>African Lakes Corporation<o:p></o:p></span></p>
                </td>
                <td width="24%" style='width:24.48%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Equity Offer<o:p></o:p></span></p>
                </td>
                <td width="25%" style='width:25.12%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Lead Sponsoring Broker<o:p></o:p></span></p>
                </td>
                <td width="15%" style='width:15.2%;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:7.5pt 7.5pt 7.5pt 7.5pt'>
                  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span style='font-size:10.0pt;font-family:Roboto;
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:#626262;mso-ansi-language:EN-US'>Information Technology<o:p></o:p></span></p>
                </td>
              </tr>
            </table>


          </div>
        </div>

      </div>



    </div>
  </div>
</section>
@endsection
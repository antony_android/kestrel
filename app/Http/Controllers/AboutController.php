<?php

namespace App\Http\Controllers;

class AboutController extends Controller
{
	public function index()
	{
		return view('about.index');
	}
	public function team()
	{
		$profiles =
			array(
				[
					"name" => "Francis Mwangi, CFA, FCCA",
					"title" => "CEO & Executive Director ",
					"description" => "Francis joined Kestrel Capital in 2018 from Standard Investment Bank where he was the Head of Research. He has previously worked at African Alliance Investment Bank and Stanbic Bank (previously CFC Bank). He is a CFA Charter holder, holds a Bachelor of commerce (Finance Major) honors degree from the University of Nairobi and is a Fellow Chartered Certified Accountant (ACCA, England & Wales). "
				],
				[
					"name" => "Nicholas Mukele, FCCA",
					"title" => "CFO & Executive Director ",
					"description" => "Nicholas joined Kestrel Capital in 2016 from SBG Securities (Standard Bank Group). He is a Fellow member of the Association of Chartered Certified Accountants (FCCA) UK, a member of the Institute of Certified Public Accountants of Kenya (ICPAK) and holds a BA (Business management major) degree from Moi University and an MBA (Finance) postgraduate degree from the University of Lincolnshire and Humberside (Lincoln) UK. "
				],
				[
					"name" => "Chris Miriti",
					"title" => "Head of Equity Sales & Trading",
					"description" => "Chris  joined Kestrel Capital in  2014  from African Alliance Investment Bank where he was the  head of trading.He has previously worked at SBG Securities(previously CFC Stanbic services), Kingdom securities (previously Bob Mathews stockbrokers) and Francis Drummond.He has an MBA from  African Nazarene University and Bsc.International Business Administration from  United States International University."
				],
				[
					"name" => "Sally Kotut",
					"title" => "Equity Sales & Trading",
					"description" => "Sally joined Kestrel Capital in 2010 from African Alliance Investment Bank where she worked as a Portfolio Manager. She has previously worked at SBG Securities (previously CFC Stanbic Financial Services). She holds a Bachelor of Business Administration degree majoring in Accounts from the University of Eastern Africa, Baraton and has passed Level I of the CFA® Program.  "
				],
				[
					"name" => "Amanda  Onyango",
					"title" => "Equity Sales & Trading ",
					"description" => "Amanda joined Kestrel Capital in  2014 having  worked previously at African Alliance Investment Bank and SBG securities where she was responsible for growing  equity sales as well as management  and placement of commercial paper programmes.Amanda has a Bsc. in Acturial science from  the  university of Nairobi."
				],
				[
					"name" => "Alexander Muiruri",
					"title" => "Head of Fixed Income Sales & Trading",
					"description" => "Alexander joined Kestrel Capital in 2014 from African Alliance Kenya Investment Bank where he worked as a Fixed Income Trader. He has an International Business Administration degree from the United States International University."
				],
				[
					"name" => "Edwin Muthaura",
					"title" => "Fixed Income Sales & Trading",
					"description" => "Edwin joined Kestrel Capital in 2018 from Dyer and Blair Investment Bank where he worked as a Fixed Income Trader. He has previously worked at Standard Chartered Bank. He holds a Bachelor of Commerce Degree from The University of Nairobi and is pursuing Master of Science Degree in Finance and Investments at The University of Nairobi."
				],
				[
					"name" => "Ephantus Maina, ACCA",
					"title" => "Senior Research Analyst ",
					"description" => "Ephantus joined Kestrel Capital in 2014 from the University of Nairobi where he graduated with a BSc in Actuarial Science. He is an ACCA (England & Wales) Affiliate, is currently pursuing his actuarial and CFA Program Level 3 exams. "
				],
				[
					"name" => "Maureen Kirigua, CFA",
					"title" => "Senior Research Analyst",
					"description" => "Maureen joined Kestrel Capital in 2021 from African Alliance Investment Bank where she was an Equity Analyst. She has previously worked at Sterling Investment Bank. She is a CFA Charter holder and holds a B.Sc. International Business Administration from United States International University."
				],
				[
					"name" => "Sophie Njau",
					"title" => "Head of Operations",
					"description" => "Sophie joined Kestrel Capital in 2014 from African Alliance Kenya Investment Bank where she worked as Head of Operations. She has a BSC in Computer Science from the Catholic University of Eastern Africa. "
				],
				[
					"name" => "Martin Anyika",
					"title" => "Operations Manager ",
					"description" => "Martin joined Kestrel Capital in 2007. He holds a Bachelor of Business Administration degree majoring in Accounting and Finance from Kenya Methodist University and is a CPA (K). "
				],
				[
					"name" => "Carlos Makau",
					"title" => "Chief Accountant ",
					"description" => "Carlos joined Kestrel Capital in year 2015 from Kingdom Securities Limited where he worked as a Financial Accountant. He holds a Bachelors of Commerce degree majoring in Accounting from Catholic University of Eastern Africa and is a CPA(K)."
				],
				[
					"name" => "Faith Muthoka",
					"title" => "Administrative Coordinator",
					"description" => "Faith joined Kestrel Capital in 2018 from Strathmore University where she graduated with a Bachelor of Commerce (Accounting and Finance) and is a CPA (K) Finalist."
				]

			);
		return view('about.team')->with('profiles', $profiles);
	}

	public function nonexecutive()
	{
		$profiles =
			array(
				[
					"name" => "Charles Field Marsham",
					"title" => "Chairman & Founder ",
					"description" => "<p>Charles Field-Marsham is a Canadian entrepreneur with more than 25 years of experience building businesses. In 1991, he joined investment bank, Credit Suisse First Boston in New York. He then lived and worked in Nairobi for 10 years where he started and acquired several businesses. In 2004, he founded Kestrel Capital Management Capital (KCMC), which provides strategic, investment and management consulting services to businesses in and outside Canada.</p>
<p>In addition to his leadership of Kestrel Capital Management Capital, Charles owns and is chairman of the following businesses: Kestrel Capital, which he developed into the leading investment bank and stock broker in Kenya; Panafrican Equipment Group, and Kenya Fluorspar, a fluorspar mine he purchased in 1997 but closed in 2017. His companies employ approximately 400 people and have offices in Canada, Dubai, Ghana, Sierra Leone, Nigeria, Kenya, Uganda and Tanzania. In January 2020, Charles invested in and became a board member of Slingshot Sports LLC, a world leading brand of kiteboarding equipment, headquartered in Hood River, Oregon.</p>
<p>Charles is involved with a range of charities and not-for-profit organizations. Charles is the principal benefactor and chairman of the Kenya Scholar Access Program (KENSAP), an initiative that since 2004 has helped secure full scholarships for more than 215 Kenyan students at top-tier American universities. He and his wife, Rita, also founded The Charles and Rita Field-Marsham Foundation. Previously, he sat on the boards of The Foundation of the Hospital for Sick Children, Next Canada, Grand Challenges Canada, and the African Medical Research Foundation Canada (AMREF).  </p> "
				],
				[
					"name" => "Scott Field-Marsham",
					"title" => "Non Executive Director ",
					"description" => "<p>Scott Field-Marsham worked at Morgan Stanley, a top tier global financial services firm, in the Institutional Equities Division for 26 years. He has extensive experience across a broad range of equity products, services and markets. Scott worked in multiple different IED product areas and roles, in London and Hong Kong.  For his final 3 years Scott was the Chief Operating Officer of IED Asia (500+ employees), and a board member of 5 Morgan Stanley Asia firm wide operating companies in the region.</p> "
				]

			);
		return view('about.nonexecutive')->with('profiles', $profiles);
	}

	public function overviewnse()
	{
		return view('about.overviewnse');
	}
	public function transactionhistory()
	{
		return view('about.transactionhistory');
	}
	public function usefullinks()
	{
		return view('about.usefullinks');
	}
}

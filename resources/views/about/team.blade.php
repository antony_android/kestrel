@extends('layouts.app')
<!-- Carousel section -->
@section('content')
<section id="team-top-section">
	<div class="py-5 brown-bg ">
		<h3 class="text-light text-center"> <strong>ABOUT US</strong> </h3>
		<h1 class="text-light text-center"> <strong>OUR TEAM</strong> </h1>
	</div>
	<div class="overflow-sec brown-bg">

	</div>
	<div class="container">


		<div class="row">
			<?php $counter = 0;
			foreach ($profiles as $profile) {
				$counter = $counter + 1; ?>
				<div class="col-md-4 mb-4">
					<div class="card team-wrap shadow">
						<div class="card-body text-center">
							<h6 class="mb-3 mt-3"><?= $profile['name'] ?> </h6>
							<?php $profTitle = $profile['title']; ?>
							<?php $cols = 177; ?>
							@if($profTitle == "Head of Fixed Income Sales & Trading")
							<?php $cols = 125; ?>
							@endif
							<h5 class="mb-3 title-sec" style="color: #ffce0a!important"><?= $profTitle  ?> </h5>
							<p style="line-height: 2;"><?= substr($profile['description'], 0, $cols) . "..."  ?></p>
							<button class="btn btn-sm read-more-profile" data-bs-toggle="modal" data-bs-target="#modal<?= $counter ?>">Read More</button>
						</div>
					</div>

				</div>
			<?php } ?>

		</div>
	</div>
</section>

<?php $counter = 0;
foreach ($profiles as $profile) {
	$counter = $counter + 1; ?>
	<!-- Modal -->
	<!-- Modal -->
	<div class="modal fade" id="modal<?= $counter ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel"><?= $profile['name'] ?> </h5>

					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<h5 class="text-yellow mb-4"><?= $profile['title'] ?></h5>
					<p class="small-text"><?= $profile['description']  ?></p>
				</div>

			</div>
		</div>
	</div>


<?php } ?>

@endsection
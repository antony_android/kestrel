<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\SendMessage;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class ContactController extends Controller
{
	public function index()
	{
		return view('contact.index');
	}

	public function sendEmail(Request $request)
	{

		$displayNone = $request->message_more;

		if (strlen($displayNone) == 0) {

			$data = $request->validate([
				'fullname' => 'required|max:255',
				'email_address' => 'required|max:255',
				'subject' => 'required|max:255',
				'message' => 'required',
			]);

			Mail::to("info@kestrelcapital.com")
				->send(new SendMessage($data));

			Mail::to("antonjoro2008@gmail.com")
				->send(new SendMessage($data));

			Session::flash('success', 'Your message has been sent successfully to us! Please wait and we will be in touch. Thank you.');
			return redirect()->back();
		}
		return redirect()->back();
	}
}

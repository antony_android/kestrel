$( document ).ready(function() {

    $('.custom-selectbox #category').on('click', function( e ) {
        $(this).parent().toggleClass("open");
        return false;
    });

    $('#add-user-form').hide();
    $('#add-category-form').hide();
    $('#add-subcategory-form').hide();
    $('#add-element-form').hide();
    $('#add-data-form').hide();
    $('#upload-data-form').hide();

    $(document).on("click", ".deluser", function () {
        var passedID = $(this).attr('rel');
        $("#del-user").val(passedID);
    });

    $(document).on("click", ".delcategory", function () {
        var passedID = $(this).attr('rel');
        $("#del-category").val(passedID);
    });

    $(document).on("click", ".delsubcategory", function () {
        var passedID = $(this).attr('rel');
        $("#del-subcategory").val(passedID);
    });

    $(document).on("click", ".delelement", function () {
        var passedID = $(this).attr('rel');
        $("#del-element").val(passedID);
    });

    $(document).on("click", ".deldata", function () {
        var passedID = $(this).attr('rel');
        $("#del-data").val(passedID);
    });

});

function formCollapseHandler(elem){

    if($('#'+elem).is(":visible")){

        $('#'+elem).hide();
    }else {
        $('#add-data-form').hide();
        $('#upload-data-form').hide();
        $('#'+elem).show();
    }
}

function searchUsers() {
    var keyword = $("#search-user").val();
    
    $.ajax({
        type: "POST",
        data: {"_token": $('meta[name="csrf-token"]').attr('content'),"keyword":keyword},
        url: "/admin/users/search",
        success: function(data){
            data = $.parseJSON(data);

            let html = "";
            let approvalHTML = ""; 
            let userStatus = 'Deleted';

            for(let item of data) {
                let name = item.name;
                let email = item.email;
                let role = item.role;
                let status = item.status;
                let user_id = item.id;
                let approvedBy = item.approved_by;

                if(approvedBy == null || status == 0){
                    approvalHTML = approvalHTML+'<a href="/admin/users/approve/'+user_id+'" title="Approve User"><img src="/img/approve.png" width="15px" alt="Approve" /></a>';
                }

                if(status == 0){
                    userStatus = 'Inactive'
                }else if(status == 1){
                    userStatus = 'Active';
                }else if(status == 2){
                    userStatus = 'Locked';
                }

                html += '<div class="row t-content"><div class="col-3">'+name+'</div><div class="col-4">'+email+'</div><div class="col-2">'+role+'</div><div class="col-1">'+userStatus+'</div><div class="col-2 edit-butts"><span><a href="/admin/users/edit/'+user_id+'"><img src="/img/edit.png" width="15px" /></a><button rel="'+user_id+'" class="btn deluser" data-bs-toggle="modal" data-bs-target="#usersModal"><img src="/img/delete.png" width="15px" /></button>'+approvalHTML+'</div></div>';
            }
            $("#users-data").animate({opacity: '0.8'});
            $("#users-data").html(html);
            $("#users-data").animate({opacity: '1'});
        }
     });
}

function searchCategories() {
    var keyword = $("#search-category").val();

    $.ajax({
        type: "POST",
        data: {"_token": $('meta[name="csrf-token"]').attr('content'),"keyword":keyword},
        url: "/admin/elements/categories/search",
        success: function(data){
            data = $.parseJSON(data);
            let html = "";
            for(let item of data) {
                let category_name = item.category_name;
                let category_id = item.category_id;
                html += '<div class="row t-content"><div class="col-6">'+category_name+'</div><div class="col-6 edit-butts"><span><a href="/admin/elements/categories/edit/'+category_id+'"><img src="/img/edit.png" width="15px" /></a><button rel="'+category_id+'" class="btn delcategory" data-toggle="modal" data-bs-target="#categoriesModal"><img src="/img/delete.png" width="15px" /></button></span></div></div>';
            }
            $("#categories-data").animate({opacity: '0.8'});
            $("#categories-data").html(html);
            $("#categories-data").animate({opacity: '1'});
        }
     });
}

function searchSubCategories() {
    var keyword = $("#search-subcategory").val();

    $.ajax({
        type: "POST",
        data: {"_token": $('meta[name="csrf-token"]').attr('content'),"keyword":keyword},
        url: "/admin/elements/sub-categories/search",
        success: function(data){
            data = $.parseJSON(data);
            let html = "";
            for(let item of data) {
                let sub_category_name = item.sub_category_name;
                let sub_category_id = item.sub_category_id;
                let category_name = item.category_name;
                html += '<div class="row t-content"><div class="col-sm-4">'+sub_category_name+'</div><div class="col-sm-4">'+category_name+'</div><div class="col-sm-4 edit-butts"><span><a href="/admin/elements/sub-categories/edit/'+sub_category_id+'"><img src="/img/edit.png" width="15px" /></a><button rel="'+sub_category_id+'" class="btn delsubcategory" data-toggle="modal" data-bs-target="#subCategoriesModal"><img src="/img/delete.png" width="15px" /></button></span></div></div>';
            }
            $("#sub-categories-data").animate({opacity: '0.8'});
            $("#sub-categories-data").html(html);
            $("#sub-categories-data").animate({opacity: '1'});
        }
     });
}

function searchElements() {
    var keyword = $("#search-element").val();

    $.ajax({
        type: "POST",
        data: {"_token": $('meta[name="csrf-token"]').attr('content'),"keyword":keyword},
        url: "/admin/elements/search",
        success: function(data){
            data = $.parseJSON(data);
            let html = "";
            for(let item of data) {
                let element_name = item.elem_description;
                let sub_category_name = item.sub_category_name;
                let elem_id = item.elem_id;
                let category_name = item.category_name;
                html += '<div class="row t-content"><div class="col-sm-6">'+element_name+'</div><div class="col-sm-2">'+sub_category_name+'</div><div class="col-sm-2">'+category_name+'</div><div class="col-sm-2 edit-butts"><span><a href="/admin/elements/edit/'+elem_id+'"><img src="/img/edit.png" width="15px" /></a><button rel="'+elem_id+'" class="btn delelement" data-toggle="modal" data-bs-target="#elementsModal"><img src="/img/delete.png" width="15px" /></button></span></div></div>';
            }
            $("#elements-data").animate({opacity: '0.8'});
            $("#elements-data").html(html);
            $("#elements-data").animate({opacity: '1'});
        }
     });
}

function capitalize(word) {
  const lower = word.toLowerCase();
  const splitted = lower.split(" ");
  const capitalized = splitted.map(ucword);
  return capitalized.join(" ");
}

function ucword(value) { return value.charAt(0).toUpperCase() + value.slice(1)}

function searchData() {
    var keyword = $("#search-data").val();

    $.ajax({
        type: "POST",
        data: {"_token": $('meta[name="csrf-token"]').attr('content'),"keyword":keyword},
        url: "/admin/insights/search",
        success: function(data){
            data = $.parseJSON(data);
            let html = "";
            for(let item of data) {
                let element_name = item.element_description;
                let data_table_type = item.data_table_type;
                let data_table_id = item.data_table_id;
                let period = item.month;
                let data_value = item.data_value;

                if(period == null){
                    period = item.period;
                }
                let year = item.year;

                if(year != null && period != null){
                    period = period+", "+year;
                }else if(year != null && period == null){
                    period = year;
                }
                if(period == null && year == null){
                    period = 'N/A';
                }
                let time_of_trade = item.time_of_trade;
                if(time_of_trade == null){
                    time_of_trade = 'N/A';
                }

                html += '<div class="row t-content"><div class="col-sm-4">'+element_name+'</div><div class="col-sm-1">'+capitalize(data_table_type)+'</div><div class="col-sm-2">'+period+'</div><div class="col-sm-2">'+time_of_trade+'</div><div class="col-sm-2">'+data_value+'</div><div class="col-sm-1 edit-butts"><span><a href="/admin/insights/edit/'+data_table_id+'"><img src="/img/edit.png" width="15px" /></a><button rel="'+data_table_id+'" class="btn deldata" data-toggle="modal" data-bs-target="#dataModal"><img src="/img/delete.png" width="15px" /></button></span></div></div>';
            }
            $("#data").animate({opacity: '0.8'});
            $("#data").html(html);
            $("#data").animate({opacity: '1'});
        }
     });
}
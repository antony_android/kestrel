@extends('layouts.app')
<!-- Carousel section -->
@section('content')
<section id="contactsection">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h1 class="contact-hd text-grey" style="font-size: 2rem;"> <strong>CONTACT DETAILS</strong> </h1>

				<p>
					Kestrel Capital (East Africa) Ltd <br>
					1st Floor, Orbit Place <br>
					Westlands Road <br>
					P.O. Box 40005 – 00100 <br>
					Nairobi, Kenya <br>
					Tel: 254 (0) 20 2251758 <br>
					Mobile: 254 (0) 722205897 <br>
					Email: info@kestrelcapital.com <br>
					Bloomberg: KEST
				</p>
			</div>

			<div class="col-md-6">
				<div class="card contact-form shadow">
					<div class="card-body p-5">
						<h3 style="font-weight: lighter;"> SEND US A MESSAGE </h3>
						<form method="post" action="/send-message">
							@csrf()
							<div class="form-group mb-3">

								<input type="text" class="form-control" id="fullname" name="fullname" aria-describedby="emailHelp" placeholder="Enter name">

							</div>
							<div class="form-group mb-3">

								<input type="email" class="form-control" id="email_address" name="email_address" aria-describedby="emailHelp" placeholder="Enter email">

							</div>
							<div class="form-group mb-3">

								<input type="text" class="form-control" id="subject" name="subject" aria-describedby="emailHelp" placeholder="Enter subject">

							</div>
							<div class="form-group mb-3">

								<textarea class="form-control" id="message" name="message" rows="3" style="height: 80px;"></textarea>
							</div>
							<input class="dispnon" name="message_more" id="message_more" type="text">
							<button type="submit" class="btn btn-success btn-sm form-control py-2" style="background-color: #3E281A; border-color: #3E281A;color:#FFF;">Send</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<style type="text/css">
	.dispnon {
		display: none;
	}

	#map {
		height: 400px;
		/* The height is 400 pixels */
		width: 100%;
		/* The width is the width of the web page */
	}
</style>
<!-- <div id="map"></div> -->

<div class="mapouter-contact">
	<div class="gmap_canvas">
		<iframe width="100%" height="300" id="gmap_canvas" src="https://maps.google.com/maps?q=Orbit%20Place&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><br>
		<style>
			.mapouter-contact {
				position: relative;
				text-align: right;
				height: 300px;
				width: 103%;
			}
		</style>
		<style>
			.gmap_canvas {
				overflow: hidden;
				background: none !important;
				height: 300px !important;
				width: 100% !important;
				border-radius: 10px;
			}
		</style>
	</div>
</div>


@endsection

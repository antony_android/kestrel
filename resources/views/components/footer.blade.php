<section id="footerSection" class="brown-bg py-5">
	<div class="container">
		<div class="row">
			<div class="col-md-4" data-aos="fade-up">
				<h4 class="text-light mb-4">Useful Links</h4>
				<ul class="footer_list">
					<li>
						<a target="_blank" href="https://www.nse.co.ke/">
							Nairobi Securities Exchange
						</a>
					</li>
					<li>
						<a target="_blank" href="https://www.cma.or.ke/">
							Capital Markets Authority
						</a>
					</li>
					<li>
						<a target="_blank" href="https://www.cdsckenya.com/">
							Central Depository & Settlement Corporation
						</a>
					</li>
					<li>
						<a target="_blank" href="https://www.centralbank.go.ke/">
							Central Bank of Kenya
						</a>
					</li>
					<li>
						<a target="_blank" href="http://www.knbs.or.ke/">
							Kenya National Bureau of Statistics
						</a>
					</li>
					<li>
						<a target="_blank" href="https://www.rba.go.ke/">
							Retirement Benefits Authority
						</a>
					</li>

				</ul>
			</div>
			<div class="col-md-4" data-aos="fade-up">
				<h4 class="text-light mb-4">Contact Us</h4>
				<div class="contact-list">
					<div class="icon-section">
						<svg height="20" viewBox="0 0 512 512" width="20" xmlns="http://www.w3.org/2000/svg">
							<path d="m256 75c-24.8125 0-45 20.1875-45 45s20.1875 45 45 45 45-20.1875 45-45-20.1875-45-45-45zm0 0" />
							<path d="m256 0c-66.167969 0-120 53.832031-120 120 0 22.691406 6.371094 44.796875 18.429688 63.925781l101.570312 162.074219 101.570312-162.074219c12.058594-19.128906 18.429688-41.234375 18.429688-63.925781 0-66.167969-53.832031-120-120-120zm0 195c-41.351562 0-75-33.648438-75-75s33.648438-75 75-75 75 33.648438 75 75-33.648438 75-75 75zm0 0" />
							<path d="m182.996094 512h145.957031l-11.535156-91h-123.175781zm0 0" />
							<path d="m197.992188 391h50.914062l-42.488281-67.386719zm0 0" />
							<path d="m343.828125 391h118.175781l-37.5-90h-92.21875zm0 0" />
							<path d="m49.996094 391h117.765625l11.25-90h-91.515625zm0 0" />
							<path d="m263.09375 391h50.476562l-8.527343-66.523438zm0 0" />
							<path d="m164.011719 421h-126.515625l-37.496094 91h152.765625zm0 0" />
							<path d="m474.503906 421h-126.832031l11.539063 91h152.789062zm0 0" />
						</svg>
					</div>
					<div class="footer_text">

						Kestrel Capital (East Africa) Ltd<br>
						1st Floor, Orbit Place<br>
						Westlands Road<br>
						P.O. Box 40005 – 00100<br>
						Nairobi, Kenya<br>
					</div>
				</div>

				<!--  -->
				<div class="contact-list mt-2">
					<div class="icon-section">
						<svg height="20" width="20" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
							<g>
								<g>
									<path d="M436.992,74.953c-99.989-99.959-262.08-99.935-362.039,0.055s-99.935,262.08,0.055,362.039s262.08,99.935,362.039-0.055
			c48.006-48.021,74.968-113.146,74.953-181.047C511.986,188.055,485.005,122.951,436.992,74.953z M387.703,356.605
			c-0.011,0.011-0.022,0.023-0.034,0.034v-0.085l-12.971,12.885c-16.775,16.987-41.206,23.976-64.427,18.432
			c-23.395-6.262-45.635-16.23-65.877-29.525c-18.806-12.019-36.234-26.069-51.968-41.899
			c-14.477-14.371-27.483-30.151-38.827-47.104c-12.408-18.242-22.229-38.114-29.184-59.051
			c-7.973-24.596-1.366-51.585,17.067-69.717l15.189-15.189c4.223-4.242,11.085-4.257,15.326-0.034
			c0.011,0.011,0.023,0.022,0.034,0.034l47.957,47.957c4.242,4.223,4.257,11.085,0.034,15.326c-0.011,0.011-0.022,0.022-0.034,0.034
			l-28.16,28.16c-8.08,7.992-9.096,20.692-2.389,29.867c10.185,13.978,21.456,27.131,33.707,39.339
			c13.659,13.718,28.508,26.197,44.373,37.291c9.167,6.394,21.595,5.316,29.525-2.56l27.221-27.648
			c4.223-4.242,11.085-4.257,15.326-0.034c0.011,0.011,0.022,0.022,0.034,0.034l48.043,48.128
			C391.911,345.502,391.926,352.363,387.703,356.605z" />
								</g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
						</svg>
					</div>
					<div class="footer_text">
						254 (0) 20 2251758
					</div>
				</div>

				<!--  -->
				<div class="contact-list mt-2">
					<div class="icon-section">
						<svg height="20" width="20" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 27.442 27.442" style="enable-background:new 0 0 27.442 27.442;" xml:space="preserve">
							<g>
								<path d="M19.494,0H7.948C6.843,0,5.951,0.896,5.951,1.999v23.446c0,1.102,0.892,1.997,1.997,1.997h11.546
		c1.103,0,1.997-0.895,1.997-1.997V1.999C21.491,0.896,20.597,0,19.494,0z M10.872,1.214h5.7c0.144,0,0.261,0.215,0.261,0.481
		s-0.117,0.482-0.261,0.482h-5.7c-0.145,0-0.26-0.216-0.26-0.482C10.612,1.429,10.727,1.214,10.872,1.214z M13.722,25.469
		c-0.703,0-1.275-0.572-1.275-1.276s0.572-1.274,1.275-1.274c0.701,0,1.273,0.57,1.273,1.274S14.423,25.469,13.722,25.469z
		 M19.995,21.1H7.448V3.373h12.547V21.1z" />
								<g>
								</g>
								<g>
								</g>
								<g>
								</g>
								<g>
								</g>
								<g>
								</g>
								<g>
								</g>
								<g>
								</g>
								<g>
								</g>
								<g>
								</g>
								<g>
								</g>
								<g>
								</g>
								<g>
								</g>
								<g>
								</g>
								<g>
								</g>
								<g>
								</g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
						</svg>

					</div>
					<div class="footer_text">
						254 (0) 722205897
					</div>
				</div>

				<!--  -->
				<div class="contact-list mt-2">
					<div class="icon-section">
						<!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
						<svg version="1.1" height="20" width="20" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
							<g>
								<g>
									<polygon points="339.392,258.624 512,367.744 512,144.896 		" />
								</g>
							</g>
							<g>
								<g>
									<polygon points="0,144.896 0,367.744 172.608,258.624 		" />
								</g>
							</g>
							<g>
								<g>
									<path d="M480,80H32C16.032,80,3.36,91.904,0.96,107.232L256,275.264l255.04-168.032C508.64,91.904,495.968,80,480,80z" />
								</g>
							</g>
							<g>
								<g>
									<path d="M310.08,277.952l-45.28,29.824c-2.688,1.76-5.728,2.624-8.8,2.624c-3.072,0-6.112-0.864-8.8-2.624l-45.28-29.856
			L1.024,404.992C3.488,420.192,16.096,432,32,432h448c15.904,0,28.512-11.808,30.976-27.008L310.08,277.952z" />
								</g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
						</svg>


					</div>
					<div class="footer_text">
						info@kestrelcapital.com
					</div>
				</div>

			</div>
			<div class="col-md-4" data-aos="fade-up">
				<h4 class="text-light mb-4 visit-us">Visit Us</h4>

				<div class="mapouter">
					<div class="gmap_canvas">
						<iframe width="350" height="250" id="gmap_canvas" src="https://maps.google.com/maps?q=Orbit%20Place&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><br>
						<style>
							.mapouter {
								position: relative;
								text-align: right;
								height: 250px;
								width: 100%;
							}
						</style>
						<style>
							.gmap_canvas {
								overflow: hidden;
								background: none !important;
								height: 250px;
								width: 98%;
								border-radius: 10px;
							}
						</style>
					</div>
				</div>


			</div>
		</div>
	</div>
</section>
<section class="brown-bg py-3">
	<div class="container copyright-section">
		<div class="row">
			<div class="col-md-12 text-light">
				<div class="marg text-center small-text">
					Copyright <?= date('Y') ?> Kestrel Capital
				</div>

			</div>

		</div>
	</div>
</section>

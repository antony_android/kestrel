@extends('layouts.app')
@section('content')

<div class="row justify-content-center m-15">

    <div class="col-sm-10">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        Categories
                    </div>
                    <div class="col-6">
                        <button class="btn btn-save btn-add" onclick="formCollapseHandler('add-category-form')">New Category</button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form action="/admin/elements/categories/save" method="post" id="add-category-form">
                    <div class="container">
                        <div class="row">
                            @csrf
                            @if ($errors->any())
                            <div class="alert alert-danger" role="alert">
                                Please fix the following errors
                            </div>
                            @endif
                            @if($selectedCategory != null)
                            <input type="hidden" id="selected_category" name="selected_category" value="{{$selectedCategory->category_id}}" />
                            @endif
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <label for="category_name">Category Name <i class="fas fa-asterisk asterisk"></i></label>
                                    <input type="text" class="form-control @error('category_name') is-invalid @enderror" id="category_name" name="category_name" placeholder="Name" value="@if($selectedCategory != null){{$selectedCategory->category_name}}@else{{ old('category_name') }}@endif">
                                    @error('category_name')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <label for="ui_position">UI Position <i class="fas fa-asterisk asterisk"></i></label>
                                    <select class="form-control @error('ui_position') is-invalid @enderror" id="ui_position" name="ui_position">
                                        <option value="">Select UI Position</option>
                                        <option value="statistics" @if($selectedCategory !=null) @if($selectedCategory->ui_position == 'statistics') selected @endif @endif>Statistics</option>
                                        <option value="price-trends" @if($selectedCategory !=null) @if($selectedCategory->ui_position == 'price-trends') selected @endif @endif>Price Trends</option>
                                    </select>
                                    @error('ui_position')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-primary bt-margin">Save</button>
                                @if($selectedCategory != null)
                                <a class="btn cancel-bt bt-margin" href="/admin/elements/categories">
                                    Cancel
                                </a>
                                @endif
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="container cnt-bd">
                <div class="row search-mg">
                    <div class="col-md-12 search-col">
                        <button class="btn float-right search-bt" onclick="searchCategories()">Search</button>
                        <input type="text" class="form-control float-right search-txt" id="search-category" name="search-category" placeholder="Search" />
                    </div>
                </div>
                <div class="row t-header">
                    <div class="col-6">Category Name</div>
                    <div class="col-6"></div>
                </div>
                <div id="categories-data">
                    @foreach ($categories as $category)
                    <div class="row t-content">
                        <div class="col-6">
                            {{ $category->category_name }}
                        </div>
                        <div class="col-6 edit-butts">
                            <span>
                                <a href="/admin/elements/categories/edit/{{$category->category_id}}">
                                    <img src="/img/edit.png" width="15px" alt="Edit" />
                                </a>
                                <button rel="{{$category->category_id}}" class="btn delcategory" data-bs-toggle="modal" data-bs-target="#categoriesModal">
                                    <img src="/img/delete.png" width="15px" alt="Delete" />
                                </button>
                            </span>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="categoriesModal" tabindex="-1" role="dialog" aria-labelledby="categoriesModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="categoriesModalLabel">Confirm Delete</h5>
                    <button type="button" class="btn-close bt-close" data-bs-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    Are you sure you would like to delete the selected category?
                </div>
                <div class="modal-footer">
                    <div class="row" style="width:100%">
                        <div class="col-8">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                        </div>
                        <div class="col-4">
                            <form action="/admin/elements/categories/del" method="post">
                                @csrf
                                <input type="hidden" id="del-category" name="deletecategory" />
                                <button type="submit" class="btn btn-primary">Confirm</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@if(!is_null($selectedCategory))
<script>
    $(document).ready(function() {

        $('#add-category-form').show();
    });
</script>
@endif
@endsection
<?php

namespace App\Reports;

use Illuminate\Support\Facades\DB;
use \koolreport\processes\NumberBucket;

use \DateTime;
use Illuminate\Support\Facades\Redis;

use App\Models\Elem;
use App\Models\DataTable;

class StatisticsReport extends \koolreport\KoolReport
{
    use \koolreport\laravel\Friendship;

    const PERIOD_SUBQUERY = array(
        'ally' => " ORDER BY year, period_id, month_id, time_of_trade",
        '1y' => " AND DATE(time_of_trade) >= CURDATE() - INTERVAL 12 MONTH AND DATE(time_of_trade) <= CURDATE() ORDER BY year, period_id, month_id, time_of_trade",
        '5y' => " AND year >= YEAR(NOW()) - 5 AND year < YEAR(NOW()) GROUP BY year ORDER BY year",
        "10y" => " AND year >= YEAR(NOW()) - 10 AND year < YEAR(NOW()) GROUP BY year ORDER BY year"
    );

    function setup()
    {

        $periodToSelect = $this->params["period"];

        if ($periodToSelect == "hour") {

            $periodToSelect = "HOUR(time_of_trade)";
        } else if ($periodToSelect == "day") {

            $periodToSelect = "DAYNAME(time_of_trade)";
        } else if ($periodToSelect == "month") {

            $periodToSelect = "MONTHNAME(time_of_trade)";
        }

        if (
            $this->params["filterPeriod"] == "5y" ||
            $this->params["filterPeriod"] == "10y" ||
            strpos($this->params["report"], "banking") !== false ||
            strpos($this->params["report"], "telecom") !== false
        ) {

            $query = "SELECT " . $periodToSelect . " AS " . $this->params["periodTitle"] . ",
                 data_value as Amount FROM data_table WHERE 
                 element_description='" . $this->params["report_titles"][$this->params["report"]] . "'"
                . self::PERIOD_SUBQUERY[$this->params["filterPeriod"]];

            if ($this->params["filterPeriod"] == "1y") {

                $lastData = DB::select("SELECT DATE(time_of_trade-interval 12 month) AS last_date FROM data_table
                 WHERE element_description='" . $this->params["report_titles"][$this->params["report"]] . "' ORDER BY DATE(time_of_trade)
                  DESC LIMIT 1");

                if ($lastData) {
                    $lastDate = $lastData[0]->last_date;
                    $query = "SELECT " . $periodToSelect . " AS " . $this->params["periodTitle"] . ",
                        data_value as Amount FROM data_table WHERE 
                        element_description='" . $this->params["report_titles"][$this->params["report"]] . "'
                        AND DATE(time_of_trade) >= '" . $lastDate . "' AND DATE(time_of_trade) <= CURDATE() 
                        ORDER BY year, period_id, month_id, time_of_trade";
                }
            }

            if (
                $this->params["filterPeriod"] == "5y" ||
                $this->params["filterPeriod"] == "10y"
            ) {

                $summedUpRS = Elem::where('slug', $this->params["report"])
                    ->where('is_summed_up', 1)->first();

                $limit = 20;

                if($this->params["filterPeriod"] == "10y"){
                    $limit = 40;
                }

                $summedUp = !is_null($summedUpRS)?TRUE:FALSE;

                if($summedUp){

                    $query = "SELECT " . $periodToSelect . " AS " . $this->params["periodTitle"] . ",
                        SUM(data_value) as Amount FROM data_table WHERE 
                        element_description='" . $this->params["report_titles"][$this->params["report"]] . "'"
                            . self::PERIOD_SUBQUERY[$this->params["filterPeriod"]];
                }else {

                    $dataTableRS = DataTable::where('slug', $this->params["report"])
                    ->orderBy('data_table_id', 'DESC')->first();

                    $defaultPeriod = "Quarter";

                    if(!is_null($dataTableRS)){
                        $defaultPeriod = $dataTableRS->default_period;
                    }

                    if($defaultPeriod == "Quarter"){
                        
                        $query = "SELECT ". $periodToSelect . " AS " . $this->params["periodTitle"] . ",
                         main.data_value as Amount FROM (SELECT `year`,period,data_value FROM data_table WHERE 
                        element_description='" . $this->params["report_titles"][$this->params["report"]] . "'
                         ORDER BY `year` DESC, `period_id` DESC, month_id DESC LIMIT $limit) main GROUP BY main.year 
                         ORDER BY main.year ASC";

                    }else if($defaultPeriod == "Month"){

                        $limit = 60;
                
                        if($this->params["filterPeriod"] == "10y"){
                            $limit = 120;
                        }
                        $query = "SELECT ". $periodToSelect . " AS " . $this->params["periodTitle"] . ",
                         main.data_value as Amount FROM (SELECT `month`,period,data_value FROM data_table WHERE 
                        element_description='" . $this->params["report_titles"][$this->params["report"]] . "'
                         ORDER BY `year` DESC, `month_id` DESC LIMIT $limit) main GROUP BY main.year 
                         ORDER BY main.year ASC";
                    }
                }
            }

            $this->src("mysql")
                ->query($query)
                ->params(array('report_titles' => $this->params["report_titles"]))
                ->pipe(new NumberBucket(array(
                    "Year" => array(
                        "type" => "number",
                        "thousandSeparator" => "",
                        "step" => 1,
                        "formatString" => "{from}"
                    )
                )))
                ->pipe($this->dataStore("datastore"));
        } else {

            $query = "SELECT " . $periodToSelect . " AS " . $this->params["periodTitle"] . ",
                 data_value as Amount FROM data_table WHERE 
                 element_description='" . $this->params["report_titles"][$this->params["report"]] . "'"
                . self::PERIOD_SUBQUERY[$this->params["filterPeriod"]];

            if ($this->params["filterPeriod"] == "1y") {

                $lastData = DB::select("SELECT DATE(time_of_trade-INTERVAL 12 MONTH) AS last_date FROM data_table
                 WHERE element_description='" . $this->params["report_titles"][$this->params["report"]] . "' ORDER BY DATE(time_of_trade)
                  DESC LIMIT 1");

                if ($lastData) {
                    $lastDate = $lastData[0]->last_date;
                    $query = "SELECT " . $periodToSelect . " AS " . $this->params["periodTitle"] . ",
                        data_value as Amount FROM data_table WHERE 
                        element_description='" . $this->params["report_titles"][$this->params["report"]] . "'
                        AND DATE(time_of_trade) >= '" . $lastDate . "' AND DATE(time_of_trade) <= CURDATE() 
                        ORDER BY year, period_id, month_id, time_of_trade";
                }
            }

            if (
                $this->params["filterPeriod"] == "5y" ||
                $this->params["filterPeriod"] == "10y"
            ) {

                $query = "SELECT " . $periodToSelect . " AS " . $this->params["periodTitle"] . ",
                 SUM(data_value) as Amount FROM data_table WHERE 
                 element_description='" . $this->params["report_titles"][$this->params["report"]] . "'"
                    . self::PERIOD_SUBQUERY[$this->params["filterPeriod"]];
            }
            
            $this->src("mysql")
                ->query($query)
                ->params(array("report_titles" => $this->params["report_titles"]))
                ->pipe($this->dataStore("datastore"));
        }
    }

    public static function getNseMarketCapitalization()
    {

        $cachedMarketCap = json_encode(DB::select("SELECT period AS Quarter, data_value as Amount
             FROM data_table WHERE element_description='NSE market-cap (USD m)'"));

        return json_decode($cachedMarketCap, FALSE);
    }

    public static function getActiveMobileUsers()
    {

        $cachedActiveMobileUsers = json_encode(DB::select("SELECT `year` AS Year, data_value as Amount
             FROM data_table WHERE element_description='Active mobile phone users (m)'"));

        return json_decode($cachedActiveMobileUsers, FALSE);
    }

    public static function getTreasuryBillsPerformance()
    {

        $cachedTreasuryBillsPerfomance = json_encode(DB::select("SELECT period AS Month,
            data_value as Amount FROM data_table WHERE 
            element_description='Treasury bills auction performance rate %'"));

        return json_decode($cachedTreasuryBillsPerfomance, FALSE);
    }

    public static function getBankingTotalAssets()
    {

        $cachedBankingTotalAssets = json_encode(DB::select("SELECT `year` AS Year,
            data_value as Amount FROM data_table WHERE 
            element_description='Total assets (USD bn)'"));

        return json_decode($cachedBankingTotalAssets, FALSE);
    }
}

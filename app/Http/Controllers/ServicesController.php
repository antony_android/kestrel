<?php

namespace App\Http\Controllers;

class ServicesController extends Controller
{
	public function sales()
	{
		return view('services.sales');
	}
	public function research()
	{
		return view('services.research');
	}
	public function privateplacements()
	{
		return view('services.privateplacements');
	}
	public function corporateadvisory()
	{
		return view('services.corporateadvisory');
	}
	
	
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\Models\User;
use App\Models\UserRole;
use App\Rules\MatchOldPassword;

use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    const USERS_BLADE = 'admin/users';
    const PWD_CHANGE_BLADE = 'admin/change_password';
    const USERS_HOME = '/admin/users';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $users = User::where('status', '!=', -1)->orderBy('created_at', 'DESC')
            ->paginate(10);
        $user_roles = UserRole::all();

        return view(self::USERS_BLADE, [
            'users' => $users,
            'user_roles' => $user_roles,
            'selectedUser' => null
        ]);
    }

    public function save(Request $request)
    {

        $selected = $request->selected_user;

        if ($selected) {
            $data = $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255'],
                'user_role_id' => ['required', 'numeric']
            ]);
        }

        $data['approved_by'] = Auth::user()->id;
        $data['status'] = $request->status;

        if ($selected) {

            $updateData = [
                'name' => $data['name'],
                'user_role_id' => $data['user_role_id'],
                'email' => $data['email'],
                'status' => $data['status']
            ];

            $user = tap(User::findOrFail($selected))->update($updateData);
            if ($user->approved_by == null) {
                $user->approved_by =
                    Auth::user()->id;
                $user->save();
            }
        } else {

            $data = $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'user_role_id' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed']
            ]);

            $data['approved_by'] = Auth::user()->id;
            $data['status'] = $request->status;
            $data['password'] = Hash::make($data['password']);
            $data['email_verified_at'] = date("Y-m-d H:i:s");

            tap(new User($data))->save();
        }

        Session::flash('success', 'User details saved successfully!');
        return redirect(self::USERS_HOME);
    }

    public function search(Request $request)
    {

        $keyword = $request->keyword;
        $users = DB::select(Db::raw("SELECT `name`, email,  users.status,
             users.id, user_role.role, approved_by FROM users INNER JOIN user_role  
            ON users.user_role_id = user_role.user_role_id WHERE (`name`
             LIKE '%" . $keyword . "%' OR email like '%" . $keyword . "%' OR 
             user_role.role like '%" . $keyword . "%') AND users.status != -1"));

        return json_encode($users);
    }

    public function edit($id)
    {

        $userId = $id;
        $selectedUser = User::find($userId);
        $users = User::where('status', '!=', -1)->orderBy('created_at', 'DESC')
            ->paginate(10);
        $user_roles = UserRole::all();

        return view(
            UserController::USERS_BLADE,
            [
                'users' => $users, 'user_roles' => $user_roles,
                'selectedUser' => $selectedUser
            ]
        );
    }

    public function approve($id)
    {

        tap(User::findOrFail($id))->update([
            'approved_by' => Auth::user()->id,
            'status' => 1,
        ]);

        Session::flash('success', 'User approved successfully!');
        return redirect(self::USERS_HOME);
    }

    public function del(Request $request)
    {

        $userId = $request->deleteuser;
        $user = User::find($userId);
        $user->update(['status' => -1]);

        Session::flash('success', 'User deleted successfully!');
        return redirect(self::USERS_HOME);
    }

    public function changePassword(Request $request)
    {
        return view(self::PWD_CHANGE_BLADE);
    }

    public function changePasswordSave(Request $request)
    {
        $request->validate([
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required', 'min:8'],
            'new_confirm_password' => ['same:new_password'],
        ]);

        User::find(auth()->user()->id)->update(['password' => Hash::make($request->new_password)]);

        Session::flash('success', 'Your password has been changed successfully!');
        return redirect('/admin/user/change-password');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}

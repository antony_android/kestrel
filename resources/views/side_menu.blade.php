<ul id="accordion" class="accordion stats-menu">
    <li>
        <div class="link"><i class="fa fa-question">
            </i>Capital Markets<i class="fa fa-chevron-down"></i>
        </div>
        <ul class="submenu">
            <li>
                <a href="/statistics/equities">Equities</a>
            </li>
            <li>
                <a href="/statistics/income">Fixed Income</a>
            </li>
        </ul>
    </li>
    <li>
        <div class="link"><i class="fa fa-question">
            </i>Industry<i class="fa fa-chevron-down"></i>
        </div>
        <ul class="submenu">
            <li>
                <a href="/statistics/banking">Banking</a>
            </li>
            <li>
                <a href="/statistics/telecoms">Telecoms</a>
            </li>
        </ul>
    </li>
</ul>
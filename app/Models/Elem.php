<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $elem_id
 * @property string $elem_description
 * @property string $elem_slug
 * @property string $ui_position
 * @property string $category_id
 * @property string $sub_category_id
 * @property string default_period
 * @property string default_period_slug
 * @property string default_period_duration
 * @property string $created
 * @property string $modified
 * @property integer $status
 * @property integer $user_id
 * @property integer $is_summed_up
 * @property integer $priority
 * @property Category $category
 * @property SubCategory $subCategory
 */

class Elem extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'elem';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'elem_id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'elem_id', 'elem_description', 'slug', 'ui_position',
        'category_id', 'sub_category_id', 'default_period',
        'default_period_slug', 'default_period_duration',
        'created', 'modified', 'status', 'user_id', 'priority',
         'is_summed_up'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id', 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subCategory()
    {
        return $this->belongsTo('App\Models\SubCategory', 'sub_category_id', 'sub_category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function dataTableElems()
    {
        return $this->hasMany('App\Models\DataTable', 'slug', 'slug');
    }
}

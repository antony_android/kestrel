@extends('layouts.app')
@section('content')

<div class="row justify-content-center m-15">

    <div class="col-sm-10">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        Sub Categories
                    </div>
                    <div class="col-6">
                        <button class="btn btn-save btn-add" onclick="formCollapseHandler('add-subcategory-form')">New Sub Category</button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form action="/admin/elements/sub-categories/save" method="post" id="add-subcategory-form">
                    <div class="container">
                        <div class="row">
                            @csrf
                            @if ($errors->any())
                            <div class="alert alert-danger" role="alert">
                                Please fix the following errors
                            </div>
                            @endif
                            @if($selectedSubCategory != null)
                            <input type="hidden" id="selected_subcategory" name="selected_subcategory" value="{{$selectedSubCategory->sub_category_id}}" />
                            @endif

                            <div class="col-sm-9">
                                <div class="form-group">
                                    <label for="category_id">Category <i class="fas fa-asterisk asterisk"></i></label>
                                    <select class="form-control @error('category_id') is-invalid @enderror" id="category_id" name="category_id">
                                        <option value="">Select Category</option>
                                        @foreach($categories as $category)
                                        @if($selectedSubCategory != null)
                                        <option @if($selectedSubCategory->category_id == $category->category_id) selected @endif value="{{$category->category_id}}">{{$category->category_name}}</option>
                                        @else
                                        <option value="{{$category->category_id}}">{{$category->category_name}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                    @error('category_id')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-9">
                                <div class="form-group">
                                    <label for="category_name">Sub Category Name <i class="fas fa-asterisk asterisk"></i></label>
                                    <input type="text" class="form-control @error('sub_category_name') is-invalid @enderror" id="sub_category_name" name="sub_category_name" placeholder="Name" value="@if($selectedSubCategory != null){{$selectedSubCategory->sub_category_name}}@else{{ old('sub_category_name') }}@endif">
                                    @error('sub_category_name')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-9">
                                <div class="form-group">
                                    <label for="icon_class">FA Icon Class</label>
                                    <input type="text" class="form-control @error('icon_class') is-invalid @enderror" id="icon_class" name="icon_class" placeholder="Icon Class" value="@if($selectedSubCategory != null){{$selectedSubCategory->icon_class}}@else{{ old('icon_class') }}@endif">
                                    @error('icon_class')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-primary bt-margin">Save</button>
                                @if($selectedSubCategory != null)
                                <a class="btn cancel-bt bt-margin" href="/admin/elements/sub-categories">
                                    Cancel
                                </a>
                                @endif
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="container cnt-bd">
                <div class="row search-mg">
                    <div class="col-md-12 search-col">
                        <button class="btn float-right search-bt" onclick="searchSubCategories()">Search</button>
                        <input type="text" class="form-control float-right search-txt" id="search-subcategory" name="search-subcategory" placeholder="Search" />
                    </div>
                </div>
                <div class="row t-header">
                    <div class="col-sm-4">Sub Category Name</div>
                    <div class="col-sm-4">Category</div>
                    <div class="col-sm-4"></div>
                </div>
                <div id="sub-categories-data">
                    @foreach ($subCategories as $subCategory)
                    <div class="row t-content">
                        <div class="col-sm-4">
                            {{ $subCategory->sub_category_name }}
                        </div>
                        <div class="col-sm-4">
                            {{ $subCategory->category->category_name }}
                        </div>
                        <div class="col-sm-4 edit-butts">
                            <span>
                                <a href="/admin/elements/sub-categories/edit/{{$subCategory->sub_category_id}}">
                                    <img src="/img/edit.png" width="15px" alt="Edit" />
                                </a>
                                <button rel="{{$subCategory->sub_category_id}}" class="btn delsubcategory" data-bs-toggle="modal" data-bs-target="#subCategoriesModal">
                                    <img src="/img/delete.png" width="15px" alt="Delete" />
                                </button>
                            </span>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="subCategoriesModal" tabindex="-1" role="dialog" aria-labelledby="subCategoriesModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="subCategoriesModalLabel">Confirm Delete</h5>
                    <button type="button" class="btn-close bt-close" data-bs-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    Are you sure you would like to delete the selected sub-category?
                </div>
                <div class="modal-footer">
                    <div class="row" style="width:100%">
                        <div class="col-8">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                        </div>
                        <div class="col-4">
                            <form action="/admin/elements/sub-categories/del" method="post">
                                @csrf
                                <input type="hidden" id="del-subcategory" name="deletesubcategory" />
                                <button type="submit" class="btn btn-primary">Confirm</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@if(!is_null($selectedSubCategory))
<script>
    $(document).ready(function() {

        $('#add-subcategory-form').show();
    });
</script>
@endif
@endsection